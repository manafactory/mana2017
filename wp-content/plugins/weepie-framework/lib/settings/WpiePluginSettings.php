<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * Abstract Class WpiePluginSettings
 * 
 * Class for handeling the WordPress Plugin settings.  
 * This class defines a couple of abstract methods which a child class must implement
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpiePluginSettings.php 55 2015-05-01 12:13:38Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 0.1
 */
abstract class WpiePluginSettings extends WpieBaseSettings {
	

	/**
	 * Settings group
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	protected $group;		
	

	/**
	 * The path of the (xml)  settings file
	 *
	 * @since 0.1
	 *
	 * @var string
	 */
	protected $path;	
	
	
	/**
	 * Unique Plugin namespace
	 * 
	 * @since 1.0
	 * 
	 * @var string
	 */
	protected $namespace; 
	
		
	/**
	 * The settings form fields data
	 *
	 * @since 0.1
	 *
	 * @var SimpleXMLElement
	 */
	protected $settings;	
	
	
	/**
	 * All instances of this class
	 * 
	 * @since 0.1
	 * 
	 * @var array
	 */
	public static $instances = array();
	

	/**
	 * Constructor
	 * 
	 * Calls parent WpieBaseSettings class. Hooks to WordPress actions. The child class must set appropriate action callback
	 * - admin_init: to register the settings
	 * - pre_update_option_{$option_name}: to save the settings to the stack before updating in the db   
	 * 
	 * @access public	
	 * 
	 * @uses WpiePluginSettings::_setGroup()  
	 * @uses WpiePluginSettings::_setPath()
	 * @uses WpiePluginSettings::setSettings() 	    
	 * 
	 * @param string $name
	 * @param string $path
	 * @param string $locale
	 * @param array $locales
	 * @param string $namespace
	 * 
	 * @since 0.1
	 */
	public function WpiePluginSettings( $name = '', $path = '', $locale = '', $locales = array(), $namespace = '' )
	{			
		try {	
						
			$this->_setGroup( $name );
			
			$this->_setPath( $path );	
			
			$this->_setNamespace( $namespace );
			
			parent::WpieBaseSettings( $name, $locale, $locales );
			
			$this->setSettings();						
			
			} catch( Exception $e ) {
			
				// only show Exception messages in the WP Dashboard
				if( is_admin() )
					echo "ERROR: {$e->getMessage()}<br/>";
			}					
			
		if( !isset( self::$instances[$name] ) )
			self::$instances[$name] = $this;	

		// only add hooks for current settings request
		if( isset( $_REQUEST['option_page'] ) && $_REQUEST['option_page'] === $name ) {
			
			add_action( 'admin_init' , array(&$this, 'register') );
			add_filter( 'pre_update_option_'.$this->getOptionName(), array( &$this, 'beforeUpdate' ), 10, 2 );
		}
	}		
	
	
	/**
	 * Destructor to unset big arrays
	 * 
	 * This destructor will unset all instances in {@link WpiePluginSettings::$instances}
	 * 
	 * @access public
	 * 
	 * @since 0.1
	 */
	public function __destruct()
	{
		unset( self::$instances[$this->getOptionName()] );
	}	
	
	
	/**
	 * Get the settings group
	 *
	 * @access public
	 *
	 * @since 0.1
	 *
	 * @return string
	 */
	public function getGroup()
	{
		return $this->group;
	}
	
	
	/**
	 * Get the settings form field info
	 *
	 * @access public
	 *
	 * @since 0.1
	 *
	 * @return SimpleXMLElement
	 */
	public function getSettings()
	{
		return $this->settings;
	}
	
	
	/* (non-PHPdoc)
	 * @see WpSettings::setDefaults()
	*
	* @since 0.1
	*/
	public function setDefaults()
	{
		if ( '' !== $this->path )
		{
			$name = $this->getOptionName();
				
			$xmlStr = WpieXmlSettingsHelper::serializeXml( file_get_contents( $this->path ) );
				
			if( $this->_setTransient( $name, $xmlStr ) )
			{
				$this->settings = simplexml_load_file( $this->path, null, LIBXML_NOCDATA  );
	
				$this->_writeTranslations( $this->settings );
	
				$this->_settingsWalker( $this->settings );
	
			} else {
				throw new Exception( sprintf( 'Transient not set for setting: %s', $name ) );
			}
		}
	}
	
	
	/**
	 * Let Modules extend the plugin settings
	 *
	 * @access public
	 *
	 * @param string $path abolute path to the settings file
	 *
	 * @since 0.1
	 */
	public function extendSettings( $path )
	{
		if( true === $this->isInit )
		{
			$name = $this->getOptionName();
			$pathSettings = $this->getPath();
				
			$extendedSettings = simplexml_load_file( $path, null, LIBXML_NOCDATA  );
				
			$this->_mergeSettings( $this->settings, $extendedSettings );
				
			$this->_writeTranslations( $this->settings );
				
			$this->_settingsWalker( $this->settings );
	
			$xmlStr = WpieXmlSettingsHelper::serializeXml( $this->settings->asXML() );
			$this->_setTransient( $name, $xmlStr );
		}
	}		
	
		
	/**
	 * Callback for the admin_init hook: register the setting
	 *
	 * @acces public
	 * 
	 * @uses register_setting()
	 *
	 * @since 0.1
	 */
	public function register()
	{		
		register_setting( $this->group , $this->getOptionName() ,  array(&$this, 'validator') );			
	}
		

	/**
	 * Action callback for sanitize_option_{$option_name}
	 * 
	 * The sanitize_option_{$option_name} hook is called threw 'register_setting()' and must be implemented by child class
	 * 
	 * @access public
	 * 
	 * @param array $data
	 * 
	 * @since 0.1
	 */
	abstract public function validator( $data );
	

	/**
	 * Do some setting related actions before updating the settings option
	 * 
	 * This is a callback hooked to filter 'pre_update_option_{$option_name}' 
	 * 
	 * @access public
	 * 
	 * @param array $data, the new values
	 * @param array $option the old values
	 * 
	 * @since 0.1
	 * 
	 * @return array
	 */
	abstract public function beforeUpdate( $data, $option );


	/**
	 * Satinize fields in the current stack
	 *
	 * @access protected
	 *
	 * @param array $data the $POST fields to satinize
	 * @param array $skip Skip fieds to skip
	 *
	 * @uses WpieBaseSettings::getStack()
	 * @uses WpieBaseSettings::offsetSet()
	 *
	 * @since 1.0.1
	 *
	 * @return $data
	 */
	protected function satinize( $data = array(), $skip = array() ) {
	
		foreach ( (array) $data as $field => $value ) {
				
			if( empty( $skip ) ) {
	
				$data[$field] = sanitize_text_field( $value );
			}
			elseif( !empty( $skip ) && !in_array( $field, $skip ) )
			{
					
				$data[$field] = sanitize_text_field( $value );
			}
		}
	
		return $data;
	}
	
	
	/**
	 * Get all instances of this class
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return array with settings WpiePluginSettings objects or empty array
	 */
	protected function getInstances()
	{
		return self::$instances;		
	}
	
	
	/**
	 * Get a single instance of this class
	 * 
	 * @access protected
	 * @param string $name
	 * 
	 * @since 0.1
	 * 
	 * @return object WpiePluginSettings or bool false if not exists 
	 */
	protected function getInstance( $name )
	{
		return (isset(self::$instances[$name])) ? self::$instances[$name] : false;
	}	
	
	
	/**
	 * Set the member settings
	 * 
	 * Child classes must implement this method
	 * This method sets the settings member with the SimpleXMLElement data from the DB transient _transient_[namespace]-{$this->optionName}
	 * 	  
	 * @access public
	 * 
	 * @since 0.1
	 */
	protected function setSettings()
	{		
		$name = $this->getOptionName();
		
		if( $transient = $this->_getTransient( $name, false ) )
		{ 
			$xmlStr = unserialize( $transient );
			
			$this->settings = simplexml_load_string( $xmlStr, null, LIBXML_NOCDATA );
			
		} else {
			throw new Exception( sprintf( 'Couldn\'t read transient for setting: %s', $name ) );
		}		
	}		

			
	
	/**
	 * Set the settings group for the setting being registered
	 * 
	 * @access private
	 * 
	 * @param string $group
	 * 
	 * @since 0.1
	 */
	private function _setGroup( $group )
	{			
		if( '' === $group )
			throw new Exception( 'input parameter $group is empty.' );
		else
			$this->group = $group;			
	}
	
	
	/**
	 * Set the path of the (xml) settings file
	 *
	 * @access private
	 * 
	 * @param string $path
	 * 
	 * @since 0.1
	 */
	private function _setPath( $path )
	{
		if( '' === $path )
			throw new Exception( 'input parameter $path is empty.' );
		else
			$this->path = $path;	
	}

	
	/**
	 * Set the namespace
	 * 
	 * @access private
	 * @param string $namespace
	 * @throws Exception if $namespace is empty
	 * 
	 * @since 1.0
	 */
	private function _setNamespace( $namespace )
	{
		if( '' === $namespace )
			throw new Exception( 'input parameter $namespace is empty.' );
		else
			$this->namespace = $namespace;
	}	
	
	
	/**
	 * Merge two SimpleXMLElement settings objects 
	 * 
	 * Modules settings are added to the plugin settings
	 * 
	 * @access private
	 * @param SimpleXMLElement $oldSettings
	 * @param SimpleXMLElement $extendedSettings
	 * 
	 * @since 0.1
	 */
	private function _mergeSettings( &$oldSettings, $extendedSettings )
	{
		foreach ( $extendedSettings->children() as $field )
		{
			if( 'group' === $field->getName() )
			{
				$groupName = (string)$field['name'];
				
				if( ! $oldSettings->xpath( '//settings/group[@name="'.$groupName.'"]' ) )
				{	
					$newGroup = $oldSettings->addChild( 'group' );
					$newGroup->addAttribute( 'name', $groupName );

					if( $field->group_title )
					{
						WpieXmlSettingsHelper::cdataField( $newGroup->addChild( 'group_title' ), (string)$field->group_title );
					}
					if( $field->group_descr )
					{
						WpieXmlSettingsHelper::cdataField( $newGroup->addChild( 'group_descr' ), (string)$field->group_descr );
					}			
					if( $field->group_warning )
					{
						WpieXmlSettingsHelper::cdataField( $newGroup->addChild( 'group_warning' ), (string)$field->group_warning );
					}							

					foreach ( $field->children() as $subfield )
					{												
						if( 'inline' === $subfield->getName() )
						{
							$inlineName = (string)$subfield['name'];
							$newInline = $newGroup->addChild( 'inline' );
							$newInline->addAttribute( 'name', $inlineName );
								
							if( $subfield->inline_title )
							{
								WpieXmlSettingsHelper::cdataField( $newInline->addChild( 'inline_title' ), (string)$subfield->inline_title );
							}
							if( $subfield->inline_descr )
							{
								WpieXmlSettingsHelper::cdataField( $newInline->addChild( 'inline_descr' ), (string)$subfield->inline_descr );
							}
								
							foreach ( $subfield->field as $inlineField )
							{
								$fieldName = (string)$inlineField['name'];
								if( ! $oldSettings->xpath( '//settings/group[@name="'.$groupName.'"]/inline[@name="'.$inlineName.'"]/field[@name="'.$fieldName.'"]' ) )
								{
									$newInlineField = $newInline->addChild( 'field' );
									$newInlineField->addAttribute( 'name', $fieldName );
									WpieXmlSettingsHelper::copyField( $inlineField, $newInlineField, $this->locale );
								}
									
							}//end inline loop							
						}//end inline
						if( 'field' === $subfield->getName() )
						{
							$fieldName = (string)$subfield['name'];
							if( ! $oldSettings->xpath( '//settings/group[@name="'.$groupName.'"]/field[@name="'.$fieldName.'"]' ) )
							{
								$newGroupField = $newGroup->addChild( 'field' );
								$newGroupField->addAttribute( 'name', $fieldName );
								WpieXmlSettingsHelper::copyField( $subfield, $newGroupField, $this->locale );
							}								
						}//end field						
					}//end loop children of group	
				}
			} 			
			elseif( 'inline' === $field->getName() )
			{
				$inlineName = (string)$field['name'];
				$newInline = $oldSettings->addChild( 'inline' );
				$newInline->addAttribute( 'name', $inlineName );
				
				if( $field->inline_title )
				{
					WpieXmlSettingsHelper::cdataField( $newInline->addChild( 'inline_title' ), (string)$field->inline_title );
				}
				if( $field->inline_descr )
				{
					WpieXmlSettingsHelper::cdataField( $newInline->addChild( 'inline_descr' ), (string)$field->inline_descr );
				}						
				
				foreach ( $field->field as $inlineField )
				{			
					$curName = (string)$inlineField['name'];
					if( ! $oldSettings->xpath( '//settings/inline/field[@name="'.$curName.'"]' ) )
					{												
						$newInlineField = $newInline->addChild( 'field' );
						$newInlineField->addAttribute( 'name', $curName );						
						WpieXmlSettingsHelper::copyField( $inlineField, $newInlineField, $this->locale );
					}
					
				}//end inline loop
			}			
			else { //node === field			

				$curName = (string)$field['name'];
				
				if( ! $oldSettings->xpath( '//settings/field[@name="'.$curName.'"]' ) )
				{									
					$newField = $oldSettings->addChild( 'field' );
					$newField->addAttribute( 'name', $curName );
					WpieXmlSettingsHelper::copyField( $field, $newField, $this->locale );					
				}				
			}
		}
	}	

	
	/**
	 * Set a transient for the current setting
	 * 
	 * This transient holds the string presentation of the current settings XML
	 * 
	 * @access private
	 * @param string $name
	 * @param string $xml
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if succeeded or false on failure
	 */
	private function _setTransient( $name, $xml )
	{
		if( !$xml )
			return false;
		else {
			return set_transient( $name.'_fields' , $xml );
		}								
	}	
	
	
	/**
	 * Get the current settings transient
	 * 
	 * @access private
	 * @param string $name the settings name
	 * 
	 * @since 0.1
	 * 
	 * @return mixed
	 */
	private function _getTransient( $name, $unserialize = true )
	{
		if( true === $unserialize )
			return unserialize( get_transient( $name.'_fields' ) );
		else
			return get_transient( $name.'_fields');		
	}
	
	
	/**
	 * Walk threw a settings (xml) field and add data to the stack
	 * 
	 * @access private
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 */
	private function _fieldWalkerDefaults( $field )
	{		
		foreach ( $field->xpath( '//field' ) as $subField )
		{
			$curName = (string)$subField['name'];
		
			if( $subField->xpath( 'defaults/default[@lang="'.$this->locale.'"]' ) )
			{
				$default = $subField->xpath( 'defaults/default[@lang="'.$this->locale.'"]' );
				$this->offsetSet( $curName, WpieXmlSettingsHelper::trimXmlField( $default[0] ));
			}
			elseif( $subField->xpath( 'default[@lang="'.$this->locale.'"]' ) )
			{
				$default = $subField->xpath( 'default[@lang="'.$this->locale.'"]' );
				$this->offsetSet( $curName, WpieXmlSettingsHelper::trimXmlField( $default[0] ));
			}
			elseif( $subField->default  )
			{
				$default = $subField->default;
				$this->offsetSet( $curName, WpieXmlSettingsHelper::trimXmlField( $default ));
			}
			else{
		
			}
		}		
	}	
	
	
	/**
	 * Walk threw a settings (xml) field and add I18n fields to the content returned.
	 * 
	 * Two types are used: normal and inline. Inline fields do not have 'title' and 'descr' fields.
	 * So these are only set for 'normal' fields.
	 * 
	 * @access private
	 * 
	 * @param SimpleXMLElement $field
	 * @param string $type
	 * 
	 * @since 0.1
	 *  
	 * @return string
	 */
	private function _fieldWalkerI18n( $field, $type='normal' )
	{
		$content = '';

		if( 'normal' === $type )
		{	
			if( $field->title )
			{
				$content .= WpieXmlSettingsHelper::I18nField( $field->title, $this->namespace );
			}
			if( $field->descr )
			{
				$content .= WpieXmlSettingsHelper::I18nField( $field->descr, $this->namespace );
			}			
		}		
		
		if( $field->inner )
		{
			$content .= WpieXmlSettingsHelper::I18nField( $field->inner, $this->namespace );
		}
		if( $field->options->option )
		{
			foreach( $field->options->option as $option )
			{
				$content .= WpieXmlSettingsHelper::I18nField( $option[0], $this->namespace );
			}
		}
	
		return $content;		
	}	
	
	
	/**
	 * Walk threw an (xml) settings object
	 * 
	 * @access private
	 * 
	 * @uses WpiePluginSettings::_fieldWalkerDefaults() 
	 * 
	 * @param SimpleXMLElement $settings
	 * 
	 * @since 0.1
	 */
	private function _settingsWalker( $settings )
	{		
		foreach ( $settings->children() as $field )
		{
			if( 'group' === $field->getName() || 'inline' === $field->getName() )
			{			
				$this->_fieldWalkerDefaults( $field );				
			} else { 
				//node === field
				$this->_fieldWalkerDefaults( $settings );
			}
		}		
	}
	
	
	/**
	 * Create a php file with WordPress translation functions
	 * 
	 * This will let WordPress know about translatable strings inside the settings XML
	 * 
	 * @access private
	 * 
	 * @param SimpleXMLElement $settings
	 * 
	 * @uses WpieXmlSettingsHelper::I18nField()
	 * @uses WpiePluginSettings::_fieldWalkerI18n() 
	 * 
	 * @since 0.1
	 */
	private function _writeTranslations( $settings )
	{
		$path = $this->getPath();
		$basePath = dirname( $path );
		$file = "$basePath/I18n.php";		
		$name = $this->getOptionName();	
		$beginPhrase = '//BEGIN ' . $name;
		$endPhrase = '//END ' . $name;
		$content = $beginPhrase . "\n";
		$i=1;
		clearstatcache();
		
		foreach ( $settings->children() as $field )
		{
			if( 'group' === $field->getName() )
			{
				foreach ( $field->children() as $subField )
				{
					//check for meta fields
					if( 'group_title' === $subField->getName() ) $content .= WpieXmlSettingsHelper::I18nField( $subField, $this->namespace );
					if( 'group_descr' === $subField->getName() ) $content .= WpieXmlSettingsHelper::I18nField( $subField, $this->namespace );
					if( 'group_warning' === $subField->getName() ) $content .= WpieXmlSettingsHelper::I18nField( $subField , $this->namespace);

					//check for inline fields
					if( 'inline' === $subField->getName() )
					{
						foreach ( $subField->children() as $inlineField )
						{
							if( 'inline_title' === $inlineField->getName() ) $content .= WpieXmlSettingsHelper::I18nField( $inlineField, $this->namespace );
							if( 'inline_descr' === $inlineField->getName() ) $content .= WpieXmlSettingsHelper::I18nField( $inlineField, $this->namespace );
						}						
						$content .= $this->_fieldWalkerI18n( $subField, 'inline' );					
					}//end inline							
				
					if( 'field' === $subField->getName() ) $content .= $this->_fieldWalkerI18n( $subField, 'normal' );
									
				}//end group loop
			} 
			elseif( 'inline' === $field->getName() ) 
			{		
				foreach ( $field->children() as $inlineField )
				{
					if( 'inline_title' === $inlineField->getName() ) $content .= WpieXmlSettingsHelper::I18nField( $inlineField, $this->namespace );
					if( 'inline_descr' === $inlineField->getName() ) $content .= WpieXmlSettingsHelper::I18nField( $inlineField, $this->namespace );
				}										
				$content .= $this->_fieldWalkerI18n( $field, 'inline' );					
			}			
			else { //node === field
				$content .= $this->_fieldWalkerI18n( $field, 'normal' );			
			}
		}
		$content .= $endPhrase. "\n";		
		
		if( file_exists( $file ) )
		{			
			$curContent = file_get_contents( $file );
			
			$beginPhrase = str_replace('/', '\/', $beginPhrase);
			$endPhrase =  str_replace('/', '\/', $endPhrase);
			
			$pattern = '/('.$beginPhrase.'.+'.$endPhrase.')/s';
			
			if( preg_match( $pattern, $curContent, $matches) )
			{
				$content = trim($content);
				$content = preg_replace($pattern, $content, $curContent);
				// be sure we have a php opening tag
				if( !preg_match('/\?php{1}/', $content) )
				{
					$content = '<?php' ."\n" . $content;
				}
								
			} else {				
				$content = $curContent . $content;	
				// be sure we have a php opening tag
				if( !preg_match('/\?php{1}/', $content) )
				{
					$content = '<?php' ."\n" . $content;
				}				
			}
								
		} else 
			$content = '<?php' . "\n" . $content;
	
		if( $handle = @fopen( $file, 'w+' ) )
		{
			fwrite( $handle, $content );
			fclose($handle);
		}
	}	
}