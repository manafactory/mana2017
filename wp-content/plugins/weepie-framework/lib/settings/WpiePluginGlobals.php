<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * Final class WpiePluginGlobals 
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpiePluginGlobals.php 64 2015-05-24 13:51:41Z Vincent Weber <vincent@webrtistik.nl> $
 * 
 * @since 0.1
 */
final class WpiePluginGlobals extends WpieBaseSettings {
	
	/**
	 * The Plugins namespace
	 * 
	 * @since 1.0
	 * 
	 * @var string
	 */
	private $nameSpace;
	
	
	/**
	 * The Plugins file
	 * 
	 * @since 1.0
	 * 
	 * @var string
	 */
	private $pluginFile;		
	

	/**
	 * Constructor
	 * 
	 * Calls parent class WpieBaseSettings
	 * 
	 * @access public
	 * 
	 * @param string $name
	 * @param string $nameSpace
	 * @param string $pluginFile
	 * 
	 * @since 0.1
	 */
	public function WpiePluginGlobals( $name = '', $nameSpace = '', $pluginFile = '' )
	{
		$this->nameSpace = $nameSpace;
		$this->pluginFile = $pluginFile;
		
		parent::WpieBaseSettings( $name );			
	}
	
	
	/* (non-PHPdoc)
	 * @see WpSettings::setDefaults()
	 * 
	 * @since 0.1
	 */
	public function setDefaults()
	{
		// WeePie Framework assets URLs		
		$wfPluginUri = plugins_url( '/weepie-framework' );
		$this->offsetSet( 'wfAssetsUri', $wfPluginUri . '/assets' );	
		$this->offsetSet( 'wfImgUri',  $this->offsetGet( 'wfAssetsUri' ) . '/img' );		
		
		$pluginBase = plugin_basename( $this->pluginFile );		
		$pluginDirName = ( preg_match( '/[^\/]+/', $pluginBase, $m ) ) ? $m[0] : '';		
		
		$this->offsetSet( 'pluginNameSpace', $this->nameSpace ); 
		$this->offsetSet( 'pluginDirName', $pluginDirName );
		$this->offsetSet( 'pluginFile', $pluginBase );
		$this->offsetSet( 'pluginFileBase', basename( $this->offsetGet( 'pluginFile' ), '.php' ) );
		
		$this->offsetSet( 'pluginUri', plugins_url( '/' . $pluginDirName ) );
		$this->offsetSet( 'pluginPath', WP_PLUGIN_DIR . '/' . $pluginDirName );
		$this->offsetSet( 'pluginPathFile', $this->pluginFile );
		$this->offsetSet( 'templatePath',  $this->offsetGet( 'pluginPath' ) . '/templates' );
		$this->offsetSet( 'templatePathTheme', array( get_stylesheet_directory(), get_stylesheet_directory().'/'.$this->nameSpace ) );		
		$this->offsetSet( 'modulePath',  $this->offsetGet( 'pluginPath' ) . '/modules' );
		$this->offsetSet( 'moduleUri',  $this->offsetGet( 'pluginUri' ) . '/modules' );	
		$this->offsetSet( 'cssUri',  $this->offsetGet( 'pluginUri' ) . '/css' );
		$this->offsetSet( 'jsUri',  $this->offsetGet( 'pluginUri' ) . '/js' );
		$this->offsetSet( 'imgUri',  $this->offsetGet( 'pluginUri' ) . '/img' );
		$this->offsetSet( 'jsNamespace', $this->nameSpace.'Data' );
		$this->offsetSet( 'transientModules', $this->nameSpace . '_modules' ); 
		$this->offsetSet( 'locale', ( '' !== ( $locale = get_locale() ) ) ? $locale : 'en_US' );	
	}		
}