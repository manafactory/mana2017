<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * Stack interface
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpieBaseSettings.php 66 2015-05-24 23:27:12Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 0.1
 */
interface Stack extends ArrayAccess {	
	
	public function getStack ();

	public function setStack( $data );
		
}

/**
 * iWpieSettings interface
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpieBaseSettings.php 66 2015-05-24 23:27:12Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 0.1
 */
interface iWpieSettings extends Stack {
	
	/**
	 * Save the settings to the database
	 */
	public function setOption();
	
	
	/**
	 * Set default settings to the stack
	 */
	public function setDefaults(); 
	
	/**
	 * Get the settings name
	 */
	public function getOptionName();
	
	/**
	 * Get the settings from the database
	 */
	public function getOption();
	
	
	/**
	 * Delete the settings from the database 
	 * 
	 * @param string $name
	 */
	public function deleteOption( $name = '' );
	
}

/**
 * WpieBaseSettings Class
 *
 * Base class for handeling WordPress Settings
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpieBaseSettings.php 66 2015-05-24 23:27:12Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 0.1
 */
abstract class WpieBaseSettings implements iWpieSettings {

	/**
	 * The name of the setting
	 * 
	 * This is the name that will be added to the 'wp_options' table 
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	protected $optionName;
	
	/**
	 * @var string the active language
	 *
	 * @since 1.1.3
	 *
	 * Default is en_US
	 */
	protected $locale;	

	/**
	 * @var array all languages
	 *
	 * @since 1.1.3
	 *
	 */
	protected $locales = array();	
	
	/**
	 * Flag if a locale is present
	 * 
	 * @since 1.1.3
	 * 
	 * @var bool
	 */
	protected $hasLocale = false;
	
	
	/**
	 * Flag if a multiple locales are present
	 * 
	 * @since 1.1.3
	 * 
	 * @var bool
	 */
	protected $hasMultipleLocales = false;
	
	
	/**
	 * Stack for holding all options 
	 * 
	 * @since 0.1
	 * 
	 * @var array
	 */
	protected $stack = array();
	
	/**
	 * Flag that indicates if settings are a first time or later
	 * 
	 * @since 0.1
	 * 
	 * @var bool
	 */	
	protected $isInit = false;
	
	

	/**
	 * Constructor
	 * 
	 * @param string $name
	 * 
	 * @since 0.1
	 */
	public function WpieBaseSettings( $name = '', $locale = '', $locales = array() )
	{		
		try {
			
			$this->_setOptionName( $name );
			
			$this->_setLocale( $locale );			
			$this->_setLocales( $locales );
			
			if( '' !== $this->locale )
				$this->hasLocale = true;
			
			if( is_array( $this->locales ) && !empty( $this->locales )  )
				$this->hasMultipleLocales = true;
			
			$option = $this->getOption();						
			
			if( false === $option )
			{
				$this->isInit = true;
				$this->setDefaults();				
				$this->mergeOptionTmp();				
				$this->setOption();
			}
			else {
				$this->isInit = false;
				$this->setStack( ( $this->hasMultipleLocales ) ? $option[$this->locale] : $option );		
			}			
		
		} catch( Exception $e ) {
		
			// only show Exception messages in the WP Dashboard
			if( is_admin() )
				echo "ERROR: {$e->getMessage()}<br/>";
		}
	}

	
	/* (non-PHPdoc)
	 * @see ArrayAccess::offsetExists()
	 * 
	 * @since 0.1
	 */
	public function offsetExists( $offset ) 
	{		
		return isset( $this->stack[$offset] );		
	}
	
	/**
	 * Get a single setting from the stack
	 *
	 * @access public
	 * 
	 * @param string $offset the settings name
	 * 
	 * @since 0.1
	 * 
	 * @return mixed
	 */
	public function offsetGet( $offset ) 
	{
		return $this->stack[$offset];
	}
		
	
	/**
	 * Get the settings name
	 *
	 * @access public
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	public function getOptionName()
	{
		return $this->optionName;
	}
	
	
	/**
	 * Get settings path
	 * 
	 * @access public
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}	
	
	
	/**
	 * Get the settings from the database
	 * 
	 * The settings option is a serialized array
	 *
	 * @access public
	 * 
	 * @uses get_option()
	 * 
	 * @since 0.1
	 * 
	 * @return object or bool false on failure
	 */
	public function getOption()
	{
		return get_option( $this->optionName );
	}
	
	
	/**
	 * Get the stack with all settings stored in it
	 *
	 * @access public
	 * 
	 * @since 0.1
	 * 
	 * @return array
	 */
	public function getStack()
	{
		return $this->stack;
	}	
	
	
	/**
	 * Add data to the stack
	 *
	 * @access public
	 * 
	 * @param string $offset the settings name
	 * @param mixed $value
	 * 
	 * @since 0.1
	 */
	public function offsetSet ( $offset, $value ) 
	{
		$this->stack[$offset] = $value;
	}		
	
	
	/**
	 * Save the settings to the database
	 * 
	 * The current settings in the stack are saved
	 * 
	 * If the current settings has multiple languages, save the option
	 * with the locales as array key
	 * 
	 * @uses add_option()
	 * 
	 * @since 0.1
	 */
	public function setOption()
	{
		$option = array();
		
		if( $this->hasMultipleLocales )
		{
			foreach( $this->locales as $locale )
			{
				$option[$locale] = $this->stack;			
			}
		} else {
			$option = $this->stack;
		}		
		
		add_option( $this->optionName , $option );
	}	
	
	
	/**
	 * Create a backup of the current setting
	 *
	 * Duplicate the settings and rename with _tmp appended
	 *
	 * @uses WpieBaseSettings::getOption()
	 * @uses add_option()
	 *
	 * @since 1.0.7
	 */
	public function setOptionTmp() {
	
		$current = $this->getOption();
	
		$option = $this->optionName . '_tmp';
	
		add_option( $option , $current );
	}
			
	
	/**
	 * Add data to the stack
	 *
	 * @access public
	 * 
	 * @param array $data
	 * 
	 * @since 0.1
	 */
	public function setStack( $data )
	{
		$this->stack = $data;
	}	
	
	
	/**
	 * Unset an entry in the stack
	 * 
	 * @access public
	 * 
	 * @param string offset
	 * 
	 * @since 0.1
	 */
	public function offsetUnset( $offset ) 
	{
		unset( $this->stack[$offset] );	
	}


	/**
	 * Delete the settings from the database 
	 * 
	 * @access public
	 * 
	 * @param string the settings name (optional)
	 * 
	 * @uses delete_option()
	 * 
	 * @since 0.1
	 * 
	 * @return bool true on succes or false on failure
	 */
	public function deleteOption( $name = '' )
	{
		$name = ( '' !== $name) ? $name : $this->optionName;
		
		return delete_option( $name );
	}


	/**
	 * Update the option in de database directly
	 * 
	 * Using this method instead of {@link WpieBaseSettings::setOption()} can be used to bypass the hooks used within WordPress function {@link add_option()} 
	 * 
	 * if a locale is active, update the locale only instead of the whole setting
	 * 
	 * @access public
	 * 
	 * @uses wpdb::update()
	 * 
	 * @since 0.1
	 * 
	 * @return Ambigous <number, false, boolean, mixed>
	 */
	public function updateOption( $locale = false )
	{
		global $wpdb;
		
		$locale = ( $locale && '' !== $locale ) ? $locale : $this->locale;
		
		$optionOld = $this->getOption();
		
		if( $this->hasLocale )
			$optionOld[$locale] = $this->stack;
		else 
			$optionOld = $this->stack;

		$option = maybe_serialize( $optionOld );

		// from wp-includes/option.php update_option() around line 299
		if ( ! defined( 'WP_INSTALLING' ) ) {
			$alloptions = wp_load_alloptions();
			if ( isset( $alloptions[$this->optionName] ) ) {
				$alloptions[$this->optionName] = $option;
				wp_cache_set( 'alloptions', $alloptions, 'options' );
			} else {
				wp_cache_set( $this->optionName, $option, 'options' );
			}			
			unset( $alloptions );
		}

		unset( $optionOld );
		
		return $wpdb->update( $wpdb->options, array( 'option_value' => $option ), array( 'option_name' => $this->optionName ) );
	}	

	
	/**
	 * Check if current settings option exist
	 * 
	 * @access public
	 * 
	 * @uses WpieBaseSettings::getOption() 
	 * 
	 * @since 1.1.7
	 * 
	 * @return bool
	 */
	public function hasOption() 
	{
		return ( $this->getOption() ) ? true : false;		
	}
	
	
	/**
	 * Merge temp settings option with current settings
	 * 
	 * This ensures to maintain previous settings when updating the Plugin
	 * 
	 * @access protected
	 * 
	 * @uses get_option()
	 * @uses WpieBaseSettings::getStack()
	 * @uses WpieBaseSettings::setStack()
	 * @uses WpieBaseSettings::deleteOption()
	 * 
	 * @since 1.0.7
	 */
	protected function mergeOptionTmp() {
		
		$option = $this->optionName . '_tmp';
		
		if( false !== ( $backup = get_option( $option ) ) ) {

			$mergedStack = array();
			
			foreach( $this->getStack() as $k => $v ) {
				
				if( isset( $backup[$k] ) )
					$mergedStack[$k] = $backup[$k];
				else 
					$mergedStack[$k] = $v;
			}
			
			$this->setStack( $mergedStack );

			$this->deleteOption( $option );
		}		
	}	
	
	
	/**
	 * Set the option name
	 * 
	 * @access private
	 * 
	 * @param string $name
	 * @throws Exception if $name is empty
	 * 
	 * @since 0.1
	 */
	private function _setOptionName( $name = '' )
	{
		if( '' === $name )
			throw new Exception( 'input parameter $name is empty.' );
		else
			$this->optionName = $name;
	}
	
	
	/**
	 * Set the locale
	 *
	 * @access private
	 *
	 * @param string $locale
	 * @throws Exception if $locale is not a string
	 *
	 * @since 1.1.3
	 */
	private function _setLocale( $locale )
	{
		if( !is_string( $locale ) )
			throw new Exception( 'input parameter $locale is not a string for setting: ' . $this->optionName );
		else 
			$this->locale = $locale;
	}

	/**
	 * Set the locales
	 *
	 * @access private
	 *
	 * @param string $locales
	 * @throws Exception if $locales is not an array
	 *
	 * @since 1.1.3
	 */
	private function _setLocales( $locales )
	{
		if( !is_array( $locales ) )
			throw new Exception( 'input parameter $locales is not an array for setting: ' . $this->optionName );
		else
			$this->locales = $locales;
	}	
}