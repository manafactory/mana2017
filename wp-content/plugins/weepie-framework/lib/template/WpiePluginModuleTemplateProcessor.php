<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpiePluginModuleTemplateProcessor Class
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpiePluginModuleTemplateProcessor.php 20 2015-01-15 14:22:29Z Vincent Weber <vincent@webrtistik.nl> $
 * 
 * @since 0.1
 */
class WpiePluginModuleTemplateProcessor {

	
	/**
	 * The templates file name
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	public $fileName;
	
	
	/**
	 * The templates default absolute path
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	public $defaultPath;
	
	
	/**
	 * Other optional path(s)
	 * 
	 * These path(s) can overide the default template path
	 * 
	 * @since 0.1
	 * 
	 * @var array
	 */
	public $otherPaths;
		
	
	/**
	 * Flag if template is found
	 * 
	 * @since 0.1
	 * 
	 * @var bool
	 */
	public $haveTemplate = false;

	
	/**
	 * The final template path 
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	public $templatePath = null;
	
	
	/**
	 * Constructor
	 * 
	 * @param string $fileName
	 * @param string $defaultPath
	 * @param (array|string) $otherPaths
	 * 
	 * @since 0.1
	 */
	public function WpiePluginModuleTemplateProcessor( $fileName = '', $defaultPath = '', $otherPaths = array() )
	{
		$this->fileName = $fileName;
		$this->defaultPath  = $defaultPath;
		$this->otherPaths = (array) $otherPaths;		
	}
	
	
	/**
	 * Get the templates file name
	 * 
	 * @since 0.1
	 * 
	 * @return string the file name
	 */
	public function getFileName()
	{
		return $this->fileName;
	}
	
	
	/**
	 * Get the templates default path
	 * 
	 * @since 0.1
	 * 
	 * @return string the defaultPath
	 */
	public function getDefaultPath()
	{
		return $this->defaultPath;
	}
	
	
	/**
	 * Get the optional templates path(s)
	 * 
	 * @since 0.1
	 * 
	 * @return array with paths
	 */
	public function getOtherPaths()
	{
		return $this->otherPaths;
	}	
	
	
	/**
	 * Choose a template 
	 * 
	 * First take a look at other path(s) locations. If a template file is found, this file will be used.
	 * Otherwise the default path will be used.
	 * 
	 * When a file is found, the {@link WpiePluginModuleTemplateProcessor::haveTemplate} flag is set to true.
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if a path is set, false otherwise
	 */
	public function choose()
	{		
		if( !empty( $this->otherPaths) ) 
		{			
			foreach( $this->otherPaths as $path )
			{
				if($template = realpath($path.'/'.$this->fileName)) 
				{				
					if( file_exists($template) ) 
					{
						$this->haveTemplate = true;
						$this->templatePath = $path;
						break;
					}				
				}					
			}			
		}
			
		if( false === $this->haveTemplate )
		{
			if($template = realpath($this->defaultPath.'/'.$this->fileName)) 
			{				
				if( file_exists($template) ) 
				{
					$this->haveTemplate = true;
					$this->templatePath = $this->defaultPath;					
				}				
			}				
		}

		return ( true === $this->haveTemplate ) ? $this->templatePath : false;		
	}	
}