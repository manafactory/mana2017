<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * iWpiePluginModule interface
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpiePluginModuleProcessor.php 40 2015-03-11 11:05:48Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 0.1
 */
interface iWpiePluginModule {
	
	/**
	 * callback for [PREFIX]_module_init hook
	 * 
	 * Every Module is hooked threw this init method with given $priority
	 * 
	 * @since 0.1
	 */
	public function init();			
}

/**
 * WpiePluginModule Class
 * 
 * Parent class for Plugin Modules
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpiePluginModuleProcessor.php 40 2015-03-11 11:05:48Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 0.1
 */
abstract class WpiePluginModule implements iWpiePluginModule {
	
	
	/**
	 * Flag if Module is active. 
	 * 
	 * If false, module is not loaded
	 * 
	 * @since 1.0
	 * 
	 * @var bool
	 */
	protected $active = true;
	

	/**
	 * The loading priority for the Module
	 * 
	 * 1 will load first, 2 after etc.
	 * 
	 * @since 0.1
	 * 
	 * @var int
	 */
	protected $priority = 10;
	
	
	/**
	 * The index for the Module
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	protected $index;
	
	
	/**
	 * Absolute path to the Module
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	protected $path;
	
	
	/**
	 * Uri to the module
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	protected $uri;
	
	
	/**
	 * Data that is passes to the Module
	 * 
	 * @since 1.0
	 * 
	 * @var array
	 */
	protected $vars;
	
	
	/**
	 * Set class member $status 
	 * 
	 * @access public
	 * @param bool $status
	 * 
	 * @since 1.0
	 */
	public function setActive( $status )
	{
		$this->active = $status;
	}

	
	/**
	 * Set class member $priority 
	 * 
	 * @access public
	 * @param int $priority
	 * 
	 * @since 0.1
	 */
	public function setPriority( $priority )
	{
		$this->priority = $priority;
	}
		
	
	/**
	 * Set class member $index 
	 * 
	 * @access public
	 * @param string $index
	 * 
	 * @since 0.1
	 */
	public function setIndex( $index )
	{
		$this->index = $index;
	}
	
	
	/**
	 * Set class member $path 
	 * 
	 * @access public
	 * @param string $path
	 * 
	 * @since 0.1
	 */
	public function setPath( $path )
	{
		$this->path = $path;
	}
	
	
	/**
	 * Set class member $uri 
	 *
	 * @access public
	 * @param string $uri
	 * 
	 * @since 0.1
	 */
	public function setUri( $uri )
	{
		$this->uri = $uri;
	}	
	
	
	/**
	 * Set class member $vars
	 * 
	 * @access public
	 * @param array $vars
	 * 
	 * @since 1.0
	 */
	public function setVars( $vars )
	{
		$this->vars = $vars;
	}	
	
	
	/**
	 * Get class member $active 
	 * 
	 * @access public
	 * 
	 * @since 1.0
	 * 
	 * @return bool
	 */
	public function getActive()
	{
		return $this->active;
	}
	
	
	/**
	 * Get class member $priority 
	 * 
	 * @access public
	 * 
	 * @since 0.1
	 * 
	 * @return int
	 */
	public function getPriority()
	{
		return $this->priority;
	}
	
	
	/**
	 * Get class member $index
	 *
	 * @access public
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	public function getIndex()
	{
		return $this->index;
	}		
	

	/**
	 * Get class member $path
	 * 
	 * @access public
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}	

	
	/**
	 * Get class member $uri
	 * 
	 * @access public
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	public function getUri()
	{
		return $this->uri;
	}	

	/**
	 * Get class member $vars
	 * 
	 * @access public
	 * 
	 * @since 1.0
	 * 
	 * @return array
	 */
	public function getVars()
	{
		return $this->vars;
	}	
	
}


/**
 * WpiePluginModuleProcessor Class
 *
 * Class for handling Plugin Modules inside the Plugin
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpiePluginModuleProcessor.php 40 2015-03-11 11:05:48Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 0.1
 */
final class WpiePluginModuleProcessor extends WpieBaseModuleProcessor {
	
	/**
	 * Unique Module prefix
	 * 
	 * Every Module needs this prefix in the file name
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	private $modulePrefix;
	
	/**
	 * Name of the transient that saves all founed Modules
	 * 
	 * @since 1.0
	 * 
	 * @var string
	 */
	private $transient;

	
	/**
	 * Constructor
	 * 
	 * @param string $transient
	 * @param string $rootPath
	 * @param string $rootUri
	 * @param string $ext
	 * @param string $ns
	 * 
	 * @access public
	 * 
	 * @throws Exception if the transient param is empty
	 * 
	 * @since 0.1
	 */
	public function WpiePluginModuleProcessor( $transient = '', $rootPath = '', $rootUri = '', $ext = '.php', $ns = '' ) 
	{
		try {			
			if( '' === $transient )
				throw new Exception( 'input parameter $transient is empty.' );			
			
			parent::WpieBaseModuleProcessor( $rootPath, $rootUri, $ext, $ns );
			
			$this->modulePrefix = $this->getNamespace() . '-module-';
				
		} catch( Exception $e ) {
		
			// only show Exception messages in the WP Dashboard 
			if( is_admin() )
				echo "ERROR: {$e->getMessage()}<br/>";
		}
				
		$this->transient = $transient;	
	}	
	

	/**
	 * Initialize all modules
	 * 
	 * Create class instances and set a WordPress Hook per module
	 * 
	 * @access public
	 * @param array $vars
	 * 
	 * @since 0.1
	 */
	public function init( $vars = array() )
	{
		if( !$this->hasModules )
			return;
		
		foreach( $this->moduleFiles as $dirName => $file )
		{			
			$className = $dirName;
			if( false !== strpos( $dirName, '-' ) )
			{			
				$strParts = explode( '-', $dirName );
				foreach( $strParts as $k => $str )
				{
					$strParts[$k] = ucfirst( $str );
				}				
				$className = join( '',$strParts );
			}
			
			$className = ucfirst( $this->getNamespace() ).ucfirst( $className );
						
			if( class_exists( $className ) )
			{				
				$module = new $className();
				
				if( is_object( $module ) && is_a( $module, 'WpiePluginModule' ) && method_exists( $module, 'init' ) )
				{	
					if( false === $module->getActive() )
						continue;					
					
					$module->setIndex( $dirName );
					$module->setPath( $this->getRootPath() . '/'. $file );
					$module->setUri( $this->getRootUri().'/'. $file );										
					
					if( !empty( $vars ) )
						$module->setVars( $vars );
					
					// add module to collection
					$this->setModule( $this->getNamespace(), $dirName, $module );					

					// add module priority to static priorities array 
					$this->setPriority( $dirName, $module->getPriority() );

					add_action( $this->getNamespace() . '_module_init', array( $module, 'init' ), $module->getPriority() );			
					unset( $module );
						
				} else {
					unset( $module );
				}
			}
		}	
	}
		
	
	/**
	 * Hook the modules into the plugin 
	 * 
	 * @access public
	 * @uses do_action()
	 * @uses remove_all_actions()
	 * 
	 * @since 0.1
	 */
	public function hook()
	{
		do_action( $this->getNamespace() . '_module_init' );
		
		// remove all attached action to ensure the callbacks are only called ones
		remove_all_actions( $this->getNamespace() . '_module_init' );		
	}

	
	/**
	 * Store the modules (files) in the database as a transient
	 * 
	 * @access public
	 * @param array $moduleFiles
	 * 
	 * @since 1.0
	 */
	public function setModulesTransient( $moduleFiles )
	{
		set_transient( $this->transient, $moduleFiles );	
	}	
	

	/**
	 * Get the transient with module files
	 * 
	 * @access public
	 * @uses get_transient()
	 * 
	 * @since 1.0
	 * 
	 * @return array with module files
	 */
	public function getModulesTransient()
	{
		return get_transient( $this->transient );		
	}
	
	
	/**
	 * Delete transient in database 
	 * 
	 * @access public
	 * @uses delete_transient()
	 * 
	 * @since 1.0
	 */
	public function deleteModulesTransient()
	{
		delete_transient( $this->transient );
	}		
	
	
	/* (non-PHPdoc)
	 * @see BaseModule::findModules()
	 * 
	 * If transient exist return this array else look in the folder 'modules'.
	 * Module files must begin with '$this->modulePrefix' to be included
	 *
	 * @access public
	 * @uses get_transient()
	 * @uses set_transient()
	 * 
	 * @since 0.1
	 */
	public function findModules()
	{
		if( false != ( $moduleFiles = $this->getModulesTransient() ) )
		{
			$this->moduleFiles = $moduleFiles;
			$this->hasModules = true;
		}
		else {
			$modulePath = parent::getRootPath();
			$moduleExt = parent::getExt();
				
			$modulesDir = @ opendir( $modulePath );
			$moduleFilesFound = array();
			$moduleFiles = array();
			if ( $modulesDir )
			{
				while ( ( $file = readdir( $modulesDir ) ) !== false )
				{
					if ( substr($file, 0, 1) === '.' )
						continue;
					if ( is_dir( $modulePath.'/'.$file ) )
					{
						$modulesSubdir = @ opendir( $modulePath.'/'.$file );
						if ( $modulesSubdir ) {
							while ( ( $subfile = readdir( $modulesSubdir ) ) !== false )
							{								
								if ( substr( $subfile, 0, 1 ) === '.' )
									continue;
								if ( substr( $subfile, -4 ) === $moduleExt && substr( $subfile, 0,  strlen( $this->modulePrefix ) ) === $this->modulePrefix )
								{	
									$moduleFilesFound[] = "$file/$subfile";
								}
							}
							closedir( $modulesSubdir );
						}
					} else {
						if ( substr( $file, -4 ) === $moduleExt && substr( $subfile, 0,  12 ) === $this->modulePrefix )
							$moduleFilesFound[] = $file;
					}
				}
				closedir( $modulesDir );
			}
				
			if ( !empty( $moduleFilesFound ) )
			{
				// init modules
				foreach ( $moduleFilesFound as $moduleFile ) {
					if ( !is_readable( "$modulePath/$moduleFile" ) )
						continue;
	
					if( preg_match( "/{$this->modulePrefix}([a-zA-Z_-]+){$moduleExt}/", $moduleFile, $matches ) )
					{
						if( $matches[1] )
							$moduleFiles[$matches[1]] = $moduleFile;					
					} else
						continue;								
				}
			}
				
			if( !empty( $moduleFiles ) )
			{
				// To make sure the directory listing is ASC, sort by the array key 
				ksort( $moduleFiles );
				
				$this->setModulesTransient( $moduleFiles );
				$this->moduleFiles = $moduleFiles;
				$this->hasModules = true;
			}
		}
	}		
}