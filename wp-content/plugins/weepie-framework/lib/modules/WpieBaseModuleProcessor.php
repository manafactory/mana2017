<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * iBaseModuleProcessor interface
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpieBaseModuleProcessor.php 64 2015-05-24 13:51:41Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 0.1
 */
interface iWpieModuleProcessor {
	
	public function setModule( $ns, $key, $module );
	public function setPriority( $key, $priority = 10 );
	public function getModule( $ns, $key );
	public function getModules( $ns );	
	public function includeModules();
			
}


/**
 * WpieBaseModuleProcessor Class
 * 
 * Class for handling Plugin Modules
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpieBaseModuleProcessor.php 64 2015-05-24 13:51:41Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 0.1
 */
abstract class WpieBaseModuleProcessor implements iWpieModuleProcessor {
	
	/**
	 * The root path for all Modules
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	protected $rootPath;
	
	
	/**
	 * The root URI for all Modules
	 * 
	 * @since 1.0
	 * 
	 * @var string
	 */
	protected $rootUri;
	
	
	/**
	 * The extension that all Modules should have
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	protected $ext;
	
	
	/**
	 * The namespace for all Modules
	 * 
	 * @since 1.0.3
	 * 
	 * @var string
	 */
	protected $ns;
	
	
	/**
	 * All founded Modules
	 * 
	 * @since 0.1
	 * 
	 * @var array
	 */
	protected $moduleFiles = array();
	
	
	/**
	 * Flag if any Modules are found
	 * 
	 * @since 1.0
	 * 
	 * @var bool
	 */
	protected $hasModules = false;
	
	
	/**
	 * All module priorities
	 * 
	 * @since 1.0
	 * 
	 * @var array
	 */
	public static $priorities = array();
	

	/**
	 * All Module instances
	 * 
	 * @since 0.1
	 * 
	 * @var array
	 */
	public static $modules = array();
	
	
	/**
	 * Constructor
	 * 
	 * @since 0.1
	 */
	public function WpieBaseModuleProcessor( $rootPath = '', $rootUri = '', $ext = '', $ns = '' )
	{
		$this->_setRootPath( $rootPath );
		$this->_setRootUri( $rootUri );
		$this->_setExt( $ext );
		$this->_setNamespace( $ns );
	}	
	
	
	/**
	 * Check if a Module exists
	 * 
	 * @access public
	 * 
	 * @param string $ns
	 * @param string $key
	 * 
	 * @since 1.1.3
	 * 
	 * @return bool
	 */
	public static function hasModule( $ns, $key )
	{
		return ( isset( self::$modules[$ns][$key] ) ) ? true : false;		
	}
	
	
	
	/**
	 * Set a module
	 * 
	 * @access public
	 * 
	 * @param string $ns
	 * @param string $key
	 * @param WpPluginModule $module
	 * 
	 * @since 0.1
	 */
	public function setModule( $ns, $key, $module )
	{
		self::$modules[$ns][$key] = $module;		
	}
	
	
	/**
	 * Set module priority to $priorities array
	 * 
	 * @param string $key
	 * @param int $module
	 * 
	 * @since 1.0
	 */
	public function setPriority( $key, $priority=10 )
	{
		self::$priorities[$key] = $priority;		
	}		
	

	/**
	 * Get all modules from the DB transient
	 * 
	 * @access protected
	 * @param string $transient
	 * 
	 * @since 0.1
	 */
	abstract public function findModules();
		
	

	/**
	 * Get all Module files
	 * 
	 * @since 0.1
	 * 
	 * @return array
	 */
	public function getModuleFiles() 
	{
		return $this->moduleFiles;
	}	
	
	
	/**
	 * Get a module from the stack
	 * 
	 * @access public
	 * 
	 * @param string $ns
	 * @param string $key
	 * 
	 * @since 0.1
	 * 
	 * @return WpPluginModule
	 */
	public function getModule( $ns, $key )
	{
		return self::$modules[$ns][$key];		
	}
	
	
	/**
	 * Get all Modules
	 * 
	 * @access public
	 * 
	 * @param string $ns
	 * 
	 * @since 0.1
	 * 
	 * @return array
	 */
	public function getModules( $ns )
	{	
		return self::$modules[$ns];		
	}
	
	
	/**
	 * Get root Path 
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected function getRootPath()
	{
		return $this->rootPath;		
	}
	
	
	/**
	 * Get root URI
	 *
	 * @since 1.0
	 *
	 * @return string
	 */
	protected function getRootUri()
	{
		return $this->rootUri;
	}	
	
	
	/**
	 * Get module generic ext (like .php)
	 *
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected function getExt()
	{
		return $this->ext;
	}
		
	
	/**
	 * Get module generic namespace
	 * 
	 * @access protected
	 * 
	 * @since 1.0.3
	 * 
	 * @return string
	 */
	protected function getNamespace()
	{
		return $this->ns;		
	}
	
	
	/**
	 * Include the module(s)
	 * 
	 * @access public
	 * 
	 * @since 0.1
	 */
	public function includeModules()
	{
		$moduleFiles = $this->moduleFiles;
		
		if( !empty( $moduleFiles ) )
		{
			foreach ( $moduleFiles as $file )
			{
				include_once "{$this->rootPath}/$file";		
			}
		}		
	}
	
		
	/**
	 * Set the root path to the modules folder
	 * 
	 * @access private
	 * @param string $rootPath
	 * @throws Exception if path is empty
	 * 
	 * @since 0.1
	 */
	private function _setRootPath( $path ) {
		if( '' === $path )
			throw new Exception( 'input parameter $rootPath is empty.' );
		else
			$this->rootPath = $path;
	}
	
	
	/**
	 * Set the URI to the modules folder
	 * 
	 * @access private
	 * @param string $rootPath
	 * @throws Exception if uri is empty
	 * 
	 * @since 1.0
	 */
	private function _setRootUri( $uri ) {
		if( '' === $uri )
			throw new Exception( 'input parameter $rootUri is empty.' );
		else
			$this->rootUri = $uri;
	}	
	
	
	/**
	 * Set the file extension of the module
	 * 
	 * @access private
	 * @param string $ext
	 * @throws Exception if ext is empty else string extension
	 * 
	 * @since 0.1
	 */
	private function _setExt( $ext ) {
		if( '' === $ext )
			throw new Exception( 'input parameter $ext is empty.' );
		else
			$this->ext = $ext;
	}	
	
	/**
	 * Set the namespace of the module
	 * 
	 * All modules should have the same prefix
	 * 
	 * @access private
	 * @param string $ns
	 * @throws Exception if namespace is empty else string prefix
	 * 
	 * @since 1.0.3
	 */
	private function _setNamespace( $ns )
	{
		if( '' === $ns )
			throw new Exception( 'input parameter $ns is empty.' );
		else
			$this->ns = $ns;		
	}
	
}