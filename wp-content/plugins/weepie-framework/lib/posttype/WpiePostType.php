<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpiePostType class
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpiePostType.php 38 2015-03-10 21:51:15Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 0.1
 */
final class WpiePostType {

	
	/**
	 * The name for the Post Type to register
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	private $postTypeStr = '';
	

	/**
	 * The arguments for registering the Post Type
	 * 
	 * @since 0.1
	 * 
	 * @var array
	 */
	private $argsRegister = array();
	
	
	/**
	 * The arguments for creating a new Post
	 * 
	 * @since 1.0
	 * 
	 * @var array
	 */
	private $argsNew = array();
	
	
	/**
	 * The registered Post Type object
	 * 
	 * @since 1.0
	 * 
	 * @var object
	 */
	private $posttypeObject;
	

	/**
	 * Constructor
	 *
	 * @access public
	 * @param string $postTypeStr the name for the Post Type to register
	 * 
	 * @since 0.1
	 */
	public function WpiePostType( $postTypeStr = '', $argsRegister = array(), $argsNew = array() )
	{
		$this->postTypeStr = $postTypeStr;
		
		if( !empty( $argsRegister ) ) {

			$this->_setArgsRegister( $argsRegister );
			
			if( !empty( $argsNew ) )
				$this->_setArgsNew( $argsNew );			
		}
	}
	
	
	/**
	 * Bind taxonomie(s) to an registered Post Type
	 * 
	 * @access public
	 * 
	 * @param (string|array) $taxonomies
	 *
	 * @uses register_taxonomy_for_object_type() 
	 * 
	 * @since 1.0
	 */
	public function bindTaxonmies( $taxonomies ) {
	
		foreach ( (array)$taxonomies as $tax ) {
				
			if( '' === $tax ) continue;
				
			register_taxonomy_for_object_type( $tax, $this->postTypeStr );
		}
	}	
	

	/**
	 * Register the Post Type
	 * 
	 * Saves the Post Type object to $this->posttypeObject
	 * 
	 * @access public
	 * @uses register_post_type()  
	 * 
	 * @since 0.1
	 */
	public function register()
	{
		$this->posttypeObject = register_post_type( $this->postTypeStr, $this->argsRegister );
	}


	/**
	 * Create a Post
	 * 
	 * @access public
	 * 
	 * @param array $args
	 * @param WP_Error $wp_error
	 * 
	 * @uses wp_insert_post()
	 * @uses is_wp_error()
	 * 
	 * @since 0.1
	 * 
	 * @return bool false on error or int $postId   
	 */
	public function create( $title = '', $args = array(), $wp_error = false )
	{	
		$defaults = $this->getArgsNew( $title );
		
		if( !empty( $args ) )
			$args = wp_parse_args( $args, $defaults );
		else
			$args = $defaults;
		
		$postId = wp_insert_post( $args, $wp_error );

		if( false == $postId || is_wp_error( $postId ) )
			return false;
		else
			return $postId;
	}


	/**
	 * Retrieve a Post
	 * 
	 * @param int $postId
	 * @param (int|bool) $reset
	 * 
	 * @since 0.1
	 * 
	 * @return WP_Query object
	 */
	public function retrieve( $postId = null, $reset = false )
	{
		$args = array
		(
			'p' => $postId,
			'post_type' => $this->postTypeStr,
			'posts_per_page' => 1
		);
		
		if( $reset )
			wp_reset_query();		
		
		return new WP_Query( $args );
	}


	/**
	 * Retrieve all Posts
	 * 
	 * @param (int|bool) $reset
	 * 
	 * @uses wp_reset_query() if $reset is true
	 * 
	 * @since 0.1
	 * 
	 * @return WP_Query object
	 */
	public function retrieveAll( $reset = false )
	{
		$args = array
		(
				'post_type' => $this->postTypeStr,
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'order' => 'ASC',
				'orderby' => 'ID'
		);
		
		if( $reset )
			wp_reset_query();
		
		return new WP_Query( $args );
	}
	
	
	/**
	 * Retrieve Posts with a custom query
	 * 
	 * @param array $args
	 * @param (int|bool) $reset
	 * 
	 * @uses wp_reset_query() if $reset is true
	 * @uses wp_parse_args() 
	 * 
	 * @since 0.1
	 *  
	 * @return bool false if $args is empty , WP_Query object otherwise
	 */
	public function retrieveCustom( $args = array(), $reset = false )
	{
		if( empty( $args ) )
			return false;
		
		$defaults = array
		(
				'post_type' => $this->postTypeStr,
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'order' => 'ASC'
		);		
		
		if( $reset )
			wp_reset_query();
		
		$args = wp_parse_args( $args, $defaults );
		
		return new WP_Query( $args );
	}

	
	/**
	 * Retrieve a Post fields
	 * 
	 * @param string $field the field to retrieve
	 * @param int|bool) $reset
	 * 
	 * @uses wp_reset_query() if $reset is true
	 * 
	 * @since 0.1
	 * 
	 * @return WP_Query object
	 */
	public function retrieveField( $field = 'ID', $reset = false )
	{
		$args = array
		(
				'post_type' => $this->postTypeStr,
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'order' => 'ASC'
		);

		if( 'ID' === $field )
		{
			$args['fields'] = 'ids';

		} else {
				
			// return all
				
			/*
			 WP now only supports 'ids' and '', if code is more flexible, we can use this:

			$this->customQueryField = $field;
				
			$args['suppress_filters'] = false;
				
			add_filter( 'posts_fields',	array(&$this, 'updatePostsFields') );
			*/
		}
		
		if( $reset )
			wp_reset_query();		
		
		return new WP_Query( $args );
	}
	
	
	
	/**
	 * Get registered Post Type object  
	 * 
	 * @since 1.0
	 * 
	 * @return object
	 */
	public function getPosttypeObject() 
	{
		return $this->posttypeObject;
	}
	
	
	/**
	 * Get the arguments for a new Post
	 * 
	 * @param string $title
	 * @uses add_magic_quotes()
	 * 
	 * @since 1.0
	 * 
	 * @return array, empty array if $title is empty
	 */
	public function getArgsNew( $title = '' )
	{
		if( '' === $title )
			return array();
		
		$args =  $this->argsNew;
		$args['post_title'] = $title;

		$args = add_magic_quotes( $args );
	
		return $args;
	}	
	

	/**
	 * Callback for the posts_fields hook
	 * 
	 * @param string $field
	 * 
	 * @since 0.1
	 * 
	 * @todo implement when WP is ready 
	 * 
	 * @return string
	 */
	public function updatePostsFields( $field )
	{
		$field = sprintf( 'wp_posts.%s', $this->customQueryField );		

		remove_filter( 'posts_fields', array( &$this, 'updatePostsFields' ) );
		
		return $field;
	}
	
	
	/**
	 * Fetch a WP_Query result
	 * 
	 * if $query {@link WP_Query::have_posts()} loop threw posts 
	 * 
	 * @param WP_Query $query
	 * 
	 * @uses WP_Query::have_posts()
	 * @uses WP_Query::the_post()
	 * 
	 * @since 0.1
	 * 
	 * @return array with Posts
	 */
	public function fetch( WP_Query $query )
	{
		$result = array();

		$all = ( isset( $query->posts[0]) && is_a( $query->posts[0], 'WP_Post' ) ) ? true : false;

		switch( $all )
		{
			case false:

				if( $query->have_posts() )
				{
					foreach( $query->posts as $k => $post )
					{
						$result[] = $post;
					}
				}

				break;
			case true:
			default:

				if( $query->have_posts() )
				{
					while ( $query->have_posts() )
					{
						$query->the_post();
						$result[$query->post->ID] = $query->post;
					}
				}

				break;
		}

		return $result;
	}


	/**
	 * Update a Post
	 * 
	 * @param int $postId, the Post ID to update
	 * @param array $args with update values
	 * @param bool $wp_error
	 * 
	 * @uses wp_insert_post()
	 * 
	 * @since 0.1
	 * 
	 * @return bool false on invalid $postId or int Post ID, 0 or WP_Error on failure
	 */
	public function update( $postId = 0, $args = array(), $wp_error = false, $forceGuid = false )
	{
		if( 0 !== $postId )
			$args['ID'] = $postId;
		else
			return false;
		
		$postId = wp_update_post( $args, $wp_error );
		
		if( false == $postId || is_wp_error( $postId ) ) {

			return false;
		}
		else {
			
			if( isset( $args['guid'] ) && true === $forceGuid ) {
					
				global $wpdb;
				$wpdb->update( "{$wpdb->prefix}posts" , array( 'guid' => $args['guid'] ), array( 'ID' => $postId ) );
			}			
			
			return $postId;		
		}
		//return wp_insert_post( $args, $wp_error );
	}


	/**
	 * Determine if registered Post Type has any Posts
	 * 
	 * @uses WpiePostType::retrieveAll() 
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if Posts are found, false otherwise
	 */
	public function hasPosts()
	{
		$posts = $this->retrieveAll();

		return $posts->have_posts();
	}


	/**
	 * Delete a Post
	 * 
	 * @param int $postid the ID of the Post to delete
	 * @param bool $force_delete, whether to bypass trash and force deletion
	 * 
	 * @since 0.1
	 * 
	 * @return Ambigous <multitype:, boolean, WP_Post, unknown, mixed, string, NULL>
	 */
	public function delete( $postid = 0, $force_delete = false )
	{
		return wp_delete_post( $postid, $force_delete );
	}
	

	/**
	 * Delete all Posts for the registered Post Type
	 * 
	 * @param bool $force_delete, whether to bypass trash and force deletion
	 * 
	 * @uses WpiePostType::fetch()
	 * @uses WpiePostType::retrieveField()
	 * @uses wp_delete_post()
	 * 
	 * @since 0.1
	 */
	public function deleteAll( $force_delete = false )
	{
		$posts = $this->fetch( $this->retrieveField() );

		foreach( $posts as $postid )
			wp_delete_post( $postid, $force_delete );
	}


	/**
	 * Set the arguments for registering the Post Type
	 * 
	 * @param array $args
	 * 
	 * @since 0.1
	 */
	private function _setArgsRegister( $args )
	{		
		$this->argsRegister	= $args;	
	}
	
	
	/**
	 * Set the arguments for creating a new Post
	 * 
	 * @param array $args
	 * 
	 * @since 1.0
	 */
	private function _setArgsNew( $args = array() )
	{
		$this->argsNew = $args;
	}	
}