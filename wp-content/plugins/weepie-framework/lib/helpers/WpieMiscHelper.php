<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpieMiscHelper class
 * 
 * Helper class that helps with various often uses functions
 *  
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpieMiscHelper.php 40 2015-03-11 11:05:48Z Vincent Weber <vincent@webrtistik.nl> $
 * 
 * @since 1.0.8
 */
class WpieMiscHelper {	
	

	/**
	 * Construct a post edit url based on a base url 
	 * 
	 * @access public
	 * 
	 * @param string $uriBase
	 * @param WP_Post $post
	 * 
	 * @uses add_query_arg()
	 * @uses admin_url()
	 * @uses esc_url()
	 * 
	 * @return bool false on failure, the escaped url otherwise
	 * 
	 * @since 1.0.8
	 */
	public static function getEditUri( $uriBase = '', $post = false, $args = array() ) 
	{	
		if( !is_a( $post, 'WP_Post' ) || '' === $uriBase )
			return false;
	
		$uri = add_query_arg( array( 'post' => $post->ID ), $uriBase );
		
		if( is_array( $args ) && !empty( $args ) ) {
			
			foreach ( $args  as $name => $value ) {
				
				if( is_string( $name ) && is_string( $value ) )				
					$uri = add_query_arg( array( $name => $value ), $uri );
			}
		}
		
		if( false === strpos( $uri, 'http://' ) )
			$uri = esc_url( admin_url( $uri ) );
		else
			$uri = esc_url( $uri );
	
		return $uri;
	}
	
	
	
	/**
	 * Get a file extension (without dot)
	 * 
	 * @access public
	 * 
	 * @param string $file
	 * 
	 * @since 1.0.8
	 *  
	 * @return bool false on failure, otherwise string the file extension 
	 */
	public static function getFileExt( $file = '' ) 
	{
		if( '' === $file || !is_string( $file ) )
			return false;
		
		
		$fileParts = explode( '.', $file );
		$ext = array_pop( $fileParts );
		
		return $ext;		
	}
	
}