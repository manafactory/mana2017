<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpieMultisiteHelper class
 * 
 * Helper class that helps with multisite operations
 *  
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpieMultisiteHelper.php 66 2015-05-24 23:27:12Z Vincent Weber <vincent@webrtistik.nl> $
 * 
 * @since 1.1
 */
class WpieMultisiteHelper {		

	/**
	 * Get all sites in the network
	 * 
	 * @access public
	 * 
	 * @since 1.1
	 * 
	 * @return bool false on failure or if not multisite. An array with blog_id and path values otherwise
	 */
	public static function getSites()
	{
		if( !is_multisite() )
			return false;
		
		global $wpdb;
	
		$query = $wpdb->prepare( "SELECT blog_id,path FROM $wpdb->blogs
				WHERE site_id = %d
				AND spam = '0'
				AND deleted = '0'
				AND archived = '0'
				AND path != '/'
				order by blog_id", $wpdb->siteid );
	
		$result = $wpdb->get_results( $query );
	
		if(null !== $result)
			return $result;
		else
			return false;
	}	
	
	
	/**
	 * Get a detail for the current blog
	 * 
	 * @access public
	 * 
	 * @param unknown_type $detail
	 * 
	 * @uses get_blog_details()
	 * 
	 * @since 1.1
	 * 
	 * @return bool false on failure/no multisite. Mixed the blog detail otherwise
	 */
	public static function getBlogDetail( $detail = 'blog_id' ) 
	{
		if( !is_multisite() )
			return false;
		
		$details = get_blog_details();
		
		return ( isset( $details->{$detail} ) ) ? $details->{$detail} : false;		
	}
	
	
	/**
	 * Determine if the current blog is the network home
	 * 
	 * @access public
	 * 
	 * @uses network_home_url()
	 * @uses get_bloginfo( 'url' )
	 * 
	 * @since 1.1
	 * 
	 * @return bool true or false
	 */
	function isNetworkHome()
	{
		if( network_home_url() === get_bloginfo( 'url' ) . '/' )
			return true;
		else
			return false;
	}
	
	
	/**
	 * Get the URI for the current page
	 *
	 * @access public
	 *
	 * @uses is_subdomain_install()
	 * @uses home_url()
	 * @uses network_home_url()
	 * @uses add_query_arg()
	 *
	 * @since 1.1.7
	 * 
	 * @return string
	 */
	public static function getCurrentUri()
	{
		if( !is_multisite() )
			return false;
	
		if( is_subdomain_install() )
			$uri = home_url( add_query_arg( NULL, NULL ) ); // subdomain
		else
			$uri = network_home_url( add_query_arg( NULL, NULL ) ); // subfolder
	
		return $uri;
	}		
}