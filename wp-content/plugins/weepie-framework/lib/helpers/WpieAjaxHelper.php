<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpieAjaxHelper class
 *
 * Helper class for AJAX processes
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpieAjaxHelper.php 38 2015-03-10 21:51:15Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.0
 */
class WpieAjaxHelper {
	
	
	/**
	 * Validate ajax return data
	 * 
	 * Data is valid to return when no errors, false values, empty values etc are found  
	 * 
	 * @access public 
	 * @param mixed $data
	 * 
	 * @since 1.0
	 * 
	 * @return string '1' if valid else '0'
	 */
	public static function isValidData( $data = false )
	{
		return ( !$data || self::hasWpError( $data ) || empty( $data ) || ( is_array( $data ) && in_array( false, $data, true ) ) ) ? '0' : '1';
	}

	
	/**
	 * Determine if ajax return data contains a WP_Error object
	 * 
	 * @access public
	 * 
	 * @param mixed $data
	 * 
	 * @uses WP_Error::is_wp_error()
	 * 
	 * @since 1.0.8
	 * 
	 * @return bool true or false
	 */
	public static function hasWpError( $data )
	{
		if( is_wp_error( $data ) ) {
			return true;
		}
		elseif( is_array( $data ) ) {
			
			foreach ( $data as $entry ) {

				if( is_wp_error( $entry ) )
					return true;				
			}			
		}		
	}
	
	
	/**
	 * Retrieve WP_Error messages found in ajax return data
	 * 
	 * @access public
	 * 
	 * @param mixed $data
	 * 
	 * @uses WP_Error::is_wp_error()
	 * @uses WP_Error::get_error_messages()
	 * 
	 * @since 1.0.8
	 * 
	 * @return array empty if no errors are found, otherwise array
	 */
	public static function getWpErrors( $data ) {
		
		$msg = array();
		
		if( is_wp_error( $data ) ) {
			$msg = $data->get_error_messages();
		}
		elseif( is_array( $data ) ) {
				
			foreach ( $data as $entry ) {
		
				if( is_wp_error( $entry ) ) {
					$msg = $entry->get_error_messages();
					break;
				}
			}
		}		

		return $msg;
	}	
}