<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpieFormHelper class
 * 
 * Helper class for rendering formfields
 * 
 * Depending on the type (e.a text, select, checkbox etc.) the corresponding HTML is returned
 *  
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpieFormHelper.php 61 2015-05-03 22:21:00Z Vincent Weber <vincent@webrtistik.nl> $
 * 
 * @since 0.1
 */
class WpieFormHelper {	
	
	/**
	 * The type of the formfield
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	protected static $type;	
	
	/**
	 * The name of the formfield
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	protected static $name;
	
	/**
	 * The namespace of the formfield
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	protected static $nameSpace;
	
	/**
	 * The value of the formfield
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	protected static $value;	
	
	/**
	 * HTML attributes
	 * 
	 * @since 0.1
	 * 
	 * @var array
	 */
	protected static $attributes;
	
	
	/**
	 * Element types that dont need the 'name' and 'value' attribute
	 * 
	 * @since 0.1
	 * 
	 * @var array
	 */
	protected static $attributesEscapeNameValue = array('div', 'buttonsubmitnoname', 'linkbutton');

	
	/**
	 * Element types that are 'buttons' 
	 *
	 * @since 0.1
	 *
	 * @var array
	 */
	protected static $attributesButton = array('buttonsubmitnoname', 'buttonsubmit', 'linkbutton');	
	
	
	/**
	 * Element types that don't need tabindex attribute
	 * 
	 * @since 1.1.5
	 * 
	 * @var array  
	 */
	protected static $tabindexEscapeFields = array('hidden', 'disabled', 'div');
		
	
	/**
	 * InnerHtml 
	 * 
	 * e.a. <a>{innerHtml}</a>
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	protected static $innerHtml;
	
	/**
	 * The options for a <select> element
	 * 
	 * @since 0.1
	 * 
	 * @var array
	 */
	protected static $selectOptions;
	
	/**
	 * Flag if current element has Attributes
	 * 
	 * @since 0.1
	 * 
	 * @var bool
	 */
	protected static $hasAttributes = false;
	
	/**
	 * Flag if current element has innerHtml
	 * 
	 * @since 0.1
	 * 
	 * @var bool
	 */
	protected static $hasInnerHtml = false;
	
	/**
	 * Flag if current element is a <select> field
	 * 
	 * @since 0.1
	 * 
	 * @var bool
	 */
	protected static $isSelect = false;
	
	
	/**
	 * Counts the number of fields during the current page request
	 * 
	 * @since 1.1.5
	 * 
	 * @var int
	 */
	protected static $fieldCount = 1;
	
	
	/**
	 * Flag if current element is a disabled field
	 * 
	 * i.e. an attribute disabled="disabled" is added
	 * 
	 * @since 1.0
	 *  
	 * @var bool
	 */
	protected static $isDisabled = false;
	
	/**
	 * Placeholder for element attributes
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	const ATTR_MASK = '{attr}';
	
	/**
	 * Placeholder for element attributes
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	const NAME_MASK = '{name}';
	
	/**
	 * Placeholder for element attributes
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	const VALUE_MASK = '{value}';
	
	/**
	 * Placeholder for element attributes
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	const SELECT_OPTIONS_MASK = '{options}';
	
	/**
	 * Placeholder for element attributes
	 * 
	 * @var string
	 */
	const INNER_HTML_MASK = '{innerhtml}';
	
	/**
	 * HTML string for case selected with, e.a. <option selected="selected"></option> 
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	const SELECTED_STR = 'selected="selected"';
	
	/**
	 * HTML string for case checked with, e.a. <input type="radio" checked="checked" />  
	 * 
	 * @since 0.1
	 * 
	 * @var string
	 */
	const CHECKED_STR = 'checked="checked"';
	
	
	/**
	 * Handle the formfield based on type
	 * 
	 * @acces public
	 * 
	 * @param string $type the type of the formfield
	 * @param string $name the name of the formfield
	 * @param string $value the value of the formfield (optional)
	 * @param array $attributes the HTML attributes (optional)
	 * @param string $innerHtml the innerHtml (optional)
	 * @param array $selectOptions selectbox options (optional)
	 * @param bool $render (optional)
	 * 
	 * @since 0.1
	 * 
	 * @return string the formfield returned by self::render() 
	 */
	public static function formField( $type='', $name='', $value='', $nameSpace='', $attributes=array(), $innerHtml='', $selectOptions=array(), $render=true )
	{		
		if( '' === (string)$type )
			return 'No formfield type defined.';
		
		self::$type = (string)$type;	
		
		self::$name = (string)$name;
		
		self::$nameSpace = ( '' !== $nameSpace ) ? $nameSpace : false;
		
		self::$value = stripslashes((string)$value);
		
		if( !empty($attributes) )
		{
			self::$hasAttributes = true;
			self::$attributes = $attributes;		
		}		
		
		self::$isDisabled = ( in_array( 'disabled', $attributes ) || 'disabled' === self::$type );
		
		if( !empty($innerHtml) )
		{
			self::$hasInnerHtml = true;
			self::$innerHtml = $innerHtml;
		}		
		
		if( 'textarea' === self::$type )
		{
			self::$hasInnerHtml = true;
			self::$innerHtml = self::$value;
		}
		
		if( !empty( $selectOptions ) )
		{
			self::$isSelect = true;
			self::$selectOptions = $selectOptions;		
		} 
		
		self::_prepareAttributes();
		
		if( true === $render )
			return self::_render();		
	}	

	
	/**
	 * Wether a selectbox is selected or not
	 * 
	 * @access public
	 * 
	 * @param string $currentValue
	 * @param string $value
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function isSelected( $currentValue, $value='' )
	{
		if( isset($value) && '' !== $value )
		{
			if(is_string($value))
				$currentValue=strval($currentValue);
				
			if(is_int($value))
				$currentValue=intval($currentValue);
				
			if(is_bool($value))
				(boolean) intval($currentValue);
				
			return ($currentValue === $value);
	
		}else
			return false;
	}	
	
	
	/**
	 * Wether a checkbox is checked or not
	 * 
	 * @access public
	 * 
	 * @param string $value
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function isChecked( $value='' )
	{
		if( isset($value) ){
	
			if(is_string($value))
				$value=intval($value);
	
			(boolean) $value;
	
			return (true == $value);
	
		}else
			return false;
	}	
	
	
	/**
	 * Prepare attributes bases on type
	 * 
	 * @since 0.1
	 * 
	 * @access protected
	 */
	protected static function _prepareAttributes()
	{		
		self::$attributes['id'] = ( isset( self::$attributes['id'] ) ) ? self::$attributes['id'] : self::$name;
		self::$attributes['class'] = ( isset( self::$attributes['class'] ) && '' !== self::$attributes['class'] ) ? self::$attributes['class'] : '';
		self::$attributes['class'] = ( 'color' === self::$type ) ? self::$attributes['class'].' colorinput' : self::$attributes['class'];
		
		if( self::$isDisabled ) 
		{
			self::$attributes['class'] = ( isset( self::$attributes['class'] ) && '' !== self::$attributes['class'] ) ? self::$attributes['class'].' disabled' : 'disabled';
		}
		
		if( !in_array( self::$type, self::$attributesEscapeNameValue ) )
		{
			self::$attributes['name'] =  ( false === self::$nameSpace || ( false !== strpos( self::$name, '[' ) ) ) ? self::$name : self::$nameSpace.'['.self::$name.']';
			self::$name = self::$attributes['name'];
			self::$attributes['value'] =  ( '' !== self::$value ) ? self::$value : '';
		}		

		if( 'textarea' === self::$type )
		{
			unset( self::$attributes['value'] );
		}
		
		if( 'checkbox' === self::$type )
		{
			if( self::isChecked(self::$value) )
			{
				self::$attributes['checked'] = 'checked';
				self::$attributes['class'] = ( isset( self::$attributes['class'] ) && '' !== self::$attributes['class'] ) ? self::$attributes['class'].' checked' : 'checked';
			}
		}	

		if( in_array( self::$type, self::$attributesButton ) )
		{
			self::$attributes['class'] = ( isset( self::$attributes['class'] ) && '' !== self::$attributes['class'] ) ? self::$attributes['class'].' button' : 'button';
		}

		if( !in_array( self::$type, self::$tabindexEscapeFields ) ) {
			self::$attributes['tabindex'] = self::$fieldCount;
		}
	}	
	
	
	/**
	 * Substitute the attributes into the formfield
	 * 
	 * For HTML selectboxes and checkboxes, a 'checked' class is added
	 * For HTML buttons, a 'button' class is added
	 *
	 * @access protected
	 *  
	 * @param object $field (passed by reference)
	 * 
	 * @since 0.1
	 * 
	 * @return void
	 */
	protected static function _substituteAttributes( &$field )
	{		
		if( preg_match_all('/{([a-zA-Z]+?)}/', $field, $matches) )
		{
			foreach( $matches[0] as $k => $attr ){
								
				switch($attr)
				{
					case  self::ATTR_MASK:
						$field = preg_replace("/$attr/", self::_getAttributeString(), $field);
						break;
					
					case  self::SELECT_OPTIONS_MASK:
						$field = preg_replace("/$attr/", self::_getSelectOptions(self::$selectOptions) , $field);
						break;
					
					case  self::INNER_HTML_MASK:
						$field = preg_replace("/$attr/", self::$innerHtml, $field);
						break;	
					
					default:				
						$field = preg_replace("/$attr/", self::$attributes[$matches[1][$k]], $field);
						break;									
				}									
				
			}			
		}		
	}		
	

	/**
	 * Manually substitute an attribute
	 * 
	 * @access protected
	 * 
	 * @param string $field the HTML string for a field (passed by reference)
	 * @param string $mask the string to replace
	 * @param string $replace the string to replace the mask with
	 * 
	 * @since 1.0
	 */
	protected static function _substituteAttributesManual( &$field, $mask='', $replace='' ) 
	{
		if('' !== $mask)
			$field = preg_replace("/$mask/", $replace, $field);		
	}
	
	
	/**
	 * Get the HTML for input type text
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getInputText()
	{
		return '<input '.self::ATTR_MASK.' type="text" />';		
	}
	
	
	/**
	 * Get the HTML for input type text with no name and value attribute
	 * 
	 * @access protected
	 * 
	 * @since 1.0
	 * 
	 * @return string
	 */
	protected static function _getInputTextNoNameValue()
	{
		return '<input '.self::ATTR_MASK.' type="text" />';		
	}
		
	
	/**
	 * Get the HTML for input type checkbox
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getInputCheckbox()
	{
		return '<input '.self::ATTR_MASK.' type="checkbox" value="1" />';	
	}
	
	
	/**
	 * Get the HTML for input type radio
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getInputRadio()
	{
		return '<input '.self::ATTR_MASK.' type="radio" />';	
	}
		
	
	/**
	 * Get the HTML for input type file
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getInputFile()
	{
		return '<input '.self::ATTR_MASK.' type="file" />';	
	}	
	
	
	/**
	 * Get the HTML for input type hidden
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getInputHidden()
	{
		return '<input '.self::ATTR_MASK.' type="hidden" />';	
	}	
	
	
	/**
	 * Get the HTML for input type text that is disabled
	 *  
	 * @access protected
	 * 
	 * @since 1.0
	 * 
	 * @return string
	 */
	protected static function _getInputDisabled() 
	{		
		return '<input '.self::ATTR_MASK.' type="text" disabled="disabled" />';	
	}
	
	
	/**
	 * Get the HTML for input type submit
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getInputSubmit()
	{
		return '<input '.self::ATTR_MASK.' type="submit" />';
	}	
	
	
	/**
	 * Get the HTML for button type submit
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getButtonSubmit()
	{
		return '<button '.self::ATTR_MASK.' type="submit" name="'.self::NAME_MASK.'">'.self::INNER_HTML_MASK.'</button>';
	}	
	
	
	/**
	 * Get the HTML for button type submit
	 *
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getButtonSubmitNoName()
	{
		return '<button '.self::ATTR_MASK.' type="submit">'.self::INNER_HTML_MASK.'</button>';
	}
		
	
	/**
	 * Get the HTML for input type button
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getInputButton()
	{
		return '<input '.self::ATTR_MASK.' type="button" />';
	}
	
	
	/**
	 * Get the HTML for a link button
	 * 
	 * @access protected
	 * 
	 * @since 1.0
	 * 
	 * @return string
	 */
	protected static function _getLinkButton()
	{
		return '<a '.self::ATTR_MASK.'>'.self::INNER_HTML_MASK.'</a>';
	}	
	

	/**
	 * Get the HTML for a selectbox
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getSelect()
	{
		$hiddenSpan = (self::$isDisabled) ? '<span class="disabled-value">'.self::$selectOptions[self::$value].'</span>' : '';
		
		return '<select '.self::ATTR_MASK.'>'.self::SELECT_OPTIONS_MASK.'</select>'.$hiddenSpan;	
	}
	
	/**
	 * Get the HTML for a textarea
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getTextarea()
	{					
		return '<textarea '.self::ATTR_MASK.'>'.self::INNER_HTML_MASK.'</textarea>';	
	}	
	
	/**
	 * Get the HTML for a div element
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getDiv()
	{
		return '<div '.self::ATTR_MASK.'>'.self::INNER_HTML_MASK.'</div>';	 		
	}
	
	
	/**
	 * Get the HTML for a colorpicker element
	 *
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	protected static function _getColorPicker()
	{
		return self::_getInputText();
	}
	
	
	/**
	 * Reset all class members to defaults
	 * 
	 * @access protected
	 * 
	 * @since 0.1
	 * 
	 * @return void
	 */
	protected static function _reset()
	{
		self::$type='';
		self::$name='';
		self::$nameSpace='';
		self::$value='';			
		self::$attributes=array();
		self::$innerHtml='';
		self::$selectOptions=array();
		self::$hasAttributes = false;
		self::$hasInnerHtml = false;
		self::$isSelect = false;
		self::$isDisabled = false;
	}		
	
	
	/**
	 * Get selectbox options
	 * 
	 * @access private
	 *  
	 * @param array $options
	 * 
	 * @uses self::isSelected to detetmine the selected option
	 * 
	 * @since 0.1
	 * 
	 * @return bool false with no options or string the options HTML
	 */
	private static function _getSelectOptions( $options=array() )
	{		
		if( empty($options) )
			return false;
		
		$value = self::$attributes['value'];

		$optionsStr = "";
		foreach( $options as $currentValue => $v )
		{
			$selected = ( self::isSelected($currentValue, $value) )? ' '.self::SELECTED_STR:'';

			$optionsStr .= "<option value=\"$currentValue\"$selected>".$v."</option>";
		}

		return $optionsStr;		
	}
		
	
	/**
	 * Get the attributes string
	 * 
	 * For selectboxes, checkboxes and 'private' attributes the 'value' attribute is escaped
	 * Attributes prefixed with a '_' (underscore) are escaped
	 * 
	 * @access private  
	 * 
	 * @since 0.1
	 * 
	 * @return string the attributes
	 */
	private static function _getAttributeString()
	{		
		$str = '';
		foreach(self::$attributes as $attr => $v)
		{
			if( 'select' === self::$type && 'value' === $attr )
				continue;
			elseif( 'checkbox' === self::$type && 'value' === $attr )
				continue;
			elseif( 0 === strpos($attr, '_') ) // private attributes
				continue;
			else			
				$str .= " $attr=\"$v\"";
		}
		
		return $str;		
	}	


	/**
	 * Render the formfield
	 * 
	 * @access private 
	 * 
	 * @since 0.1
	 * 
	 * @return string the formfield or an error message
	 */
	private static function _render()
	{
		switch( self::$type )
		{
			case 'text':
				$field = self::_getInputText();
				break;
				
			case 'textnoname':
				$field = self::_getInputTextNoNameValue();
				break;
					
			case 'checkbox':
				$field = self::_getInputCheckbox();
				break;
					
			case 'radio':
				$field = self::_getInputRadio();
				break;
					
			case 'file':
				$field = self::_getInputFile();
				break;
						
			case 'hidden':
				$field = self::_getInputHidden();
				break;
				
			case 'disabled':
				$field = self::_getInputDisabled();
				break;	
				
			case 'submit':
				$field = self::_getInputSubmit();
			break;	

			case 'buttonsubmit':
				$field = self::_getButtonSubmit();
				break;		

			case 'buttonsubmitnoname':
				$field = self::_getButtonSubmitNoName();
				break;				
	
			case 'button':
				$field = self::_getInputButton();
				break;

			case 'linkbutton':
				$field = self::_getLinkButton();
				break;					
			
			case 'select':
				$field = self::_getSelect();
				break;
					
			case 'textarea':
				$field = self::_getTextarea();
				break;
				
			case 'div':
				$field = self::_getDiv();
				break;

			case 'color':
			case 'colorpicker':	
				$field = self::_getColorPicker();
				break;		

			default:
				return sprintf( 'Invalid formfield type: %s', self::$type);
				break;
		}	

		self::_substituteAttributes( $field );		
	
		// flush members
		self::_reset();
		
		self::$fieldCount++;
	
		return $field;
	}		
}


/**
 * WpieWpFormHelper Class 
 * 
 * Helper class for rendering WordPress specific formfields
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpieFormHelper.php 61 2015-05-03 22:21:00Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 0.1
 */
class WpieWpFormHelper extends WpieFormHelper {

	
	/**
	 * Placeholder for filename value
	 * 
	 * @since 1.0
	 * 
	 * @var string
	 */
	const VALUE_MASK_FILENAME = '{filename}';		
	

	/**
	 * file name that is used by the file/image upload fields
	 * 
	 * @since 1.0
	 * 
	 * @var string 
	 */
	private static $filename = '';
	
	
	/**
	 * the file extension based on the filename
	 * 
	 * @since 1.1.x
	 * 
	 * @var string
	 */
	private static $fileExt = '';
	
	
	/**
	 * Element types that dont need the 'name' and 'value' attribute
	 * 
	 * @since 1.0
	 * 
	 * @var array
	 */
	protected static $attributesEscapeNameValue = array('wptermlist_edit');
	
	
	/**
	 * Element types that don't need tabindex attribute
	 *
	 * @since 1.1.5
	 *
	 * @var array
	 */
	protected static $tabindexEscapeFields = array();	
	
	
	
	/* (non-PHPdoc)
	 * @see FormHelper::formField()
	 * 
	 * @since 0.1
	 */
	public static function formField( $type='', $name='', $value='', $nameSpace='', $attributes=array(), $innerHtml='', $selectOptions=array(), $render=true )
	{
		parent::$attributesEscapeNameValue = array_unique(array_merge(parent::$attributesEscapeNameValue, self::$attributesEscapeNameValue));		
		
		parent::formField( $type, $name, $value, $nameSpace, $attributes, $innerHtml, $selectOptions, false );	
		
		self::_prepareAttributes();
		
		if( 'wptextareabasic' === parent::$type )
		{
			parent::$hasInnerHtml = true;
			parent::$innerHtml = parent::$value;
		}		
		
		if( ( 'wpfile' === parent::$type || 'wpfilebasic' === parent::$type || 'wpimage' === parent::$type ) && '' !== parent::$value ) {
			
			self::$filename = esc_html( wp_basename( parent::$value ) );
			self::$fileExt	= WpieMiscHelper::getFileExt( self::$filename );					
		}
			
		return self::_render();	
	}	
	
	
	/**
	 * Get a taxonomy selectbox with terms
	 * 
	 * See the documentation for {@link wp_dropdown_categories()}
	 * 
	 * @param string $id
	 * @param string $name
	 * @param string $orderby
	 * @param (bool|int) $echo
	 * @param int $selected
	 * @param string $tax
	 * @param array $args
	 * 
	 * @uses wp_parse_args()
	 * @uses wp_dropdown_categories()
	 * 
	 * @since 0.1
	 * 
	 * @return Ambigous <string, mixed>
	 */
	public static function getTermDropdown( $id, $name, $orderby='ID', $echo=1, $selected=0, $tax='category', $args=array() )
	{
		$defaults = array(
				'orderby'            => $orderby,
				'echo'               => $echo,
				'selected'           => $selected,
				'name'               => $name,
				'id'                 => $id,
				'taxonomy'           => $tax,
				'hide_empty'		 => 0	
		);		
		
		if( !empty($args) ) 
		{
			$args = wp_parse_args($args, $defaults);
		} else {			
			$args = $defaults;			
		}
		
		if( $echo )
			wp_dropdown_categories( $args );
		else
			return wp_dropdown_categories( $args );		
	}
	
	
	/**
	 * Get a HTML list with taxonomy terms
	 * 
	 * @param string $tax, the taxonomy to retrieve the terms for
	 * @param string $orderby
	 * @param array $args optional arguments to pass to get_terms()
	 * 
	 * @uses wp_parse_args()
	 * @uses get_terms()
	 * @uses WpieWpFormHelper::_getTermListRow()
	 * 
	 * @since 1.0
	 * 
	 * @return string
	 */
	public static function getTermList( $tax='category', $orderby='ID', $args=array() )
	{		
		$defaults = array(
				'orderby'   => $orderby,
				'taxonomy'  => $tax,
				'get'		=> 'all'
		);		
		
		if( !empty($args) ) 
		{
			$args = wp_parse_args($args, $defaults);
		} else {			
			$args = $defaults;			
		}

		$terms = get_terms( $tax, $args );
		
		if( empty($terms) || is_wp_error($terms) ) 
			return sprintf('No term list possible for taxonomy "%s".', $tax);	
		
		$list = '';
		
		$list .= '<ul '.parent::ATTR_MASK.'>';
		foreach($terms as $term) {
			
			$list .= sprintf(self::_getTermListRow(), $term->term_id, $term->term_id, $term->name, $term->name);
			
		}
		$list .= '</ul>';
		
		return $list;
	}
	
	
	/**
	 * Get a selectbox with Post Types
	 * 
	 * @uses get_post_types()
	 * 
	 * @since 1.0
	 * 
	 * @return string
	 */
	public static function getPostTypesDropDown()
	{
		$args = array('public' => true);
		
		parent::$selectOptions = get_post_types($args);		

		$select = '<select '.parent::ATTR_MASK.'>'.parent::SELECT_OPTIONS_MASK.'</select>';

		return $select;		
	}
	
	
	/**
	 * Get the HTML for a selectbox with pages
	 * 
	 * @param string $id
	 * @param string $name
	 * @param string $sortColumn
	 * @param (bool|int) $echo
	 * @param int $selected
	 * 
	 * @uses wp_dropdown_pages()
	 * 
	 * @since 0.1
	 * 
	 * @return Ambigous <string, mixed>
	 */
	public static function getPageDropdown( $id, $name, $sortColumn='ID', $echo=1, $selected=0  )
	{
		$args = array(
				'id' => $id,
				'name' => $name,
				'sort_column' => $sortColumn,
				'echo' => $echo,
				'show_option_no_change' => '-- '. __( 'Select', 'weepie' ) .' --',
				'selected' => $selected
		);
	
		if( $echo )
			wp_dropdown_pages( $args );
		else
			return wp_dropdown_pages( $args );
	}	
	
	
	/**
	 * Enqueue WordPress media scripts
	 * 
	 * @access public
	 * 
	 * @uses wp_enqueue_media()
	 *  
	 * @since 1.1.5
	 */
	public static function enqueueMediaScripts()
	{
		static $isEnqueued = false;
		
		if( !$isEnqueued ) {

			wp_enqueue_media();
			$isEnqueued = true;		
		}
	}
	
	
	/**
	 * Callback for the admin_print_footer_scripts hook 
	 * 
	 * Print jQuery JavaScript that is needed for an image/file upload field
	 * 
	 * The code can handle multiple uploader instances at one page
	 * 
	 * To force extensions, add the HTML data attribute 'data-force-ext' with comma separated extensions that are allowed, simplified example: <input type="file" data-force-ext="png,jpg">
	 * 
	 * @since 1.0
	 */
	public static function printInputImageJs()
	{		
		static $isPrinted = false;
		
		if( !$isPrinted ):
		?>
		<script type='text/javascript'>
		jQuery(function($) {

		// prevent the media popup triggering when hitting 'Enter' key	
		$('.wrap').on('keypress', function(e) {
			if (13 === (e.keyCode || e.which) && !$(e.target).hasClass('wp_upload_button') ) return false;
		});
			
		// code from http://www.webmaster-source.com/2013/02/06/using-the-wordpress-3-5-media-uploader-in-your-plugin-or-theme/
		// Slightly modified
		var custom_uploader;  
	  
	  	$('.wrap').on('click', '.wp_upload_button', function(e) {
			e.preventDefault();
	      
	     	var thiz = $(this),
      		  	type = thiz.data('button-type'),
      		  	input = thiz.siblings('input.wp'+type),
      		  	filenameEl = thiz.siblings('.filename'),
      		  	previewEl = thiz.siblings('.preview'),
      		  	haveForcedExt = false,
      		  	showPreview = false;      		  	
  		  	
			// get forced extensions
			if(input.data('force-ext')) {
				var extstr = input.data('force-ext'),
				forcedext = extstr.split(','),
				haveForcedExt = true;    			
			}
			
			if(input.data('show-preview')) {				
				showPreview = (true == input.data('show-preview')) ? true : false;

				var previewMaxWh = parseInt(input.data('preview-max-wh'));
				previewMaxWh = (!isNaN( previewMaxWh )) ? previewMaxWh : false;								
			}    	
	
	      	// If the uploader object has already been created, reopen the dialog
	      	
	      	custom_uploader = ( 'undefined' !== typeof thiz.data('custom_uploader')) ? thiz.data('custom_uploader') : false;
	      	
	      	if ( custom_uploader ) {
	      		custom_uploader.open();
	        	return;
	      	}
	      	
      		// Extend the wp.media object
      		custom_uploader = wp.media.frames.file_frame = wp.media({
          		title: 'Choose '+type,
	          	button: {
	              	text: 'Choose '+type
	          	},
	          	multiple: false
	      	});
      		thiz.data('custom_uploader', custom_uploader);	      
	
	      	// When a file is selected, grab the URL and set it as the text field's value
	      	custom_uploader.on('select', function() {
	          attachment = custom_uploader.state().get('selection').first().toJSON();
	
		       // filename
				var name = attachment.url.split('/').pop(),
					relUrl = attachment.url.split('/uploads/').pop();

				input.data('attachment-id', attachment.id);
				input.attr('data-attachment-id', attachment.id);
	          
	          	if(haveForcedExt) {
	          		// get ext from url
					var ext = attachment.url.split('.').pop();
	
					if(ext && -1 !== forcedext.indexOf(ext)) {
	
						input.val(relUrl);
						filenameEl.html(name);
						
					}	else {
							alert('Only files with "'+extstr+'" extension are allowed');
						}					
	          		
	          	} else {
		          	
	          		input.val(relUrl);

					if(showPreview) {

						var preview = $('<img src="'+attachment.url+'" />');
						
						if(previewMaxWh && attachment.width < previewMaxWh && attachment.height < previewMaxWh)
							preview.attr({ 'width':attachment.width, 'height':attachment.height });
						else 
							preview.attr({ 'width':'100%', 'height':'100%' });
						
						previewEl.append(preview);						
						
					} else {	          		
						filenameEl.html(name);
					}		          	
	          	}	
	      	});
	
	      // Open the uploader dialog
	      custom_uploader.open();
	  });  	
	});
	</script>
	<?php 
	$isPrinted = true;
	endif;				
	}	
	
	
	/**
	 * Callback for the admin_print_footer_scripts hook 
	 * 
	 * Print jQuery JavaScript that is needed for a  basic file upload field
	 * 
	 * To force extensions, add the HTML data attribute 'data-force-ext' with comma separated extensions that are allowed, simplified example: <input type="file" data-force-ext="docx,pdf">
	 * 
	 * @todo check if using .on() is needed
	 * 
	 * @since 1.0
	 */
	public static function printInputFileJs()
	{		
		?>
		<script type='text/javascript'>
		jQuery(function($) {

			$('.wpie-change-file').click(function(e) {

				$(this).prev('.wpfilebasic').trigger('click');			
			});
			
		  $('.wpfilebasic').change(function(e) {
		  	
	      e.preventDefault();
	      
	      var input = $(this),
	      		filename = input.val(),
	      		filenameEl = input.siblings('.filename');
	      
    		// get forced extensions
    		if(input.data('force-ext')) {

				var extstr = input.data('force-ext'),
    				forcedext = extstr.split(',');
    			
				// get ext from url
				var ext = filename.split('.')[1];		
				
				// filename
				var name = filename.split('.')[0];
					
				if(ext && -1 !== forcedext.indexOf(ext)) {

					if( '' !== filenameEl.html() ) {						
						filenameEl.html(name);							
					}
					
				}	else {

					input.val(null);
					alert('Only files with "'+extstr+'" extension are allowed');
				}	    			
    		}
		  });
		});
	</script>
	<?php 				
	}	
	
	
	/**
	 * Callback for the admin_print_footer_scripts hook 
	 * 
	 * Print jQuery JavaScript that is needed for a term list with $edit is true, see {@link WpieWpFormHelper::_getWpTermlist()}
	 * 
	 * @uses WpieWpFormHelper::_getInputTextNoNameValue()
	 * @uses WpieWpFormHelper::_getTermListRow()
	 * @uses WpieWpFormHelper::_getTermListRowActions()
	 * 
	 * @since 1.0
	 */
	public static function printTermListJs() 
	{
		?>
		<script type='text/javascript'>
		jQuery(function($) {		

			var WeePieTermList = {

				termLists: [],
				namespaceAction: '',
					
				getTermId: function (el) {
					
					return el.data('term-id');		
				},					

				getTermLi: function (id) {
						
					var li = this.termLists.find('li[data-term-id="'+id+'"]');
					
					if(0 < li.length) {
						return li
					} else {
						 return false;
					}		
				},					

				switchEditUpdate: function (id, show) {		
						
					var li = this.getTermLi(id);
					
					if(false !== li) {
						
						var btnUpdate = li.find('.wpie-action-upd');
						var btnEdit = li.find('.wpie-action-edit');
						
						switch(show) {			
							case 'edit':				
								btnUpdate.hide();	
								btnEdit.show();				
								break;				
							case 'update':				
								btnUpdate.show();	
								btnEdit.hide();				
								break;			
						}
					}
				},

				handlerMouseenter: function (e) {

					var li = $(this),
							termListliActions = li.find('.wpie-list-row-actions');  

					termListliActions.show();									
				},


				handlerMouseleave: function (e) {

					var li = $(this),
							termListliActions = li.find('.wpie-list-row-actions');  

					termListliActions.hide();					
				},


				handlerKeydown: function (e) {

					var thiz = e.data.thiz;					

					var li = $(this).parents('li'),
							termId = thiz.getTermId(li);
					
					thiz.switchEditUpdate(termId, 'update');
				},

				handlerBlur: function (e) {

					var thiz = e.data.thiz;

					var li = $(this).parents('li'),
							input = $(this),
							termNameNew = input.val(),
							termNameOri = li.data('term-name'),
							termId = thiz.getTermId(li);
			
					if('' === termNameNew) {
			
						thiz.switchEditUpdate(termId, 'edit');
			
						li.on('mouseenter', thiz.handlerMouseenter);	
						li.on('mouseleave', thiz.handlerMouseleave);						
												
						li.find('.wpie-term-name').val(termNameOri);
						li.find('.wpie-term-name').show();
						input.hide();		
						li.trigger('mouseleave');			
					}
				},			
					
				handlerBtnDel: function (e) {

					var thiz = e.data.thiz;	
					
					if (confirm(commonL10n.warnDelete)) {

						var li = $(this).parents('li.wpie-term-item'),
								termId = li.data('term-id'),
								tax = e.data.tax,
								termList = e.data.termList;
	
						if('' === termId || 'undefined' === typeof termId)
							return false;

						var args = {};
						args['action'] = 'wpie-action';
						args[thiz.namespaceAction] = 'wpie-del-term';
						args['nonce'] = wpieNonce;
						args['data'] = {termId:termId, tax:tax};

						$.ajax({ type: 'POST', url: ajaxurl, dataType: 'json', data: args, success: function(r) {
				    		
								//console.log('r: ',r);	
						  	switch(r.state) {
						  		case '-1': 	alert(r.out); break;
						  		case '1':								  		
										var termId = r.out.term_id;	
										termList.find('li.wpie-term-'+termId).remove();	
							  		break;
						  	}								
					    },
					    error: function (XMLHttpRequest, textStatus, errorThrown) { }
					  });
					} // end confirm delete						
				},

				handlerBtnEdit: function (e) {

					var thiz = e.data.thiz;	
					
					var li = $(this).parents('li.wpie-term-item'),
							termId = li.data('term-id'),
							termName = li.data('term-name'),
							liSpan = li.find('.wpie-term-name'), 
							input = $('<?php echo str_replace(parent::ATTR_MASK, 'class="wpie-edit-term" value=""', self::_getInputTextNoNameValue()) ?>');

					//console.log(termId, termName);						
			
					li.off('mouseenter', thiz.handlerMouseenter);	
					li.off('mouseleave', thiz.handlerMouseleave);
			
					liSpan.hide();
					liSpan.after(input);
			
					input.trigger('focus');
				},		
					
				handlerBtnUpd: function (e) {
					
					var li = $(this).parents('li.wpie-term-item'),
							termId = li.data('term-id'),
							liSpan = li.find('.wpie-term-name'), 
							input = li.find('input.wpie-edit-term'),
							termNameNew = input.val(),
							tax = e.data.tax;
			
					if('' === termId || 'undefined' === typeof termId || '' === termNameNew || 'undefined' === typeof termNameNew)
						return false;
			
					var args = {};
					args['action'] = 'wpie-action';
					args[thiz.namespaceAction] = 'wpie-upd-term';
					args['nonce'] = wpieNonce;
					args['data'] = {termId:termId, tax:tax, termName:termNameNew};
			
					$.ajax({ type: 'POST', url: ajaxurl, dataType: 'json', data: args, success: function(r) {
			    		
					  	switch(r.state) {
					  		case '-1': 	alert(r.out); break;
					  		case '1':			
									liSpan.html(r.out.name).show();
									li.attr('data-term-name', r.out.name).data('term-name', r.out.name);	
						  		break;
					  	}
					  	
					  	input.val('').hide();								
				    },
				    error: function (XMLHttpRequest, textStatus, errorThrown) { }
				  });						
				},

				handlerBtnAdd: function (e) {

					var termInput = $(this).prev('input.wpie-new-term'),				
							termVal =	termInput.val();

					if('' === termVal)
						return false;

					var thiz = e.data.thiz,
							tax = e.data.tax,
							termList = e.data.termList;

					var args = {};
					args['action'] = 'wpie-action';
					args[thiz.namespaceAction] = 'wpie-add-term';
					args['nonce'] = wpieNonce;
					args['data'] = {tax:tax, termVal:termVal};

					$.ajax({ type: 'POST', url: ajaxurl, dataType: 'json', data: args, success: function(r) {

					  	switch(r.state) {
					  		case '-1': 	alert(r.out); break;
					  		case '1':							  		
									var termId = r.out.term_id,
											termName = r.out.name,
					  					li = '<?php echo self::_getTermListRow()?>';
					  					
					  			li = li.replace(/%d/g, termId);
					  			li = li.replace(/%s/g, termName);
					  			termList.append(li);

					  			// Re-init all term lists again 
					  			// The just newly created term li will then also have all logic binded 					  			
					  			thiz.init();
						  		break;
					  	}							
							termInput.val('');													
				    },
				    error: function (XMLHttpRequest, textStatus, errorThrown) { }
				  });				
				},

				init: function () {

					if(0 < $('.wpie-term-list').length)
						this.termLists = $('.wpie-term-list');
					else
						return false;	

					// WordPress AJAX action
					// wpieNamespace is an unique string passed bij WeePie Framework
					// So that multiple Instances of the Framework can use this JS
					this.namespaceAction = wpieNamespace+'_action';
					
					var thiz = this;					

					this.termLists.each(function() {

						var termList = $(this),
								termInput = $(this).next('input.wpie-new-term'),
						 		btnAdd = termInput.next('.wpie-btn-add'),
								termListli = termList.find('li'),
								tax = termInput.data('tax');

						// Append row actions to each li
						termListli.each(function() {
							var li = $(this);					
							if( 0 === li.find('.wpie-list-row-actions').length ) { 
								li.append(' <?php echo self::_getTermListRowActions() ?>');
							}					
						});
						
						var termListliActions = termListli.find('.wpie-list-row-actions'),   
								btnDel = termListliActions.find('.wpie-action-del'),
								btnEdit = termListliActions.find('.wpie-action-edit'),
								btnUpd = termListliActions.find('.wpie-action-upd');

						
						termListli.on('keydown', 'input.wpie-edit-term', {thiz:thiz}, thiz.handlerKeydown);										
						termListli.on('blur', 'input.wpie-edit-term', {thiz:thiz}, thiz.handlerBlur);
						
						termListli.on({
							mouseenter: thiz.handlerMouseenter,
							mouseleave: thiz.handlerMouseleave,
						});				
						
						btnDel.on('click', {thiz:thiz, tax:tax, termList:termList}, thiz.handlerBtnDel);
						btnEdit.on('click', {thiz:thiz, tax:tax, termList:termList}, thiz.handlerBtnEdit);
						btnUpd.on('click', {thiz:thiz, tax:tax, termList:termList}, thiz.handlerBtnUpd);	
						btnAdd.on('click', {thiz:thiz, tax:tax, termList:termList}, thiz.handlerBtnAdd);						
					});	//end termLists each							
				}
			};

			// Init all params and events and loop trew all term lists 
			WeePieTermList.init();							
		});
		</script>
		<?php
	}	
	
	
	/**
	 * Callback for the admin_print_footer_scripts hook 
	 * 
	 * Print jQuery JavaScript that is needed for a color picker field
	 * 
	 * @uses wpColorPicker()
	 * 
	 * @since 1.0
	 */
	public static function printColorPickerJs() 
	{
	?>
	<script type='text/javascript'>
	jQuery(function($) {	
		$('.colorpicker').wpColorPicker();	
	});
	</script>
	<?php		
	}	
	
	
	/* (non-PHPdoc)
	 * @see FormHelper::_prepareAttributes()
	 * 
	 * @since 0.1
	 */
	protected static function _prepareAttributes()
	{			
		//add default classes
		if( 'wpajaxbutton' === self::$type )
		{
			self::$attributes['class'] = ( isset(self::$attributes['class']) ) ? self::$attributes['class'].' button' : 'button';
		}		
		
		if( 'wpimage' === self::$type )
		{
			self::$attributes['class'] = ( isset(self::$attributes['class']) ) ? self::$attributes['class'].' wpimage' : 'wpimage';
		}
		
		if( 'wpfile' === self::$type )
		{
			self::$attributes['class'] = ( isset(self::$attributes['class']) ) ? self::$attributes['class'].' wpfile' : 'wpfile';			
		}

		if( 'wpfilebasic' === self::$type )
		{
			self::$attributes['class'] = ( isset(self::$attributes['class']) ) ? self::$attributes['class'].' wpfilebasic' : 'wpfilebasic';			
		}		

		if( 'wppageselect' === self::$type )
		{
			self::$attributes['class'] = ( isset(self::$attributes['class']) ) ? self::$attributes['class'].' wppageselect' : 'wppageselect';
		}	

		if( 'wptermselect' === self::$type )
		{
			self::$attributes['class'] = ( isset(self::$attributes['class']) ) ? self::$attributes['class'].' wptermselect' : 'wptermselect';		
			self::$attributes['_tax'] = ( taxonomy_exists(self::$attributes['_tax']) ) ? self::$attributes['_tax'] : 'category';			
		}	

		if( 'wptermlist' === self::$type || 'wptermlist_edit' === self::$type )
		{
			self::$attributes['class'] = ( isset(self::$attributes['class']) ) ? self::$attributes['class'].' wpie-term-list' : 'wpie-term-list';		
			self::$attributes['_tax'] = ( taxonomy_exists(self::$attributes['_tax']) ) ? self::$attributes['_tax'] : 'category';			
		}		
		
		if( 'wpposttypeselect' === self::$type )
		{
			self::$attributes['class'] = ( isset(self::$attributes['class']) ) ? self::$attributes['class'].' wpposttypeselect' : 'wpposttypeselect';
		}			
		
		if( 'wpdate' === self::$type || 'wpdatepicker' === self::$type )
		{
			self::$attributes['class'] = ( isset(self::$attributes['class']) ) ? self::$attributes['class'].' dateinput datepicker' : 'dateinput datepicker';	
		}		

		if( 'wpcolor' === self::$type || 'wpcolorpicker' === self::$type )
		{
			self::$attributes['class'] = ( isset(self::$attributes['class']) ) ? self::$attributes['class'].' colorpicker' : 'colorpicker';
		}		
		
		if( 'wptextarea' === self::$type ) {}

		if( 'wptextareabasic' === self::$type )
		{
			unset( self::$attributes['value'] );
		}
	}		
	
	
	/* (non-PHPdoc)
	 * @see FormHelper::_substituteAttributes()
	 * 
	 * @since 1.0
	 */
	protected static function _substituteAttributes( &$field )
	{		
		if( preg_match_all('/{([a-zA-Z]+?)}/', $field, $matches) )
		{	
			foreach( $matches[0] as $k => $attr ){
								
				switch($attr)
				{
					case self::VALUE_MASK_FILENAME:						
						
						$field = preg_replace("/$attr/", self::$filename, $field);
						break;
				}									
				
			}			
		}	

		parent::_substituteAttributes($field);
	}		
	
	
	
	/* (non-PHPdoc)
	 * @see FormHelper::_reset()
	 * 
	 * @since 1.0
	 */
	protected static function _reset() {
	
		self::$filename='';
		
		parent::_reset();		
	}	
	
	
	/**
	 * Get the HTML for the WordPress image upload button
	 *
	 * @access private
	 * 
	 * @uses WpieWpFormHelper::enqueueMediaScripts()
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	private static function _getWpInputImage()
	{
		self::enqueueMediaScripts();
		add_action('admin_print_footer_scripts', array(__CLASS__, 'printInputImageJs'));		
	
		$tabindex		= ( isset( parent::$attributes['tabindex'] ) ) ? ' tabindex="'.parent::$attributes['tabindex'].'"' : '';
		$previewMaxWh	= ( isset( parent::$attributes['data-preview-max-wh'] ) && is_numeric( parent::$attributes['data-preview-max-wh'] ) ) ? parent::$attributes['data-preview-max-wh'] : false;
		$previewW		= ( isset( parent::$attributes['data-preview-w'] ) && is_numeric( parent::$attributes['data-preview-w'] ) ) ? parent::$attributes['data-preview-w'] : false;
		$previewH		= ( isset( parent::$attributes['data-preview-h'] ) && is_numeric( parent::$attributes['data-preview-h'] ) ) ? parent::$attributes['data-preview-h'] : false;
		$ratio			= ( isset( parent::$attributes['data-preview-ratio'] ) && is_numeric( parent::$attributes['data-preview-ratio'] ) ) ? parent::$attributes['data-preview-ratio'] : 1;
		
		$w = ( $previewMaxWh && $previewW && $previewMaxWh <= $previewW ) ? $previewMaxWh : $previewW; 
		$h = ( $previewMaxWh && $previewH && $previewMaxWh <= $previewH ) ? round($previewMaxWh*$ratio) : $previewH;

		$style = '';		
		if( $previewMaxWh && 'svg' === self::$fileExt ) {
			$w = $previewMaxWh;
			$h = '';
			$style = " style='max-width:{$previewMaxWh}px; max-height:{$previewMaxWh}px'";			
		}			
		
		$preview = ( isset( parent::$attributes['data-show-preview'] ) && true == parent::$attributes['data-show-preview'] && parent::$value ) ? '<img width="'.$w.'" height="'.$h.'" src="'.WP_CONTENT_URL . '/uploads/' . parent::VALUE_MASK.'"'.$style.' />' : '';
		unset( parent::$attributes['tabindex'] );
		
		$input  = '<input '.parent::ATTR_MASK.' type="hidden" name="'.parent::NAME_MASK.'" value="'.parent::VALUE_MASK.'" />';
		$input .= '<button class="button wp_upload_button upload_img_button" data-button-type="image"'.$tabindex.'>'.parent::INNER_HTML_MASK.'</button><span class="filename">'. (( '' === $preview ) ? self::VALUE_MASK_FILENAME : '') . '</span><span class="preview">'.$preview.'</span>';
	
		return $input;
	}
	
	
	/**
	 * Get the HTML for the WordPress file upload button
	 *
	 * @access private
	 * 
	 * @uses WpieWpFormHelper::enqueueMediaScripts()
	 * 
	 * @since 1.0
	 * 
	 * @return string
	 */
	private static function _getWpInputFile()
	{
		self::enqueueMediaScripts();
		add_action('admin_print_footer_scripts', array(__CLASS__, 'printInputFileJs'));		
	
		$disabled = (parent::$isDisabled) ? ' disabled="disabled"' : '';
		
		$input  = '<input '.parent::ATTR_MASK.' type="hidden" name="'.parent::NAME_MASK.'" value="'.parent::VALUE_MASK.'" /><span class="filename">'.self::VALUE_MASK_FILENAME.'</span>';
		$input .= '<button class="button wp_upload_button upload_file_button" data-button-type="file"'.$disabled.'>'.parent::INNER_HTML_MASK.'</button>';
	
		return $input;
	}

	
	
	/**
	 * Get the HTML for a basic file upload button
	 * 
	 * @access private  
	 * 
	 * @uses FormHelper::_getInputFile()
	 * @uses FormHelper::_getLinkButton()
	 * @uses FormHelper::_substituteAttributesManual()
	 * 
	 * @since 1.0
	 * 
	 * @return string
	 */
	private static function _getWpInputFileBasic()
	{
		add_action('admin_print_footer_scripts', array(__CLASS__, 'printInputFileJs'));	
		
		$input = '';
		
		if( '' !== self::$filename ) {

			$input  = '<span class="filename">'.self::VALUE_MASK_FILENAME.'</span>';
			
			$file .= parent::_getInputFile();

			if( isset(parent::$attributes['style']) ) {
			
				parent::$attributes['style'] = parent::$attributes['style'] . ';display:none;';
				
			} else {
				parent::$attributes['style'] = 'display:none';
			}
									
			$change = parent::_getLinkButton();
			$disabled = (parent::$isDisabled) ? 'disabled' : '';
			parent::_substituteAttributesManual($change, parent::ATTR_MASK, 'class="button wpie-change-file '.$disabled.'"');		
			
			$input .= $file.$change;
			
		} else {
			
			$input .= parent::_getInputFile();
		}
	
		return $input;
	}	
	
	
	/**
	 * Get the HTML for a WordPress link (a) button with ajax loader gif
	 *
	 * @access private
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	private static function _getWpInputAjaxButton()
	{
		return '<a '.self::ATTR_MASK.'>'.self::INNER_HTML_MASK.'</a><img alt="" class="ajax-loading" src="'.admin_url('images/wpspin_light.gif').'" />';
	}	
	
	
	/**
	 * Get the HTML for a selectbox with WordPress pages
	 *
	 * @access private
	 * 
	 * @since 0.1 
	 *  
	 * @return string
	 */
	private static function _getWpPageSelect()
	{
		return self::getPageDropdown(self::$attributes['id'], self::$attributes['name'], 'ID', 0, self::$attributes['value']);
	}	
	
	
	/**
	 * Get the HTML for a selectbox with Post Types
	 * 
	 * @uses WpieWpFormHelper::getPostTypesDropDown()
	 * 
	 * @since 1.0
	 * 
	 * @return string
	 */
	private static function _getWpPostTypeSelect()
	{
		return self::getPostTypesDropDown(self::$attributes['id'], self::$attributes['name'], 'ID', 0, self::$attributes['value']);
	}		
	
	
	/**
	 * Get the HTML for a selectbox with WordPress terms of given taxonomy
	 *
	 * @access private
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	private static function _getWpTermSelect()
	{
		return self::getTermDropdown(self::$attributes['id'], self::$attributes['name'], 'ID', 0, self::$attributes['value'], self::$attributes['_tax'] );
	}	
	
	
	/**
	 * Get a WordPress editor 
	 * 
	 * @uses wp_editor()
	 * 
	 * @since 1.0
	 * 
	 * @todo possibility to change wp_editor paramaters
	 * 
	 * @return string
	 */
	private static function _getWpTextarea()
	{
		$value = html_entity_decode( self::$value, ENT_QUOTES, 'UTF-8' );
		
		$name_in_ar = ( preg_match( '/^[^\[]+\[(.+)]$/', self::$name, $m ) ) ? $m[1] : '';
		$editor_id = 	( '' !== $name_in_ar ) ? $name_in_ar : self::$name;
		
		// attributes
		$teeny  	= ( isset( self::$attributes['_teeny'] ) 	&& ( 'true' === self::$attributes['_teeny'] 	|| true === self::$attributes['_teeny'] ) 	) ? true : false; 		
		$minimal	= ( isset( self::$attributes['_minimal'] ) 	&& ( 'true' === self::$attributes['_minimal']	|| true === self::$attributes['_minimal'] ) ) ? true : false;
		$settings 	= array( 'wpautop' => true, 'media_buttons' => true, 'quicktags' => true, 'textarea_rows' => '5', 'textarea_name' => self::$name, 'teeny' => $teeny );
		
		if( $minimal ) {
		
			$settings['media_buttons'] 	= false;
			$settings['tinymce']		= false;
			$settings['quicktags'] 		= array( 'buttons' => 'strong,em,link,block,del,ins,img,ul,ol,li,code,close' );
		}
		
		ob_start();		
		//echo '<style type="text/css">.form-field input {width:auto}</style>';
		wp_editor( $value, $editor_id, $settings );
		$editor = ob_get_contents();
		ob_end_clean();
		
		return $editor;		
	}	
	
	
	/**
	 * Get a default textarea but validated by WordPress
	 * 
	 * @uses esc_textarea()
	 * 
	 * @since 1.0.1
	 * 
	 * @return string
	 */
	private static function _getWpTextareaBasic() {

		$textarea = parent::_getTextarea();	
		
		parent::_substituteAttributesManual( $textarea, parent::INNER_HTML_MASK, esc_textarea( parent::$value ) );
		
		return $textarea;		
	}	


	/**
	 * Get the HTML for a datepicker element
	 * 
	 * @uses FormHelper::_getInputText()
	 * 
	 * @access private
	 * 
	 * @since 1.0
	 * 
	 * @return string
	 */
	private static function _getWpDatePicker() 
	{
		return parent::_getInputText();
	}	
	
	
	/**
	 * Get the HTML for the 'add' fields used by formfield 'wptermlist_edit' 
	 * 
	 * @access private 
	 * 
	 * @since 1.0
	 * 
	 * @return string
	 */
	private static function _getTermListActions() 
	{
		$html = $input = $add = '';
		
		$input = parent::_getInputTextNoNameValue();
		parent::_substituteAttributesManual($input, parent::ATTR_MASK, sprintf('value="" id="%s" class="wpie-new-term" data-tax="%s"', parent::$name, parent::$attributes['_tax']));
		
		$add = parent::_getLinkButton();
		parent::_substituteAttributesManual($add, parent::INNER_HTML_MASK, 'add');
		parent::_substituteAttributesManual($add, parent::ATTR_MASK, 'class="button wpie-btn-add"');
		
		$html = $input.$add;
		
		return $html;		
	}
	
	
	/**
	 * Get the HTML template for one row in a terms list
	 * 
	 * @access private
	 * 
	 * @since 1.0
	 *  
	 * @return string
	 */
	private static function _getTermListRow() {
		
		return '<li data-term-id="%d" class="wpie-term-item wpie-term-%d" data-term-name="%s"><span class="wpie-term-name">%s</span></li>';		
	}
	
		
	/**
	 * Get the HTML template for the actions in one row of a terms list
	 * 
	 * @access private
	 * 
	 * @since 1.0 
	 *  
	 * @return string
	 */
	private static function _getTermListRowActions() {
		
		return '<a class="wpie-list-row-actions" style="display:none"><span class="wpie-action-del" style="color:red">delete </span><span class="wpie-action-edit">| edit</span><span class="wpie-action-upd" style="display:none">| update</span></a>';		
	}	
	
	
	
	/**
	 * Get the HTML for a term list
	 * 
	 * If $edit is true, JavaScript is added by WordPress hook "admin_print_footer_scripts" with {@link WpieWpFormHelper::printTermListJs()}
	 * 
	 * @access private 
	 * 
	 * @param bool $edit flag if edit controls should be added or not
	 * 
	 * @uses WpieWpFormHelper::getTermList()
	 * @uses WpieWpFormHelper::_getTermListActions()
	 * @uses WpieWpFormHelper::getTermList()
	 * 
	 * @since 1.0
	 * 
	 * @return string
	 */
	private static function _getWpTermlist( $edit = false ) {
		
		if($edit) {		
			
			add_action('admin_print_footer_scripts', array(__CLASS__, 'printTermListJs'));	
			
			$list = self::getTermList(self::$attributes['_tax']);	
			
			$controls = self::_getTermListActions();	 
			
			return $list.$controls;			
			
		} else {
		
			return self::getTermList(self::$attributes['_tax']);		
		}	
	}	

	
	
	/**
	 * Get HTMNL for a color picker 
	 * 
	 * JavaScript is added by WordPress hook "admin_print_footer_scripts" with {@link WpieWpFormHelper::printColorPickerJs()}
	 * 
	 * @uses wp_enqueue_style to enqueue the color picker styles 
	 * @uses wp_enqueue_script to enqueue the color picker scripts 
	 * @uses WpieWpFormHelper::_getColorPicker()
	 * 
	 * @since 1.0
	 * 
	 * @return string
	 */
	private static function _getWpColorPicker() {
			 
		wp_enqueue_style( 'wp-color-picker' );		 
		wp_enqueue_script( 'wp-color-picker' );
		
		add_action('admin_print_footer_scripts', array(__CLASS__, 'printColorPickerJs'), 9999 );

		$input = parent::_getColorPicker();

		return $input;
	}
	
	
	/**
	 * Render the WordPress formfield
	 * 
	 * @access private 
	 * 
	 * @uses WpieWpFormHelper::_substituteAttributes()
	 * @uses WpieWpFormHelper::_reset()
	 * 
	 * @since 0.1
	 * 
	 * @return string the formfield or a translatable error message
	 */
	private static function _render()
	{
		switch( parent::$type )
		{
			case 'wpimage':
					$field = self::_getWpInputImage();
				break;
				
			case 'wpfile':
					$field = self::_getWpInputFile();
				break;		

			case 'wpfilebasic':
					$field = self::_getWpInputFileBasic();
				break;
								
			case 'wpajaxbutton':
				$field = self::_getWpInputAjaxButton();
				break;	

			case 'wppageselect' :
				$field = self::_getWpPageSelect();
				break;
				
			case 'wptermselect' :
				$field = self::_getWpTermSelect();
				break;	
				
			case 'wpposttypeselect' :
				$field = self::_getWpPostTypeSelect();
				break;					
				
			case 'wptextarea':	
				$field = self::_getWpTextarea();
				break;	
				
			case 'wptextareabasic':
				$field = self::_getWpTextareaBasic();
				break;
				
			case 'wpdate':
			case 'wpdatepicker':				
				$field = self::_getWpDatePicker();
				break;

			case 'wptermlist':
				$field = self::_getWpTermlist();
				break;
				
			case 'wptermlist_edit':
				$field = self::_getWpTermlist(true);
				break;
				
			case 'wpcolor':
			case 'wpcolorpicker':
				$field = self::_getWpColorPicker();
				break;
				
			default:
				return sprintf( __( 'Invalid formfield type: %s', 'weepie' ), self::$type);
				break;
		}	
		
		self::_substituteAttributes( $field );
		
		// flush members
		self::_reset();
	
		return $field;
	}
}