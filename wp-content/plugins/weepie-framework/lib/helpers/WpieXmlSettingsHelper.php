<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpieXmlSettingsHelper class - helper class for XML setting tasks 
 *  
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: WpieXmlSettingsHelper.php 55 2015-05-01 12:13:38Z Vincent Weber <vincent@webrtistik.nl> $
 */
class WpieXmlSettingsHelper {
		
	
	/**
	 * Check if current field is a WordPress specific field
	 * 
	 * @param SimpleXMLElement $field
	 * @return boolean
	 */
	public static function isWpField( SimpleXMLElement $field )
	{
		return( false === strpos((string)$field->elem, 'wp') ) ? false : true;
	}
	
	
	/**
	 * Check if current settingsfield is a formgroup
	 *
	 * Groups are indicated in the formfields array like 'group-0', 'group-1' etc.
	 * The purpose is to create setting sections within a settings tab
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1s
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function isFormGroup( SimpleXMLElement $field )
	{
		return ( 'group' === $field->getName() );
	}
	
	/**
	 * Check if current settingsfield is a formgroup meta entry
	 *
	 * @param string $id
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public function isFormGroupMeta( SimpleXMLElement $field )
	{
		return ( 'group_title' === $field->getName() ||  'group_descr' === $field->getName() ||  'group_warning' === $field->getName() || 'group_notice' === $field->getName() ) ? true : false;
	}
	
	/**
	 * Check if current settingsfield is a colorpicker
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function isColorPicker( SimpleXMLElement $field )
	{
		return ( 'color' === (string)$field->elem || 'colorpicker' === (string)$field->elem ) ? true : false;
	}	
	
	/**
	 * Check if current settingsfield is a datePicker
	 * 
	 * @param SimpleXMLElement $field
	 * 
	 * @since 1.0
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function isDatePicker( SimpleXMLElement $field )
	{
		return ( 'wpdate' === (string)$field->elem || 'wpdatepicker' === (string)$field->elem ) ? true : false;
	}	
	
	
	/**
	 * Check if current settingsfield is a <select>
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function isSelect( SimpleXMLElement $field )
	{
		return ( 'select' === (string)$field->elem );
	}
	
	/**
	 * Check if current settingsfield is a <textarea>
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function isTextarea( SimpleXMLElement $field )
	{
		return ( 'textarea' === (string)$field->elem );
	}
	
	/**
	 * Check if current settingsfield is an inline field
	 *
	 * E.a. {field} {another field}
	 *
	 * @param string $id
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function isInline( SimpleXMLElement $field )
	{
		return ( 'inline' === $field->getName() );
	}
	
	/**
	 * Check if current settingsfield is an inline title
	 *
	 * @param string $id
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function isInlineTitle( SimpleXMLElement $field )
	{
		return ( 'inline_title' === $field->getName() );
	}
	
	/**
	 * Check if current settingsfield is an inline description
	 *
	 * @param string $id
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function isInlineDescr( SimpleXMLElement $field )
	{
		return ( 'inline_descr' === $field->getName() );
	}
	
	/**
	 * Check if current settingsfield is a external template
	 *
	 * @param SimpleXMLElement $field
	 * @param string $name the name of the template
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function isTemplate( SimpleXMLElement $field )
	{	
		return ( 'true' === (string)$field->inner['template']) ? true : false;
	}
	
	
	/**
	 * Check if curren field is a disabled or hidden element
	 * 
	 * @param SimpleXMLElement $field
	 * 
	 * @since 1.0
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function isHiddenOrDisabled( SimpleXMLElement $field ) 
	{
		return ('disabled' === (string)$field->elem || 'hidden' === (string)$field->elem) ? true : false;
	}

	
	/**
	 * Check if curren field is a disabled element
	 * 
	 * @param SimpleXMLElement $field
	 * 
	 * @since 1.0
	 * 
	 * @return boolean
	 */
	public static function isDisabled( SimpleXMLElement $field ) 
	{
		return ('disabled' === (string)$field->elem) ? true : false;
	}			
	
	
	/**
	 * Check if curren field is a hidden element
	 * 
	 * @param SimpleXMLElement $field
	 * 
	 * @since 1.0
	 * 
	 * @return boolean
	 */
	public static function isHidden( SimpleXMLElement $field ) 
	{
		return ('hidden' === (string)$field->elem) ? true : false;
	}			
	
	
	/**
	 * Check if current settingsfield has a formgroup title
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasFormGroupTitle( SimpleXMLElement $field )
	{
		return ( isset($field->group_title) && '' !== (string)$field->group_title ) ? true : false;
	}
	
	/**
	 * Check if current settingsfield has a formgroup description
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasFormGroupDescr( SimpleXMLElement $field )
	{
		return ( isset($field->group_descr) && '' !== (string)$field->group_descr ) ? true : false;
	}
	
	/**
	 * Check if current settingsfield has a formgroup warning
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasFormGroupWarning( SimpleXMLElement $field )
	{
		return ( isset($field->group_warning) && '' !== (string)$field->group_warning ) ? true : false;
	}
	
	/**
	 * Check if current settingsfield has a formgroup notice
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasFormGroupNotice( SimpleXMLElement $field )
	{
		return ( isset($field->group_notice) && '' !== (string)$field->group_notice ) ? true : false;
	}
	
	/**
	 * Check if current settingsfield has a description
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasDescr( SimpleXMLElement $field )
	{
		return ( isset($field->descr) && '' !== (string)$field->descr ) ? true : false;
	}
	
	/**
	 * Check if current settingsfield has HTML attributes
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasAttr( SimpleXMLElement $field )
	{
		return ( isset($field->attributes) && isset($field->attributes->attr) ) ? true : false;
	}
	
	
	/**
	 * Check if current settingsfield has an attribute with given value
	 * 
	 * @param SimpleXMLElement $field
	 * @param string $nameAttr
	 * @param string $nameValue
	 * @param string $valueValue
	 * 
	 * @since 1.0
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasAttrNameValue( SimpleXMLElement $field, $nameAttr, $nameValue, $valueValue )
	{
		return ( isset($field->attributes) && isset($field->attributes->attr[$nameAttr]) && isset($field->attributes->attr[$nameValue]) && $valueValue === (string)$field->attributes->attr[$valueValue]) ? true : false;
	}
		
	
	
	/**
	 * Check if current settingsfield has selectbox options
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasSelectOptions( SimpleXMLElement $field )
	{
		return ( isset($field->options) && isset($field->options->option) );
	}
	
	/**
	 * Check if current settingsfield has innerHTml
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasInnerHtml( SimpleXMLElement $field )
	{
		return ( isset($field->inner) && '' !== (string)$field->inner ) ? true : false;
	}
	
	/**
	 * Check if current settingsfield has a title
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasTitle( SimpleXMLElement $field )
	{
		return ( isset($field->title) && '' !== (string)$field->title ) ? true : false;
	}
	
	/**
	 * Check if current settingsfield has an inline title
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasInlineTitle( SimpleXMLElement $field )
	{
		return ( isset($field->inline_title) && '' !== (string)$field->inline_title ) ? true : false;
	}
	
	
	/**
	 * Check if current settingsfield has an inline description
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasInlineDescr( SimpleXMLElement $field )
	{
		return ( isset($field->inline_descr) && '' !== (string)$field->inline_descr ) ? true : false;
	}	
	
	
	
	/** Check if current field has a default value
	 * 
	 * @param SimpleXMLElement $field
	 * 
	 * @since 1.0
	 * 
	 * @return bool true if yes and false if no
	 */
	public static function hasDefault( SimpleXMLElement $field )
	{
		return ( isset($field->default) && '' !== (string)$field->default ) ? true : false;		
	}
	
	
	
	/**
	 * Check in all fields if a datepicker element is present
	 * 
	 * @param SimpleXMLElement $fields
	 * 
	 * @since 1.0
	 * 
	 * @return boolean
	 */
	public static function hasDatePicker( SimpleXMLElement $fields )
	{		
		$hasDatePicker = false;
		
		foreach ( $fields as $field ) {
						
			if( self::isDatePicker( $field ) ) { 
				$hasDatePicker = true; break;
			}
		}

		return $hasDatePicker;		
	}
	
	
	
	/**
	 * Get all field names 
	 * 
	 * The name attribute like <field name="my_field_name">
	 * 
	 * @param SimpleXMLElement $fields
	 * 
	 * @since 1.0
	 * 
	 * @return array with field names 
	 */
	public static function getFieldNames( SimpleXMLElement $fields ) 
	{
		$names = array();
		
		foreach ($fields as $k => $field) {
			
			if( $field['name'] && '' !== $field['name'] ) $names[$k] = (string)$field['name'];
		}
		
		return $names;
	}
	
	
	
	/**
	 * Get formfield attributes
	 *
	 * If the field is a colorpicker input, a class 'colorinput' is added
	 * For div elements 'name' and 'value' are not applied
	 *
	 * @param string $id the HTML id attribute
	 * @param SimpleXMLElement $field the current formfield
	 * @param string $value  the HTML value attribute
	 * @param string $namespace for array implentation of value's
	 * 
	 * @since 0.1
	 * 
	 * @return array
	 */
	public static function getAttr( SimpleXMLElement $field )
	{
		//TODO: loop threw attributes? Now only HTML 'class' attribute can be set from formfields array in BaseForm child classes
		$attributes = array();
	
		if( self::hasAttr($field) )
		{
			foreach ( $field->attributes->attr as $at )
			{
				$attributes[(string)$at['name']] = (string)$at['value'];
			}
		}
	
		return $attributes;
	}
	
	
	/**
	 * Get selectbox options
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return array or empty array
	 */
	public static function getSelectOptions( SimpleXMLElement $field )
	{
		$options = array();
	
		if( self::hasSelectOptions($field) )
		{
			foreach ( $field->options->option as $option )
			{
				$options[(string)$option['id']] = trim($option);
			}
		}
	
		return $options;
	}
	
	/**
	 * Get InnerHtml
	 *
	 * @param SimpleXMLElement $field
	 * @param string $modulePath optional
	 * @param array $vars optional
	 * 
	 * @since 0.1
	 * 
	 * @return string or empty string
	 */
	public static function getInnerHtml( SimpleXMLElement $field, $modulePath = '', $vars = array() )
	{
		$innerHtml = '';
	
		if( self::hasInnerHtml($field) && self::isTemplate($field) )
		{
			$file = $modulePath.'/'.$field->inner;
						
			if( file_exists( $file ) ) {
				
				$fileName = basename( $file );
				$basePath = dirname( $file );
				
				$template =  new WpieTemplate( (string)$field['name'], $basePath, $fileName );
				$template->setVars( $vars );
				$innerHtml = $template->render(false, true);
				
				unset($template, $vars);
				
			} else {
				$innerHtml = __( sprintf( 'Template file "%s" is not a valid file path.', $file ), 'weepie' );
			}	
				
		} elseif ( self::hasInnerHtml( $field ) && !self::isTemplate( $field ) ) {
				
			$innerHtml = $field->inner;
				
		} else {
				
			//$innerHtml = '';
		}
	
		return  $innerHtml;
	}
	
	/**
	 * Get inline formfield titel
	 *
	 * @param SimpleXMLElement $field
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	public static function getInlineTitle( SimpleXMLElement $field )
	{
		return  $field->inline_title;
	}	
	
	
	
	/**
	 * Trim a XML node
	 * 
	 * @access public
	 * @param string $str
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	public static function trimXmlField( $str )
	{
		return (string) trim( $str );
	}	
	
	/**
	 * Serialize and prepare xml string 
	 * 
	 * @access public
	 * @param string $xml
	 * 
	 * @since 0.1
	 * 
	 * @return boolean false if string is empty else serialized string 
	 */
	public static function serializeXml( $xml = '' )
	{
		if( '' === $xml )
			return false;
	
		$xml = trim( $xml );
		$xml = preg_replace( '/>\s+</', '><', $xml );
		$xml = serialize( $xml );
	
		return $xml;
	}
	
	/**
	 * Escape single quotes in xml field 
	 * 
	 * @access public
	 * @param string $str
	 * 
	 * @since 0.1
	 * 
	 * @return string
	 */
	public static function prepareXmlField( $str )
	{
		return (string) trim( str_replace( "'", "\'", $str ) );
	}
	
	
	
	/**
	 * Add disabled attribute 
	 * 
	 * @param SimpleXMLElement $field passed by reference
	 * 
	 * @since 1.0
	 */
	public static function addDisabled( SimpleXMLElement &$field ) 
	{
		if( isset( $field->attributes ) ) {
		
			$attr = $field->attributes->addChild( 'attr' );
			$attr->addAttribute( 'name', 'disabled' );
			$attr->addAttribute( 'value', 'disabled' );
		
		} else {
			
			$attributes = $field->addChild( 'attributes' );
			$attr = $attributes->addChild( 'attr' );
			$attr->addAttribute( 'name', 'disabled' );
			$attr->addAttribute( 'value', 'disabled' );
		}	
	}
	
	
	
	/**
	 * Create an WordPress I18n function field
	 * 
	 * Create WordPress translation funciton__()
	 * 
	 * @access public
	 * @param string $str
	 * @param string $domain
	 * @return string
	 * 
	 * @since 0.1
	 */
	public static function I18nField( $str, $domain )
	{
		$str = WpieXmlSettingsHelper::prepareXmlField( $str );
		
		return ( '' !== $str ) ? "__('$str','$domain');\n" : '';
	}	
	
	
	/**
	 * Create a CDATA field based on a SimpleXMLElement node
	 * 
	 * @access public
	 * @param SimpleXMLElement $node
	 * @param string $str
	 * 
	 * @since 0.1
	 */
	public static function cdataField( SimpleXMLElement $node, $str )
	{		
		$domNode = dom_import_simplexml( $node );
		$no = $domNode->ownerDocument;
		$domNode->appendChild( $no->createCDATASection( $str ) );	
	}
	
	
	/**
	 * Copy an XML field 
	 * 
	 * This method is used to combine XML settings from Plugin and Modules
	 * 
	 * @access public
	 * @param SimpleXMLElement $field
	 * @param SimpleXMLElement $newField
	 * @param string $locale
	 * 
	 * @since 0.1
	 */
	public static function copyField( SimpleXMLElement $field, SimpleXMLElement &$newField, $locale )
	{
		if( $field->elem )
		{
			$newField->addChild( 'elem', (string)$field->elem );			
		}		
			
		if( $field->title )
		{
			self::cdataField( $newField->addChild( 'title' ), (string)$field->title );
		}
		if( $field->descr )
		{
			self::cdataField( $newField->addChild( 'descr' ), (string)$field->descr );
		}
		if( $field->inner )
		{
			$newFieldInner = $newField->addChild( 'inner', (string)$field->inner );
			
			if($field->inner['template']) 
			{
				$newFieldInner->addAttribute( 'template', (string)$field->inner['template'] );				
			}		
			if($field->inner['tmplclass'])
			{
				$newFieldInner->addAttribute( 'tmplclass', (string)$field->inner['tmplclass'] );
			}				
		}
		if( $field->options->option )
		{
			$newFieldOptions = $newField->addChild( 'options' );
			foreach( $field->options->option as $option )//TODO Check!
			{
				$id = (string)$option['id'];
				$newFieldOption = $newFieldOptions->addChild( 'option' );
				self::cdataField( $newFieldOption, (string)$option );
				$newFieldOption->addAttribute( 'id', $id );
			}
		}
		if( $field->attributes->attr )
		{
			$newFieldAttributes = $newField->addChild( 'attributes' );
			foreach( $field->attributes->attr as $attr )
			{
				$name = (string)$attr['name'];
				$value = (string)$attr['value'];
				$newFieldAttr = $newFieldAttributes->addChild( 'attr', (string)$attr );
				$newFieldAttr->addAttribute( 'name', $name );
				$newFieldAttr->addAttribute( 'value', $value );
			}
		}				
		if( $field->xpath( 'defaults/default[@lang="'.$locale.'"]' ) )
		{
			$newFieldDefaults =  $newField->addChild( 'defaults' );
			
			foreach( $field->defaults->default as $default )
			{
				$lang = (string)$default['lang'];
				$newFieldDefault = $newFieldDefaults->addChild( 'default' );
				self::cdataField( $newFieldDefault, (string)$default);				
				$newFieldDefault->addAttribute( 'lang', $lang );
			}			
		}
		if( $field->xpath( 'default[@lang="'.$locale.'"]' ) )
		{
			$default = $field->xpath( 'default[@lang="'.$locale.'"]' );
			$newDefault = $newField->addChild( 'default' );
			self::cdataField( $newDefault, (string)$default );
			$newDefault->addAttribute( 'lang', $locale );
		}
		if( $field->default )
		{
			$newField->addChild( 'default', (string)$field->default );	
		}
	}		
}