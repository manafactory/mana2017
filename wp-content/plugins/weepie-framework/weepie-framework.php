<?php
/*
Main plugin file for WeePie Framework
 
@author $Author: Vincent Weber <vincent@webrtistik.nl> $
@version $Id: weepie-framework.php 66 2015-05-24 23:27:12Z Vincent Weber <vincent@webrtistik.nl> $
 
Plugin Name: WeePie Framework
Plugin URI:
Description: Framework to help WordPress developers building Plugins
Author: WeePie Plugins
Version: 1.1.7
Author URI: http://www.weepie-plugins.com/about-weepie-plugins/
License: GPL v3

WeePie Framework - A WordPress Plugin Framework to help WordPress developers building Plugins.

Copyright (C) 2013 - 2015, Vincent Weber webRtistik

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Load all libaries
if( !class_exists( 'WpieAjaxHelper' ) 					 ) { require_once 'lib/helpers/WpieAjaxHelper.php'; }	
if( !class_exists( 'WpieXmlSettingsHelper' ) 			 ) { require_once 'lib/helpers/WpieXmlSettingsHelper.php'; }
if( !class_exists( 'WpieFormHelper' ) 					 ) { require_once 'lib/helpers/WpieFormHelper.php'; }
if( !class_exists( 'WpieMiscHelper' ) 			 		 ) { require_once 'lib/helpers/WpieMiscHelper.php'; }
if( !class_exists( 'WpieMultisiteHelper' ) 				 ) { require_once 'lib/helpers/WpieMultisiteHelper.php'; }
if( !class_exists( 'WpieBaseSettings' ) 				 ) { require_once 'lib/settings/WpieBaseSettings.php'; }
if( !class_exists( 'WpiePluginSettings' ) 				 ) { require_once 'lib/settings/WpiePluginSettings.php'; }
if( !class_exists( 'WpiePluginGlobals' ) 				 ) { require_once 'lib/settings/WpiePluginGlobals.php'; }
if( !class_exists( 'WpieTemplate' ) 					 ) { require_once 'lib/template/WpieTemplate.php'; }
if( !class_exists( 'WpiePluginModuleTemplateProcessor' ) ) { require_once 'lib/template/WpiePluginModuleTemplateProcessor.php'; }
if( !class_exists( 'WpieBaseModuleProcessor' ) 			 ) { require_once 'lib/modules/WpieBaseModuleProcessor.php'; }
if( !class_exists( 'WpiePluginModuleProcessor' ) 		 ) { require_once 'lib/modules/WpiePluginModuleProcessor.php'; }
if( !class_exists( 'WpieTaxonomy' ) 					 ) { require_once 'lib/posttype/WpieTaxonomy.php'; }
if( !class_exists( 'WpiePostType' ) 					 ) { require_once 'lib/posttype/WpiePostType.php'; }



/**
 * Callback for the activated_plugin hook
 * 
 * Make sure the Weepie Framework is loaded first. 
 * Therefor the wp_options.active_plugins database field is re-ordered.  
 * 
 * @uses update_site_option() or update_option()
 * 
 * @since 1.0
 */
function wpiefw_load_framework_first( $plugin, $network_wide )
{		
	// ensure path to this file is via main wp plugin path
	$wp_path_to_this_file = preg_replace( '/(.*)plugins\/(.*)$/', WP_PLUGIN_DIR."/$2", __FILE__ );
	$this_plugin = plugin_basename( trim( $wp_path_to_this_file ) );
		
	if( $network_wide ) {
		
		$active_sidewide_plugins = get_site_option( 'active_sitewide_plugins', array() );
		
		if( array_key_exists( $this_plugin, $active_sidewide_plugins ) ) 
		{
			$this_plugin_key = $this_plugin;
			$this_plugin_value = $active_sidewide_plugins[$this_plugin_key];
			
			unset($active_sidewide_plugins[$this_plugin_key]);
			
			$active_sidewide_plugins = array_merge( array( $this_plugin_key => $this_plugin_value ), $active_sidewide_plugins );
			
			update_site_option( 'active_sitewide_plugins', $active_sidewide_plugins );			
		}
		
	} else {
	
		$active_plugins = get_option( 'active_plugins' );
		$this_plugin_key = array_search( $this_plugin, $active_plugins );
		
		if ( false !== $this_plugin_key ) {
			 
			array_splice( $active_plugins, $this_plugin_key, 1 );
			array_unshift( $active_plugins, $this_plugin );
			
			update_option( 'active_plugins', $active_plugins );
		}	
	}	
}
add_action( 'activated_plugin', 'wpiefw_load_framework_first', 10, 2 );


/**
 * Callback for the activate_{$file} action hook
 *
 * Set/update the current WeePie Framework version in the wp_options table
 *
 * @uses update_site_option() or update_option()
 *
 * @since 1.0.4
 */
function wpiefw_activate_framework( $networkWide )
{
	if( $networkWide ) {
		
		update_site_option( 'wpiefw_version', WeePieFramework::VERSION );
		update_site_option( 'wpiefw_active', '1' );
	}
	else {
		update_option( 'wpiefw_version', WeePieFramework::VERSION );
		update_option( 'wpiefw_active', '1' );
	}
}
register_activation_hook( __FILE__, 'wpiefw_activate_framework' );


/**
 * Callback for the activate_{$file} action hook
 *
 * Set/update the current WeePie Framework version in the wp_options table
 *
 * @uses is_multisite()
 * @uses delete_site_option() or delete_option()
 *
 * @since 1.1
 */
function wpiefw_deactivate_framework( $networkDeactivating )
{
	if( $networkDeactivating ) {

		update_site_option( 'wpiefw_active', '0' );
	}
	else {

		update_option( 'wpiefw_active', '0' );
	}
}
register_deactivation_hook( __FILE__, 'wpiefw_deactivate_framework' );
	

if( !class_exists( 'WeePieFramework' ) ) {
	
	
	/**
	 * WeePieFramework Class
	 * 
	 * This is the main Plugin class for WeePieFramework
	 * 
	 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
	 * @version $Id: weepie-framework.php 66 2015-05-24 23:27:12Z Vincent Weber <vincent@webrtistik.nl> $
	 * @since 1.0
	 */
	class WeePieFramework {		
		
		/**
		 * Access to the Global PLugin settings
		 * 
		 * @access public
		 * 
		 * @var WpPluginGlobals object
		 * 
		 * @since 0.1
		 */
		public static $globals;
		
		
		/**
		 * Handle the plugin modules initialization
		 * 
		 * This instance search, inits and hooks the modules to the Plugin
		 * 
		 * @access public
		 * 
		 * @var WpiePluginModuleProcessor object
		 * 
		 * @since 0.1
		 */
		public $moduleProcessor;	
				
		
		/**
		 * Flag if plugin is being activated
		 *
		 * @var bool
		 *
		 * @since 1.1
		 */
		protected $activating = false;		
		
		
		/**
		 * Current version of the Plugin that is extending WeePie Framework
		 *
		 * @var bool|string
		 */
		protected $pluginVersion;
		
		
		/**
		 * The unique namespace of the Plugin that is extending WeePie Framework
		 *
		 * @access protected
		 *
		 * @var string
		 *
		 * @since 1.0
		 */
		protected $nameSpace;
		
		
		/**
		 * A created nonce
		 *
		 * The nonce is printed as a JavaScript variable inside {@link WeePieFramework::printScriptsAdminHeaderVars()}
		 * Also the nonce is passed to modules with {@link WeePieFramework::moduleProcessor->init()}
		 *
		 * @access private
		 *
		 * @var string
		 *
		 * @since 1.0
		 */
		private $_nonce;
		
		
		/**
		 * Current version of the WeePie Framework
		 *
		 * @var string
		 * 
		 * @since 1.0.4
		 */
		const VERSION = '1.1.7';		
		
		
		/**
		 * Start a Plugin
		 * 
		 * @access protected
		 * 
		 * @uses register_activation_hook()
		 * @uses WpPluginGlobals class
		 * 
		 * @param string $nameSpace
		 * @param stromg $file the __FILE__ path of the Plugin that is extending WeePie Framework
		 * @param (bool|string) $version the Plugins current version
		 * 
		 * @since 1.0 
		 */
		protected function start( $nameSpace = '', $file = '', $version = false ) 
		{				
			$this->nameSpace = $nameSpace;
				
			$this->pluginVersion = $version;
			
			// prevent plugins from executing during WP heartbeat AJAX calls
			if( defined( 'DOING_AJAX' ) && true === DOING_AJAX && 'heartbeat' === $_REQUEST['action'] )
				return;
						
			// save globals with Plugin namespace to prevent overwriting
			self::$globals[$this->nameSpace] = new WpiePluginGlobals( $this->nameSpace.'_globals', $this->nameSpace, $file );
			
			register_deactivation_hook( $file, array( &$this, 'deactivatePlugin' ) );
			
			// prevent plugins from executing during deactivating
			// if needed, register_deactivation_hook() should be initiated before 
			if( isset( $_REQUEST['action'] ) && 'deactivate' === $_REQUEST['action'] && isset( $_REQUEST['plugin'] ) )
				return;			
			
			// Flag that client initiated activating process of current Plugin 
			if( isset( $_REQUEST['action'] ) && 'activate' === $_REQUEST['action'] && isset( $_REQUEST['plugin'] ) && $_REQUEST['plugin'] === self::$globals[$this->nameSpace]->offsetGet( 'pluginFile' ) )
			{
				$this->activating = true;
			}			
		
			register_activation_hook( $file, array( &$this, 'activatePlugin' ) );
			
			// only init the Plugin if not activating
			if( false === $this->activating ) 
			{				
				add_action( 'init', array( &$this, 'init' ) );
				
				// plugin is running
				define( sprintf( 'WPIE_RUNNING_%s', strtoupper( $this->nameSpace ) ), true );				
			}			
		}
		
		
		
		/**
		 * Callback for the admin_enqueue_scripts hook
		 *
		 * @acces public
		 * 
		 * @uses [YOUR_PLUGIN_NAMESPACE]_scripts_admin to let WeePie Framework Plugins enqueue scripts for admin pages
		 * 
		 * @since 0.1 
		 */
		public function setScriptsAdmin( $hook_suffix )
		{						
			do_action( $this->nameSpace . '_scripts_admin', $hook_suffix );
		}
		
		
		/**
		 * Callback for the admin_print_scripts hook: setup global JavaScript params 
		 * 
		 * @access public
		 * 
		 * @uses [YOUR_PLUGIN_NAMESPACE]_script_admin_vars to let WeePie Framework Plugins modify the $wpieVars array
		 * @uses json_encode() to safely create a JavaScript array  
		 * 
		 * @since 1.0
		 */
		public function printScriptsAdminHeaderVars() 
		{					
			$wpieVars = array();
			$wpieVars['ns'] = $this->nameSpace;
			$wpieVars['nonce'] = $this->_nonce;
			
			$wpieVars = apply_filters( $this->nameSpace . '_script_admin_vars' , $wpieVars );
			
			?>
			<script type='text/javascript'>
			/* <![CDATA[ */
			var <?php echo self::$globals[$this->nameSpace]->offsetGet( 'jsNamespace' ) ?> = <?php echo json_encode( $wpieVars ) ?>;
			/* ]]> */
			</script>
			<?php				
		}
		
		
		/**
		 * Callback for the admin_print_scripts hook
		 * 
		 * @access public
		 * 
		 * @uses [YOUR_PLUGIN_NAMESPACE]_print_scripts_admin_header to let WeePie Framework Plugins print scripts in the admin head
		 * 
		 * @since 1.0
		 */
		public function printScriptsAdminHeader()
		{
			do_action( $this->nameSpace . '_print_scripts_admin_header' );
		}		
		
		
		
		/**
		 * Callback for the admin_print_footer_scripts hook
		 * 
		 * @access public
		 * 
		 * @uses [YOUR_PLUGIN_NAMESPACE]_print_scripts_admin_footer to let WeePie Framework Plugins print scripts in the admin footer
		 * 
		 * @since 1.0
		 */
		public function printScriptsAdminFooter() 
		{
			do_action( $this->nameSpace . '_print_scripts_admin_footer' );
		}	
		
		/**
		 * Callback for the admin_enqueue_scripts hook
		 *  
		 * Enqueue styles for the admin Plugin page
		 *
		 * @acces public
		 * 
		 * @uses [YOUR_PLUGIN_NAMESPACE]_styles_admin to let WeePie Framework Plugins enqueue styles for in the admin head
		 * 
		 * @since 0.1
		 */
		public function setStylesAdmin( $hook_suffix )
		{
			global $wp_styles;
			
			do_action( $this->nameSpace . '_styles_admin', $hook_suffix, $wp_styles );		
		}	
		
		
		
		/**
		 * Callback for the admin_print_styles hook 
		 * 
		 * @access public
		 * 
		 * @uses [YOUR_PLUGIN_NAMESPACE]_print_styles_admin to let WeePie Framework Plugins print styles for in the admin head
		 * 
		 * @since 1.0
		 */
		public function printStylesAdmin() 
		{
			global $hook_suffix;
			
			do_action( $this->nameSpace . '_print_styles_admin', $hook_suffix );
		}
	
		
		/**
		 * Callback for the wp_enqueue_scripts hook: Enqueue scripts for the front-end 
		 *
		 * @acces public
		 * 
		 * @uses wp_enqueue_script()
		 * @uses [YOUR_PLUGIN_NAMESPACE]_scripts_frontend (2 params are passes: $wp_scripts, $exclude
		 * @uses [YOUR_PLUGIN_NAMESPACE]_exclude_scripts_frontend to let WeePie Framework Plugins filter the $exclude parameter
		 * 
		 * @since 0.1
		 */
		public function setScriptsFrontend()
		{
			global $wp_scripts;

			$exclude =  array();
			
			// make sure jQuery is enqueued
			wp_enqueue_script( 'jquery' );
			
			if( file_exists( self::$globals[$this->nameSpace]->offsetGet( 'pluginPath' ) . '/js/global.min.js' ) ) {
				
				wp_enqueue_script( $this->nameSpace.'-global', self::$globals[$this->nameSpace]->offsetGet( 'jsUri' ) . '/global.min.js', array( 'jquery' ) );
			}
		
			do_action( $this->nameSpace . '_scripts_frontend', $wp_scripts, apply_filters( $this->nameSpace . '_exclude_scripts_frontend', $exclude ) );
		}
		
		
		/**
		 * Callback for the wp_head hook: setup global JavaScript parameters for the frontend 
		 * 
		 * @access public
		 * 
		 * @uses [YOUR_PLUGIN_NAMESPACE]_script_frontent_vars to let WeePie Framework Plugins modify the $wpieVars array
		 * @uses json_encode() to safely create a JavaScript array
		 * 
		 * @since 1.0
		 */
		public function printScriptsFrontendVars() 
		{
			// WordPress installation URI
			$wpuri = get_bloginfo( 'wpurl' );
			
			// calculate te domain without sub-domain, so www.domain.com to domain.com
			$parsedUri = parse_url( $wpuri );			
			$host = explode( '.', $parsedUri['host'] );
			$domain = $host[count( $host )-2] . "." . $host[count( $host )-1];
			
			$wpieVars = array();
			$wpieVars['ns'] = $this->nameSpace;
			$wpieVars['nonce'] = wp_create_nonce( $this->nameSpace . '-action' );
			$wpieVars['wpurl'] = $wpuri;			
			$wpieVars['domain'] = $domain;
			$wpieVars['ajaxurl'] = admin_url( 'admin-ajax.php' );
			$wpieVars['referer'] = wp_get_referer();
			$wpieVars['currenturl'] = ( is_multisite() ) ? WpieMultisiteHelper::getCurrentUri() : home_url( add_query_arg( NULL, NULL ) );
			$wpieVars['isms'] = ( is_multisite() ) ? true : false;
			$wpieVars['mspath'] = ( is_multisite() ) ? WpieMultisiteHelper::getBlogDetail( 'path' ) : '/' ;			
				
			$wpieVars = apply_filters( $this->nameSpace . '_script_frontend_vars' , $wpieVars );
				
			?>
			<script type='text/javascript'>
			/* <![CDATA[ */
			var <?php echo self::$globals[$this->nameSpace]->offsetGet( 'jsNamespace' ) ?> = <?php echo json_encode($wpieVars) ?>;
			/* ]]> */
			</script>
			<?php			
		}
		
		
		/**
		 * Callback for the wp_head hook 
		 *
		 * @access public
		 * 
		 * @uses [YOUR_PLUGIN_NAMESPACE]_print_scripts_frontend to let WeePie Framework Plugins print styles for in the frontend head
		 * 
		 * @since 0.1 
		 */
		public function printScriptsFrontend()
		{			
			do_action( $this->nameSpace . '_print_scripts_frontend' );
		}			
			
			
		/**
		 * Callback for the wp_enqueue_scripts hook
		 * 
		 * @acces public
		 * 
		 * @uses [YOUR_PLUGIN_NAMESPACE]_print_scripts_frontend to let WeePie Framework Plugins enqueue styles for in the frontend head
		 * 
		 * @since 0.1
		 */
		public function setStylesFrontend()
		{
			global $wp_styles;
	
			do_action( $this->nameSpace . '_styles_frontend', $wp_styles, apply_filters( $this->nameSpace . '_exclude_styles_frontend', array() ) );	
		}		
		
		
		/**
		 * Callback for the admin_menu hook
		 * 
		 * @access public
		 * 
		 * @uses [YOUR_PLUGIN_NAMESPACE]_add_admin_pages to let WeePie Framework Plugins add admin page(s)
		 * 
		 * @since 0.1
		 */
		public function setAdminPages()
		{
			do_action( $this->nameSpace . '_add_admin_pages' );		
		}		
		
		
		/**
		 * Callback for the admin_menu hook
		 * 
		 * @access public
		 * 
		 * @uses [YOUR_PLUGIN_NAMESPACE]_add_admin_pages to let WeePie Framework Plugins remove admin page(s)
		 * 
		 * @since 1.0
		 */
		public function removeAdminPages()
		{
			do_action( $this->nameSpace . '_remove_admin_pages' );		
		}				
		
		
		/**
		 * Callback for the template_redirect hook: render front-end templates
		 *
		 * @access public
		 * 
		 * @since 0.1
		 */
		public function renderTemplate()
		{			
			do_action( $this->nameSpace . '_render_templates' );
		}	
		
		

		/**
		 * Callback for (WordPress) AJAX hook system 
		 * 
		 * This callback is also hooked to the 'wp_footer' action for AJAX requests made in the WordPress footer.
		 *  
		 * WeePie Framework Plugins should always provide the following REQUEST parameters in their JavaScript AJAX CALL:
		 * 
		 * 	- action: wpie-action
		 * 	- YOUR_PLUGIN_NAMESPACE_action: [UNIQUE_ACTION] to add an unique action for the AJAX CALL
		 * 	- nonce: [YOUR_PLUGIN_NAMESPACE]Data.nonce (WeePie Framework automaticly adds in some JavaScript parameters inlcuded a nonce
		 * 	- data  (optional): extra data to pass from client-side to here
		 * 
		 * Example for jQuery with Plugin namespace "myplugin" : $.post(mypluginData.ajaxurl, {action:'wpie-action', myplugin_action:'do-some-logic', nonce: mypluginData.nonce, data:{'foo':bar, 'food':bars}}, function(r) { ... });
		 * 
		 * If [YOUR_PLUGIN_NAMESPACE]_ajax_json_return returns 'content' as array index, the reponse is retured directly to the client without validating and json_encode
		 *  
		 * @uses filter [YOUR_PLUGIN_NAMESPACE]_ajax_json_return to let Modules interact with the WeePie Framework AJAX process
		 * @uses action [YOUR_PLUGIN_NAMESPACE]_after_process_ajax_request to let other Modules intertact with the AJAX process
		 * @uses WpieAjaxHelper::isValidData to validate the $return parameter. 
		 * 
		 * @link WeePieFramework::printScriptsFrontendVars()
		 * @link WeePieFramework::printScriptsAdminHeaderVars()
		 * 
		 * @since 1.0
		 * 
		 * @returns void on missing YOUR_PLUGIN_NAMESPACE_action, a raw output or an json encoded array 
		 */
		public function processAjaxRequest()
		{
			if( !isset( $_REQUEST[$this->nameSpace . '_action'] ) )
				return;
			
			$data = ( isset($_REQUEST['data']) ) ? $_REQUEST['data'] : null;
			
			$r = array();
			$r['out'] = '';
			$hasError = false;
			
			$action = $_REQUEST[$this->nameSpace . '_action'];		
		
			if( false === check_ajax_referer( $this->nameSpace . '-action', 'nonce', false ) )
			{
				$r['out'] =  'ERROR: failed verifying nonce';
				$hasError = true;
			}
			
			if( false === $hasError )
			{	
				$return = apply_filters( $this->nameSpace . '_ajax_json_return', null, $action, $data );
				
				do_action( $this->nameSpace . '_after_process_ajax_request', $action, $return );
	
				if( is_array( $return ) && isset( $return['content'] ) ) {
					echo $return['content'];
					exit;
				}
				
				$r['state'] = WpieAjaxHelper::isValidData( $return );// 1 or 0
				$r['out'] = $return;
				if( WpieAjaxHelper::hasWpError( $return ) ) {				
					$r['wperrors'] = WpieAjaxHelper::getWpErrors( $return );					
				}				
			} else {
					$r['state'] = '-1';
			}			
			
			echo json_encode( $r );
			exit;	
		}	
		
		
		/**
		 * Callback for the plugin_action_links action hook
		 * 
		 * Disable the WeePie Framework from being deactivated when WeePie Framework Plugins are still active
		 * 
		 * @param array $actions
		 * @param string $plugin_file
		 * @param array $plugin_data
		 * @param string $context
		 * 
		 * @since 1.1
		 * 
		 * @return array
		 */
		public function disableFrameworkDeactivation( $actions, $pluginFile ) 
		{
			$wpiePlugins = $this->_getFrameworkActivePlugins( is_network_admin() );
			
			$wpieFrameWorkFile = plugin_basename( __FILE__ );
			
			if ( !empty( $wpiePlugins ) &&  $pluginFile === $wpieFrameWorkFile && array_key_exists( 'deactivate', $actions ) ) {
				
				unset( $actions['deactivate'] );
			}
			
			return $actions;
		}
		
		
		/**
		 * Callback for the deactivate_{$file} action hook
		 * 
		 * Main task is:
		 * - updating the 'active_wpie_plugins' option 
		 * 
		 * @acces public
		 * 
		 * @param bool $networkDeactivating Whether the plugin is deactivated for all sites in the network 
		 * 									or just the current site. Multisite only. Default is false.
		 * 
		 * @since 1.1
		 */
		public function deactivatePlugin( $networkDeactivating ) 
		{
			$functionOptionGet 		= ( $networkDeactivating ) ? 'get_site_option' 		: 'get_option';
			$functionOptionUpdate 	= ( $networkDeactivating ) ? 'update_site_option' 	: 'update_option';
			
			$pluginFile = self::$globals[$this->nameSpace]->offsetGet( 'pluginFile' );
			
			// record active WeePie Plugins in the wp_options table
			if( false !== ( $wpiePlugins = $functionOptionGet( 'active_wpie_plugins', false ) ) )
			{
				if( is_array( $wpiePlugins ) )
				{					
					$key = array_search( $pluginFile, $wpiePlugins );
						
					if ( false !== $key ) {

						unset( $wpiePlugins[$key] );									
						$functionOptionUpdate( 'active_wpie_plugins', $wpiePlugins );
					}
				}
			}
		}

		
		/**
		 * Callback for the activate_{$file} action hook
		 * 
		 * If the current installation is multisite, all blogs are being called with the switch_to_blog() function.
		 * Else, 
		 *
		 * @param bool $networkWide, indicates if the Plugin is being activated 'network wide'
		 *
		 * @uses is_multisite()
		 * @uses get_current_blog_id()
		 * @uses switch_to_blog()
		 * @uses {@link WeePieFramework::_initModules(}) with $reset to true
		 * @uses get_site_option() or get_option()
		 * @uses update_site_option() or update_option()
		 * 
		 * @since 0.1
		 */
		public function activatePlugin( $networkWide )
		{
			$upgrading = false;
			$wpiePlugins = array();
			$isMultisite = is_multisite();
			
			$functionOptionGet 		= ( $networkWide ) ? 'get_site_option' 		: 'get_option';
			$functionOptionUpdate 	= ( $networkWide ) ? 'update_site_option' 	: 'update_option';			
			
			if( false === $this->_isFrameworkActive( $networkWide, $isMultisite ) ) {

				$plugin = plugin_basename( __FILE__ );
				$msg = __( 'WeePie Framework is not activated, please activate the WeePie Framework Plugin.', 'weepie' );
				
				trigger_error( $msg, E_USER_ERROR );
				exit;				
				
			} else {

				// set/update the current WeePie Framework version in the wp_options table
				$functionOptionUpdate( 'wpiefw_version', WeePieFramework::VERSION );				
			}		
			
			// set global var to true indicating plugin is being activated
			$GLOBALS[sprintf( 'WPIE_ACTIVATING_%s', strtoupper( $this->nameSpace ) )] = true;

			// set plugin version info. for Multisite, store the version in the wp_sitemeta table 
			if( false !== $this->pluginVersion && '' !== $this->pluginVersion ) 
			{				
				$currentVersion = $functionOptionGet( $this->nameSpace . '_version' );
			
				if( false !== $currentVersion ) {
					$upgrading = version_compare( $currentVersion, $this->pluginVersion, '<' );
				}
			
				$functionOptionUpdate( $this->nameSpace . '_version', $this->pluginVersion );
			}				

			// record active WeePie Plugins in the wp_options table
			if( false === ( $wpiePlugins = $functionOptionGet(  'active_wpie_plugins', false ) ) )
			{
				$wpiePlugins = array();
			}
			
			$wpiePlugins[] = self::$globals[$this->nameSpace]->offsetGet( 'pluginFile' );
			
			// update the option 'active_wpie_plugins'
			$functionOptionUpdate( 'active_wpie_plugins', $wpiePlugins );			
			
			if( $networkWide ) 
			{								
				$currentBlog = get_current_blog_id();					
				$sites = WpieMultisiteHelper::getSites();
				
				foreach ( $sites as $site ) 
				{	
					switch_to_blog( $site->blog_id );
					
					// init Plugin modules and reset the wp_options transient 
					$this->_initModules( true );
					
					/**
					 * Let Plugin modules hook into this process
					 * 
					 * @param bool $upgrading, true if plugin is being upgraded, false otherwise
					 * 
					 * @since 1.0
					 */
					do_action( $this->nameSpace . '_activate_plugin', $upgrading );	

					// remove all attached action to ensure the callbacks are only called ones
					remove_all_actions( $this->nameSpace . '_activate_plugin' );
				}	
				
				// switch back to the current blog
				switch_to_blog( $currentBlog );
				
			} else {
				
				// init Plugin modules and reset the wp_options transient
				$this->_initModules( true );
					
				// see documentation above
				do_action( $this->nameSpace . '_activate_plugin', $upgrading );				
			}
			
			// set global var to false indicating plugin is being activated
			$GLOBALS[sprintf( 'WPIE_ACTIVATING_%s', strtoupper( $this->nameSpace ) )] = false;
		}		
		
		
		/**
		 * Callback for the wpmu_new_blog hook
		 *
		 * Ensure that new blogs are configured automaticly
		 *
		 * @access public
		 *
		 * @param int $blogId
		 *
		 * @since 1.1beta1
		 */
		public function activatePluginForNewBlog( $blogId )
		{
			switch_to_blog( $blogId );
		
			do_action( 'activate_'  . self::$globals[$this->nameSpace]->offsetGet( 'pluginFile' ), false );
		
			restore_current_blog();
		}
				
		
		/**
		 * Callback for the init hook: initialize most of the Plugin
		 * 
		 * @acces public
		 * 
		 * Main task is:
		 * - Creating a nonce with {@link wp_create_nonce()}
		 * - Loading Plugin translated texts with {@link load_plugin_textdomain()}
		 * - Initializing Modules with {@link WeePieFramework::_initModules()}
		 * - Adding WP hooks
		 * 
		 * @since 0.1
		 * 
		 * @todo check pro's for using has_filter()  
		 */	
		public function init()
		{
			$this->_nonce = wp_create_nonce( $this->nameSpace . '-action' );
			
			$this->_initModules();
			
			// load Plugin translation
			load_plugin_textdomain( $this->nameSpace, false,  self::$globals[$this->nameSpace]->offsetGet( 'pluginDirName' ) . '/lang' );
						
			/* Network vs non network admin hooks */
			if( is_network_admin() )
			{
				add_action( 'wpmu_new_blog', array( &$this, 'activatePluginForNewBlog' ) );
				add_filter( 'network_admin_plugin_action_links', array( &$this, 'disableFrameworkDeactivation' ), 10, 2 );
				
			} else {
			
				add_filter( 'plugin_action_links', array( &$this, 'disableFrameworkDeactivation' ), 10, 2 );
			}				
			
			/* admin and front-end hooks */	
			if( is_admin() )
			{
				add_action( 'admin_menu', array( &$this, 'setAdminPages' ), 10 );
				add_action( 'admin_menu', array( &$this, 'removeAdminPages' ), 11 );								
				add_action( 'admin_enqueue_scripts', array( &$this, 'setScriptsAdmin' ) );
				add_action( 'admin_enqueue_scripts', array( &$this, 'setStylesAdmin' ) );
				add_action( 'admin_print_styles', array( &$this, 'printStylesAdmin' ) );
				add_action( 'admin_print_scripts', array( &$this, 'printScriptsAdminHeaderVars' ) );
				add_action( 'admin_print_scripts', array( &$this, 'printScriptsAdminHeader' ), 11 );
				add_action( 'admin_print_footer_scripts', array( &$this, 'printScriptsAdminFooter' ) );
				
				// frontend ajax requests
				add_action( 'wp_ajax_wpie-action', array( &$this, 'processAjaxRequest' ) );	
				add_action( 'wp_ajax_nopriv_wpie-action', array( &$this, 'processAjaxRequest' ) );
				
				do_action( $this->nameSpace . '_init_admin_only_hooks' );
					
			} else {
	
				if( true ===  apply_filters( $this->nameSpace . '_do_frontend_hooks', true ) )
				{			
					add_action( 'wp_enqueue_scripts', array(&$this, 'setStylesFrontend') );
					add_action( 'wp_enqueue_scripts', array(&$this, 'setScriptsFrontend') );
					add_action( 'wp_head', array(&$this, 'printScriptsFrontendVars'), 8 );
					add_action( 'wp_head', array(&$this, 'printScriptsFrontend') );	
					add_action( 'template_redirect', array( &$this, 'renderTemplate' ) );
	
					if( isset( $_REQUEST['action'] ) && 'wpie-footer-action' === $_REQUEST['action'] ) {
						add_action( 'wp_footer', array( &$this, 'processAjaxRequest' ), 99999 );
					}				
					
					do_action( $this->nameSpace . '_init_frontend_only_hooks' );
				}			
			}
			
			do_action( $this->nameSpace . '_init_hooks' );		
		}		
		
		
		
		/**
		 * Initialize WeePie Framework Plugin modules
		 * 
		 * An instance of class WpiePluginModuleProcessor is created. 
		 * 
		 * Plugins that extend the WeePie Framework can place Modules in folder '[YOUR_PLUGIN_ROOT]/modules'.
		 * 
		 * @access private
		 * 
		 * @param bool $reset flag to delete the database transient first
		 * 
		 * @uses WpiePluginModuleProcessor::deleteModulesTransient()
		 * @uses WpiePluginModuleProcessor::findModules()
		 * @uses WpiePluginModuleProcessor::includeModule()
		 * @uses WpiePluginModuleProcessor::init()
		 * @uses WpiePluginModuleProcessor::hook()
		 * 
		 * @since 0.1
		 */
		private function _initModules( $reset = false )
		{
			$modulePath = self::$globals[$this->nameSpace]->offsetGet( 'modulePath' );
			$moduleUri 	= self::$globals[$this->nameSpace]->offsetGet( 'moduleUri' );
			$transient 	= self::$globals[$this->nameSpace]->offsetGet( 'transientModules' );
			
			$this->moduleProcessor = new WpiePluginModuleProcessor( $transient, $modulePath, $moduleUri, '.php', $this->nameSpace );
			
			if( true === $reset )
				$this->moduleProcessor->deleteModulesTransient();	
			
			$this->moduleProcessor->findModules();
					
			$this->moduleProcessor->includeModules();
			
			$this->moduleProcessor->init( array( '_nonce' => $this->_nonce ) );
			
			$this->moduleProcessor->hook();
		}
		
		
		/**
		 * Determine if Weepie Framework is active
		 *
		 * @acces private
		 *
		 * @param bool $networkWide
		 * @param bool $isMultisite
		 *
		 * @uses get_site_option()
		 * @uses get_option()
		 *
		 * @since 1.1
		 *
		 * @return bool
		 */
		private function _isFrameworkActive( $networkWide = false, $isMultisite = false )
		{
			if( $networkWide ) {
		
				$wpieActive = get_site_option( 'wpiefw_active', false );
		
			} else {
		
				$wpieActive = get_option( 'wpiefw_active', false );
				// if is multisite, do an extra check in the global site options.
				// maybe the WeePie Framework is activated network wide
				$wpieActive = ( $isMultisite && ( '0' === $wpieActive || false === $wpieActive ) ) ? get_site_option( 'wpiefw_active', false ) : $wpieActive;
			}
		
			return ( '1' === $wpieActive ) ? true : false;
		}
				
		
		/**
		 * Get an array with active WeePie Framework Plugins
		 * 
		 * @acces protected
		 * 
		 * @param bool $isNetworkAdmin
		 * 
		 * @uses get_site_option()
		 * @uses get_option()
		 * 
		 * @since 1.1
		 * 
		 * @return array with Plugins or empty array
		 */
		protected function _getFrameworkActivePlugins( $isNetworkAdmin = false )
		{
			if( $isNetworkAdmin ) {

				return get_site_option( 'active_wpie_plugins', array() );
				
			} else {
				
				return get_option( 'active_wpie_plugins', array() );
			}			
		}
			
	} // end WeePieFramework class
} // end if class_exists