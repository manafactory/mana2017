<?php
/*
Plugin Name: Ginger - Tools Installer
Plugin URI: http://www.ginger-cookielaw.com/
Description: Extend Ginger with Import/Export, Analytics, Multilanguage, and more.
Version: 3.0
Author: Manafactory
Author URI: http://manafactory.it/
License: GPLv2 or later
Text Domain: ginger
*/

// check existing addon
require_once('addon/ginger.addon.utils.php');
require_once("ginger-plus-utils.php");



add_action( 'admin_init', 'child_plugin_has_parent_plugin' );
function child_plugin_has_parent_plugin() {
    if ( is_admin() && current_user_can( 'activate_plugins' ) &&  !is_plugin_active( 'ginger/ginger-eu-cookie-law.php' ) ) {
        add_action( 'admin_notices', 'child_plugin_notice' );

        deactivate_plugins( plugin_basename( __FILE__ ) );

        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
    }
}

function child_plugin_notice(){
    ?><div class="error"><p>Sorry, but <b>Ginger Tools Installer</b> requires <b>Ginger</b> to be installed and active!</p></div><?php
}



function custom_menu_page_removing() {
remove_submenu_page( 'ginger-setup', 'ginger-about');
}
add_action( 'admin_menu', 'custom_menu_page_removing' , 999);