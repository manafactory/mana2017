<?php


function ginger_app_data($appname){

    $url = "http://www.ginger-cookielaw.com/api/?pname=".$appname;
    $response = wp_remote_get($url);
    if($response) {
        $array = json_decode($response["body"], true);
        return $array[$appname];
    }
}


function ginger_app_price($appname, $appdata = false){
    if(!$appdata)
        $appdata = ginger_app_data($appname);
    $price = $appdata["price"];
    if($price == "0"){
        echo '<small style="color: green">(';
        _e("Free", "ginger");
        echo ')</small>';
    }else{
        echo '<small style="color: green">(';
        _e("price: ", "ginger");
        echo $price;
        echo '&euro;)</small>';
    }

}



//Attivazione plugin abilito le impostazioni di base se non inserite.
function ginger_plugin_activate() {
    $options = get_option('ginger_general');
    if (!is_array($options)){
        $options = array('enable_ginger' => '0', 'ginger_cache' => 'yes', 'ginger_opt' => 'in', 'ginger_scroll' => '1', 'ginger_click_out' => '0' , 'ginger_force_reload' => '0' , 'ginger_keep_banner' => '0' );
        update_option('ginger_general', $options);

        $options =   array (
            'ginger_banner_type' => 'bar',
            'ginger_banner_position' => 'top',
            'ginger_banner_text' => addslashes(__("This website uses cookies. By continuing to use the site you are agreeing to its use of cookies.", "ginger")),
            'ginger_Iframe_text' => addslashes(__("This content has been disabled because you have not accepted cookies.", "ginger")),
            'accept_cookie_button_text' => 'Accept',
            'theme_ginger' => 'light',
            'background_color' => '',
            'text_color' => '',
            'button_color' => '',
            'link_color' => '',
            'disable_cookie_button_status' => '0',
            'read_more_button_status' => '0',
        );
        update_option('ginger_banner', $options);
    }
}
