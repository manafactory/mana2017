<h3>
    <?php _e("Ginger Policy", "ginger"); ?>
</h3>

<p>We are Manafactory SRL makers of the Ginger plugin and owners of the ginger website.
You can find more information about us, including our full address, on our company website.</p>


<h4><strong>What personal data we collect and why we collect it</strong></h4>

<p><strong>Our site (http://www.ginger-cookielaw.com/) uses the following cookies:</strong></p>
<p>Google Analytics cookies – these are set for monitoring and tracking visitors behavior on the site.
WordPress logged-in cookies – these are used by WordPress to authenticate logged-in visitors, password authentication and user verification.
WPML cookies – these are used by our WPML plugin to identify the preferred user’s language as our site is multilingual.
WooCommerce cookies – these are used by the WooCommerce plugin to track visitors and their purchased items in the cart.
W3 Total Cache cookies – these are used by W3 Total Cache plugin to monitor referrer and user identification for caching purposes.</p>

<p>In addition to Google Analytics, we are using the following statistics on our site for performance and monitoring:
WooCommerce statistics – statistics regarding our sales, conversion rates, refunds, etc.
Sometimes, to help our support debug a Ginger-related issue on your site, we might ask you to share a copy of your site with us. Since this is your site, you are the one responsible for its data and we strongly encourage you to remove any personal information from the database before sharing it with us using this form.
That being said, here are some important points about what happens with the site duplicates and data that is shared with our support:
When a debug or setting check is needed, we ask for access to the site or even a full copy of the site.
We use that access/duplicates strictly for debugging.
We do not share that data with anyone outside of the company.
Again, we strongly encourage you to remove any personal/sensitive information they might hold before sharing access/duplication if they are concerned about it.
We cannot be held responsible for any loss of private information from databases if that should occur. In other words, you should have a fully working backup of whatever site you share with us.
Data collected by the Ginger plugin and add-ons you use</p>

<h4><strong>Who we share your data with</strong></h4>

<p>We don’t share your data with third-parties in a way as to reveal any of your personal information like email, name, etc. However, some data is transferred and/or stored with third-party services we use, like cloud-based services and payment processors. This is done as a way to provide you with a better overall service and user experience.
Here are the services we use to make our own service better for you:
PayPal – we use Paypal as one of our payment processor. During checkout, a client will provide login information and credit card info. This information is processed directly within this gateway payment and we do not save it on our sites.
Amazon SES – we use this services to manage mailing campaigns to our clients. What is shared are client contact details (name and client email).
Who can see my personal information</p>

<p>If you don't give explicit consent, there is no personal information we have or can see about you.
If you give explicit consent your personal information can be accessed by:
Our System administrator
Our supporters when they need to (in order to provide support) get the information about the client accounts and access.
How long we retain your data</p>

<p>When you submit a support ticket or a comment, its metadata is retained until (if) you tell us to remove it. We use this data so that we can recognize you and approve your comments automatically instead of holding them for moderation.
If you register on our website, we also store the personal information you provide in your user profile. You can see, edit, or delete your personal information at any time (except changing your username). Website administrators can also see and edit that information.
How we protect your data and what data breach procedures we have in place</p>

<p>We protect customer data with the following site features:
Databases are sanitized (actual user personal details are removed) before deploying to development or testing environment.
In case of a data breach, System administrators will immediately go through affected users</p>

<h4><strong>What rights you have over your data</strong></h4>

<p>If you don't give it explicit consent, the only data it can collect is which theme and plugins your site is using, which terms and taxonomy do you use, which custom post type, how many posts and users. This information is used only for providing you with faster and better support. we DON'T get personal informations about your users
    Ginger will prompt you and require explicit action, asking you if you want to share with us informations about your user profile (such email). If you decline, it will not collect this data</p>

<p>You can  request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes. In short, we cannot erase data that is vital to you being our customer (i.e. basic account information like an email address).
For these requests, please use this form.</p>