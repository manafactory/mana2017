<?php
/**
 * Created by PhpStorm.
 * User: matteobarale
 * Date: 28/05/18
 * Time: 11:22
 */

/**
 * Get all registerd Role
 * @return array
 */
function getAllRegisterdRole(){
    global $wp_roles;
    $all_roles = $wp_roles->roles;
    $editable_roles = apply_filters('editable_roles', $all_roles);
    return $editable_roles;
}


function get_first_role_permission($capabilites){
    reset($capabilites);
    $first_cap = key($capabilites);
    return $first_cap;
}

/**
 * Get selcted Role permission
 * @return string
 */
function getSelectedPermissionLevel()
{
    $ginger_admin_permission = get_option('ginger_admin-permission');
    if ($ginger_admin_permission && isset($ginger_admin_permission['ginger_role_choice']) && !empty($ginger_admin_permission['ginger_role_choice'])) {
        $selectedRole = $ginger_admin_permission['ginger_role_choice'];
    } else {
        $selectedRole = '';
    }
    return $selectedRole;
}