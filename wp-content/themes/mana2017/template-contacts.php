<?php

/* Template Name: Contact Page */


get_header(); ?>

	<div class="section fp-auto-height">
		<div class="container">
			<div class="row titolo-sezione text-center">
				<div class="col-xs-12">
					<div class="titolo-pagina clearfix">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
		</div>
    </div>
<?php $nlform = get_field('nl_form_shortcode'); if ($nlform): ?>
    <div class="section fp-auto-height">
        <div class="container">
            <div class="row titolo-sezione text-center">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="card-content">
                        <div class="titolo-pagina clearfix">
                            <h2><a href="#" title="<?php _e( 'ManaNewsletter', 'mana17' ); ?>"><?php _e( 'ManaNewsletter', 'mana17' ); ?></a></h2>
                        </div>
                        <div class="text-center newsletter-form">
                            <?php echo do_shortcode($nlform); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>

    <div class="section fp-auto-height">
        <div class="container">
            <div class="row titolo-sezione text-center">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div id="map" class="card-content" style="min-height:670px;"></div>
                </div>
            </div>
        </div>
    </div>


	<div class="section fp-auto-height">
		<div class="container">
			<div class="row titolo-sezione">
				<div class="col-xs-12 col-sm-6 col-sm-offset-3">
					<div class="card-content">
						<div class="card-vertical card-padding clearfix">
							<address>
								<h3><strong><?php _e( 'manaFACTORY', 'mana17' ); ?></strong></h3>
								<h4>
                                    <?php _e( 'Via Col della Porretta, 6, 00141, Roma', 'mana17' ); ?>
								</h4>
								<h4>
									<a href="tel:+390681173726" target="_blank">+39 06 81 17 37 26</a>
								</h4>
								<h4>
									<a href="mailto:hello@manafactory.it" target="_blank"><?php _e( 'hello@manafactory.it', 'mana17' ); ?></a>
								<hr>
								</h4>
								<p>
									<small>
                                        <?php _e( 'Manafactory S.r.l.', 'mana17' ); ?> <span> | </span><?php _e( 'P.IVA 09929951003', 'mana17' ); ?><span> | </span><a href="mailto:manafactory@pec.it" target="_blank"><?php _e( 'manafactory@pec.it', 'mana17' ); ?></a>
									</small>
								</p>
							</address>
						</div>
					</div>
                </div>
			</div>
		</div>
	</div>
	
	<?php $form = get_field('form_shortcode'); if ($form): ?>
	<div class="section fp-auto-height">
		<div class="container">
			<div class="row titolo-sezione text-center">
				<div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
					<div class="card-content">
						<div class="titolo-pagina clearfix">
							<h2><a href="#" title="<?php _e( 'Work with us', 'mana17' ); ?>"><?php _e( 'Work with us', 'mana17' ); ?></a></h2>
						</div>
                        <?php $form=get_field('form_shortcode');?>
                        <?php if (!empty($form)){?>
                            <div class="text-center">
                                <?php echo do_shortcode($form); ?>
                            </div>
                        <?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>

    <?php $fb_url = get_field('facebook_url', 'option'); ?>
	<?php $tw_url = get_field('twitter_url', 'option'); ?>
	<?php $li_url = get_field('linkedin_url', 'option'); ?>
	<?php $md_url = get_field('medium_url', 'option'); ?>

	<?php if ($fb_url || $tw_url || $li_url || $md_url): ?>
	<div class="section fp-auto-height">
		<div class="container">
			<div class="row titolo-sezione text-center">
				<div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center card-margin">
					<div class="card-content clearfix">

							<div class="titolo-pagina clearfix">
								<h2><?php _e( 'Social network', 'mana17' ); ?></h2>
							</div>

							<br>

					    <div class="col-xs-8 col-xs-offset-2">
								<ul class="contacts-social">
									<?php if($fb_url): ?>
					    		<li>
					    			<a href="<?php echo $fb_url; ?>" title="<?php _e( 'Follow Manafactory on Facebook', 'mana17' ); ?>" target="_blank">
					    				<i class="fa fa-3x fa-facebook"></i>
					    			</a>
					    		</li>
					    		<?php endif; ?>
					    		<?php if($tw_url): ?>
					    		<li>
					    			<a href="<?php echo $tw_url; ?>" title="<?php _e( 'Follow Manafactory on Twitter', 'mana17' ); ?>" target="_blank">
					    				<i class="fa fa-3x fa-twitter"></i>
					    			</a>
					    		</li>
					    		<?php endif; ?>
					    		<?php if($li_url): ?>
					    		<li>
					    			<a href="<?php echo $li_url; ?>" title="<?php _e( 'Follow Manafactory on LinkedIn', 'mana17' ); ?>" target="_blank">
					    				<i class="fa fa-3x fa-linkedin"></i>
					    			</a>
					    		</li>
					    		<?php endif; ?>
					    		<?php if($md_url): ?>
					    		<li>
					    			<a href="<?php echo $md_url; ?>" title="<?php _e( 'Follow Manafactory on Medium', 'mana17' ); ?>" target="_blank">
					    				<i class="fa fa-3x fa-medium"></i>
					    			</a>
					    		</li>
					    		<?php endif; ?>
					    	</ul>
					    </div>
					
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
<div class="section fp-auto-height">
    <div class="container">
        <div class="row titolo-sezione text-center">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center card-margin">
                <div class="card-content clearfix">
                <a href="<?php echo home_url();?>/informativa-cookie/">
                    <div class="titolo-pagina clearfix">
                        <h2><?php _e( 'PRIVACY POLICY AND COOKIES', 'mana17' ); ?></h2>
                    </div>
                </a>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?php get_footer(); ?>
