<?php get_header(); ?>

	<div class="section">

    	<div class="container">        

        <div class="row">
            
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                <div class="card-content hexagon">
                    <div class="card-vertical">

                        <div class="profile-picture">
                        		<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
                            <img src="<?php echo $image[0]; ?>" alt="" class="img-responsive" width="360" height="310">
                        </div>
                        
                        <h1>
                            <?php the_title(); ?>
                            <br>
                            <span class="role"><?php echo get_field('role'); ?></span>
                        </h1>
                        
                        <div class="card-excerpt text-center">
                            <?php echo $post->post_content; ?>
                        </div>

                        <?php
												$connected = new WP_Query( array(
												  'connected_type' => 'people_to_projects',
												  'connected_items' => get_queried_object(),
												  'nopaging' => true,
												  'p2p_orderby' => 'people_to_projects'
												) );

												if ( $connected->have_posts() ) :
												?>

												<div class="own-projects text-center">

                            <h3><?php _e( 'Projects', 'html5blank' ); ?></h3>

														<ul class="horizontal-navigation">

												<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>

												    <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>

												<?php endwhile; ?>
																
														</ul>

												</div>

												<?php wp_reset_postdata(); endif; ?>


                        <?php $skill_term = wp_get_post_terms($post->ID, 'skills', array('orderby' => 'count', 'order' => 'DESC') );
													if ( ! empty( $skill_term ) ):
														if ( ! is_wp_error( $skill_term ) ): ?>
												
															<div class="skills text-center">
																
																<h3><?php _e( 'Skills', 'html5blank' ); ?></h3>

												    		<ul class="horizontal-navigation">
												
																<?php foreach( $skill_term as $term ): ?>
																	
																	<li>
																		<a href="<?php echo get_term_link( $term->slug, 'skills' ); ?>">
																			<?php echo esc_html( $term->name ); ?>
																		</a>
																	</li>

																<?php endforeach; ?>
																</ul>
															</div>
														<?php endif;
													endif;
												?>


												<?php $technology_term = wp_get_post_terms($post->ID, 'technologies', array('orderby' => 'count', 'order' => 'DESC') );
													if ( ! empty( $technology_term ) ):
														if ( ! is_wp_error( $technology_term ) ): ?>
												
															<div class="technologies text-center">

																<h3><?php _e( 'Technologies', 'html5blank' ); ?></h3>

												    		<ul class="horizontal-navigation">
												
																<?php foreach( $technology_term as $term ): ?>
                                                                    <?php $tech_icon_name=mana_term_slug_translate($term->slug);?>
																	<li>
																		<a href="<?php echo get_term_link( $term->slug, 'technologies' ); ?>">
																			<img src="<?php echo get_template_directory_uri(); ?>/assets/skills/vector/<?php echo esc_html( $tech_icon_name ); ?>.svg" alt="<?php echo esc_html( $term->name ); ?>" width="15" height="15">
																			<span> <?php echo esc_html( $term->name ); ?></span>
																		</a>
																	</li>

																<?php endforeach; ?>
																</ul>
															</div>
														<?php endif;
													endif;
												?>
                    
                        <?php $twitter = get_field('twitter'); ?>
												<?php $linkedin = get_field('linkedin'); ?>

												<?php if ($twitter || $linkedin): ?>
								        <div class="social-footer">

								        		<h3><?php _e( 'Contacts', 'html5blank' ); ?></h3>

								            <ul class="horizontal-navigation">
								            		<?php if ($linkedin): ?>
								                <li>
								                    <a href="<?php echo $linkedin; ?>" title="<?php _e('Follow me on LinkedIn','mana17');?>" target="_blank">
								                        <i class="fa fa-linkedin"></i>
								                    </a>
								                </li>
								                <?php endif; ?>
								                <?php if ($twitter): ?>
								                <li>
								                    <a href="<?php echo $twitter; ?>" title="<?php _e('Follow me on Twitter','mana17');?>" target="_blank">
								                        <i class="fa fa-twitter"></i>
								                    </a>
								                </li>
								                <?php endif; ?>
								            </ul>
								        </div>
								        <?php endif; ?>

                    </div>
                </div>
            </div>

        </div>
    	</div>
    </div>
	<!-- section -->
	<section>

<?php get_footer(); ?>
