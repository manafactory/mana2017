<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="google-site-verification" content="f5hNN235awNF8wB8II0tMy9GyhUjuR1oF_ibIid-KlM" />
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

		<link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/favicon.ico" rel="shortcut icon">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/touch.png" rel="apple-touch-icon-precomposed">

<?php wp_head(); ?>

		<script>
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
		</script>
		<script type="text/javascript">
			var templateUrl = '<?= get_bloginfo("template_url"); ?>';
		</script>

		<script type="application/ld+json">
			{
			    "@context": "http://schema.org",
			    "@type": "Organization",
			    "name": "manaFACTORY",
			    "url": "http://manafactory.it",
			    "logo": "http://manafactory.it/wp-content/themes/mana2017/assets/img/logo.png",
			    "telephone": "0681173726",
			    "sameAs": [
			        "https://www.facebook.com/manafactory/",
			        "https://twitter.com/manafactory2012",
			        "https://www.linkedin.com/company/manafactory",
			        "https://medium.com/@Manafactory"
			    ]
			}
		</script>

<?php if ( get_post_type() == 'people' ): ?>
<?php $twitter = get_field('twitter',  $post->ID ); ?>
<?php $linkedin = get_field('linkedin',  $post->ID ); ?>
<?php $title = get_the_title( $post->ID ); ?>
<?php $nome = strtolower($title); ?>
<?php $nome = str_replace(' ', '.', $nome); ?>
		<script type="application/ld+json">
			{
			    "@context": "http://schema.org",
			    "@type": "Person",
			    "name": "<?php echo $title; ?>",
			    "url": "<?php echo get_permalink( $post->ID ); ?>",
			    "affiliation": "manaFACTORY",
			    "email": "<?php echo $nome; ?>@manafactory.it",
			    "jobTitle": "<?php echo get_field('role'); ?>",
			    "telephone": "0681173726",
<?php if ($linkedin || $twitter): ?>
			    "sameAs": [
<?php if ($linkedin): ?>
						"<?php echo $linkedin; ?>"<?php if ($linkedin && $twitter): ?>,<?php endif; ?>
<?php endif; ?>
<?php if ($twitter): ?>
						"<?php echo $twitter; ?>"
<?php endif; ?>
			    ]
<?php endif; ?>
<?php
$connected = new WP_Query( array(
  'connected_type' => 'people_to_projects',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
  'p2p_orderby' => 'people_to_projects'
) );
if ( $connected->have_posts() ) :
?>
  				,"owns": [{
<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
				    	"@type":"Product",
				    	"name":"<?php the_title(); ?>",
				    	"url":"<?php the_permalink(); ?>",
				    	"image":"<?php the_post_thumbnail_url(); ?>"
				  	},{
<?php endwhile; ?>
				  }]
<?php wp_reset_postdata(); endif; ?>
			}
		</script>
<?php endif; ?>


<?php if ( get_post_type() == 'projects' ): ?>
		<script type="application/ld+json">
			{
  				"@context": "http://schema.org",
  				"@type": "CreativeWork",
  				"name":"<?php the_title(); ?>",
  				"author": "manaFACTORY"
<?php
$connected = new WP_Query( array(
  'connected_type' => 'people_to_projects',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
  'p2p_orderby' => 'people_to_projects'
) );

if ( $connected->have_posts() ) :
?>
  				,"creator": [{
<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
				    	"@type":"Person",
				    	"name":"<?php the_title(); ?>",
				    	"url":"<?php the_permalink(); ?>",
				    	"image":"<?php the_post_thumbnail_url(); ?>"
				  	},{
<?php endwhile; ?>
				  }]
<?php wp_reset_postdata(); endif; ?>
  		}
		</script>
<?php endif; ?>


		<style>

            li.langli{
                position: relative !important;
                font-weight: 700;
                vertical-align: -webkit-baseline-middle;
                margin-left: 10px !important;
            }

            li.langli.first{
                padding-left: 10px !important;
             }
            @media (max-width: 767px){
                li.langli span.current{ display: none;}
                li.langli.first {padding-left: 0!important;}
                li.langli {margin-left: 5px !important;}
            }

        </style>

	</head>
	<body <?php body_class(); ?>>

<div id="fb-root"></div>
<!--script>
;(function(d, s, id)
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-customerchat"
  attribution="setup_tool"
  page_id="200954019931124"
  logged_in_greeting="Ginger support? Write here: http://bit.ly/GINGERSupp"
  logged_out_greeting="Ginger support? Write here: http://bit.ly/GINGERSupp">
</div-->

			<!-- header -->
			<header class="header clear" role="banner">
					
					<div id="top-nav">
						<ul>
                            <?php  echo language_menu('main_menu_lang');?>

                            <li class="contextual-menu hidden hidden-lg hidden-md hidden-sm">
<!--                            <li class="contextual-menu">-->
								<a href="javascript:;" data-remodal-target="quick-contact" title="<?php _e('Quick Contacts','mana17');?>" class="contextual-menu-trigger">
									<span>•</span>
									<span>•</span>
									<span>•</span>
								</a>
							</li>
							<li class="home-link">
								<a href="<?php echo home_url(); ?>" title="Back to Homepage" class="link">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_manaFACTORY.svg" alt="<?php _e('Logo Manafactory','mana17');?>" width="200" height="21" class="nav-badge">
								</a>
							</li>
							<li class="manifesto">
								<a href="javascript:;" data-remodal-target="manifesto" title="<?php _e('Our Manifesto','mana17');?>" class="contextual-menu-trigger">
									<i class="fa fa-question"></i>
								</a>
							</li>
						</ul>
					</div>

					<?php
					if ( !is_home() && !is_front_page() && !is_404() ) { ?>
						<div class="container">
			        <div class="row titolo-sezione">
								<div class="col-xs-12">
									<div class="text-center clearfix">
					<?php create_breadcrumbs(); ?>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>

					<!-- nav -->
					<nav class="nav" role="navigation">
						<div id="bottom-nav">
							<ul>
								<li class="vision-link">
									<a href="<?php echo home_url();?>/products/" title="Our Products" class="link">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/square-shape.svg" alt="<?php _e('Products','mana17');?>" width="30" height="30">
										<span><?php _e('Products','mana17');?></span>
									</a>
								</li>
								<li class="projects-link">
									<a href="<?php echo home_url();?>/projects/" title="Our Projects" class="link">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/circle-shape.svg" alt="<?php _e('Projects','mana17');?>" width="27" height="27">
										<span><?php _e('Projects','mana17');?></span>
									</a>
								</li>
								<li class="people-link" style="display:none;">
									<a href="<?php echo home_url();?>/people/" title="Our People" class="link">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hexagon-shape.svg" alt="<?php _e('People','mana17');?>" width="30" height="30">
										<span><?php _e('Partners','mana17');?></span>
<!--										<span>--><?php //_e('People','mana17');?><!--</span>-->
									</a>
								</li>
								<li class="contacts-link">
									<a href="<?php echo home_url();?>/contacts/" title="Our Contacts" class="link">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/triangle-shape.svg" alt="<?php _e('Contacts','mana17');?>" width="29" height="29">
										<span><?php _e('Contacts','mana17');?></span>
									</a>
								</li>
							</ul>
						</div>
					</nav>
					<!-- /nav -->

			</header>
			<!-- /header -->
			

			<!-- #fullpage page container -->
			<div id="fullpage">