<?php get_header(); ?>

	<div class="section">

    	<div class="container">        

        <div class="row">
            
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                <div class="card-content hexagon">
                    <div class="card-vertical">
												
												<?php $icon = get_field('icon'); ?>
												<?php if($icon): ?>
                        
                        <div class="logo-picture">
								            <i class="fa fa-5x <?php echo $icon; ?>"></i>
								        </div>
								      	
								      	<?php endif; ?>
                        
                        <h1><?php the_title(); ?><br><span class="role"><?php echo get_field('sottotitolo'); ?></span></h1>
                        
                        <div class="card-excerpt text-center">
                            <?php echo $post->post_content; ?>
                        </div>

                        <?php $skill_term = wp_get_post_terms($post->ID, 'skills', array('orderby' => 'count', 'order' => 'DESC') );
													if ( ! empty( $skill_term ) ):
														if ( ! is_wp_error( $skill_term ) ): ?>
												
															<div class="skills text-center">
																
																<h3><?php _e( 'Skills', 'html5blank' ); ?></h3>

												    		<ul class="horizontal-navigation">
												
																<?php foreach( $skill_term as $term ): ?>
																	
																	<li>
																		<a href="<?php echo get_term_link( $term->slug, 'skills' ); ?>">
																			<?php echo esc_html( $term->name ); ?>
																		</a>
																	</li>

																<?php endforeach; ?>
																</ul>
															</div>
														<?php endif;
													endif;
												?>

												<?php $technology_term = wp_get_post_terms($post->ID, 'technologies', array('orderby' => 'count', 'order' => 'DESC') );
													if ( ! empty( $technology_term ) ):
														if ( ! is_wp_error( $technology_term ) ): ?>
												
															<div class="technologies text-center">

																<h3><?php _e( 'Technologies', 'html5blank' ); ?></h3>

												    		<ul class="horizontal-navigation">
												
																<?php foreach( $technology_term as $term ): ?>
																	
																	<li>
																		<a href="<?php echo get_term_link( $term->slug, 'technologies' ); ?>">
																			<img src="<?php echo get_template_directory_uri(); ?>/assets/skills/vector/<?php echo esc_html( $term->name ); ?>.svg" alt="<?php echo get_term_link( $term->slug, 'technologies' ); ?>" width="15" height="15">
																			<span> <?php echo esc_html( $term->name ); ?></span>
																		</a>
																	</li>

																<?php endforeach; ?>
																</ul>
															</div>
														<?php endif;
													endif;
												?>
                    
                    </div>
                </div>
            </div>

        </div>
    	</div>
    </div>
	<!-- section -->
	<section>

<?php get_footer(); ?>
