<?php $args = array(
								'posts_per_page' => -1,
								'post_type' => 'people',
								'post_status' => 'publish',
								'orderby' => 'menu_order',
								'order' => 'ASC'
							);
			$query = new WP_Query( $args ); ?>
<?php if ( $query->have_posts() ): while ( $query->have_posts() ) : $query->the_post(); ?>

	<!-- article -->
	<div class="col-xs-6 col-sm-3 text-center card-margin">
		<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
			<div class="flipper">
				<div class="front">	    
					<div class="card-padding hexagon">
						<div class="card-vertical clearfix">
							<!-- profile picture -->
							<div class="profile-picture">
								<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title();?> - <?php _e('immagine','mana17');?>" class="img-responsive" width="260" height="210">
								</a>
							</div>
							<!-- /profile picture -->
							<h2><?php the_title(); ?></h2>
						</div>
					</div>
				</div>

				<div class="back">
					<div class="card-padding hexagon">
						<div class="card-vertical text-center clearfix">

							<!-- post title -->
							<h4>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a><br><small><?php echo get_field('role') ?></small>
							</h4>
							<!-- /post title -->
							
							<div class="card-list-excerpt">
								<?php html5wp_excerpt('html5wp_index'); ?>
							</div>

							<?php $skill_term = wp_get_post_terms($post->ID, 'skills', array('orderby' => 'count', 'order' => 'DESC') );
								if ( ! empty( $skill_term ) ):
									if ( ! is_wp_error( $skill_term ) ): ?>
							
										<div class="skills text-center">
							    		<ul class="horizontal-navigation small-horizontal-navigation">
							
											<?php foreach( $skill_term as $term ): ?>
												
												<li>
													<a href="<?php echo get_term_link( $term->slug, 'skills' ); ?>">
														<?php echo esc_html( $term->name ); ?>
													</a>
												</li>

											<?php endforeach; ?>
											</ul>
										</div>
									<?php endif;
								endif;
							?>

							<?php $technology_term = wp_get_post_terms($post->ID, 'technologies', array('orderby' => 'count', 'order' => 'DESC') );
								if ( ! empty( $technology_term ) ):
									if ( ! is_wp_error( $technology_term ) ): ?>
							
										<div class="technologies text-center">
							    		<ul class="horizontal-navigation small-horizontal-navigation">
											<?php foreach( $technology_term as $term ): ?>
                                                <?php $tech_icon_name=mana_term_slug_translate($term->slug);?>
												<li>
													<a href="<?php echo get_term_link( $term->slug, 'technologies' ); ?>" title="<?php echo esc_html( $term->name ); ?>">
														<img src="<?php echo get_template_directory_uri(); ?>/assets/skills/vector/<?php echo esc_html( $tech_icon_name ); ?>.svg" alt="<?php echo esc_html( $term->name ); ?>" width="15" height="15">
														<span> <?php echo esc_html( $term->name ); ?></span>
													</a>
												</li>

											<?php endforeach; ?>
											</ul>
										</div>
									<?php endif;
								endif;
							?>

							<?php $twitter = get_field('twitter', $post->ID); ?>
							<?php $linkedin = get_field('linkedin', $post->ID); ?>

							<?php if ($twitter || $linkedin): ?>
							<div class="social-footer">
							    <ul class="horizontal-navigation small-horizontal-navigation">
							    		<?php if ($linkedin): ?>
							        <li>
							            <a href="<?php echo $linkedin; ?>" title="<?php _e('Follow me on LinkedIn','mana17');?>" target="_blank">
							                <i class="fa fa-linkedin"></i>
							            </a>
							        </li>
							        <?php endif; ?>
							        <?php if ($twitter): ?>
							        <li>
							            <a href="<?php echo $twitter; ?>" title="<?php _e('Follow me on Twitter','mana17');?>Follow me on Twitter" target="_blank">
							                <i class="fa fa-twitter"></i>
							            </a>
							        </li>
							        <?php endif; ?>
							    </ul>
							</div>
							<?php endif; ?>

							<?php //edit_post_link(); ?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /div -->

<?php endwhile; wp_reset_postdata(); ?>

<?php else: ?>

	<!-- article -->
	<div>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</div>
	<!-- /div -->

<?php endif; ?>
