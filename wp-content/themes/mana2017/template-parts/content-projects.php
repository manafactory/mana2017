								<?php global $project; ?>
								<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 text-center card-margin">
								    <div class="card-content circle no-background">
								        <div class="card-vertical">
								            <div class="logo-picture">
								            		<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($project->ID), 'medium'); ?>
								                <a href="<?php echo get_permalink( $project->ID ); ?>" title="<?php echo $project->post_title; ?>">
								                	<img src="<?php echo $image[0]; ?>" alt="<?php echo $project->post_title; ?> <?php _e('logo','mana17');?>" class="img-responsive" width="122" height="122">
								                </a>
								            </div>
								            <h4>
								                <a href="<?php echo get_permalink( $project->ID ); ?>" title="<?php echo $project->post_title; ?>">
								                	<?php echo $project->post_title; ?>
								                </a>
								            </h4>
								        </div>
								    </div>
								</div>