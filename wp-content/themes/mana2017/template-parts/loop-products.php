<?php $args = array(
								'posts_per_page' => -1,
								'post_type' => 'products',
								'post_status' => 'publish',
								'orderby' => 'menu_order',
								'order' => 'ASC'
							);
			$query = new WP_Query( $args ); ?>
<?php if ( $query->have_posts() ): while ( $query->have_posts() ) : $query->the_post(); ?>

	<!-- article -->
	<div class="col-xs-12 col-sm-6 col-md-4 text-center card-margin match-height">
		<div class="card-content square">
			<div class="card-vertical">
		
				<!-- post icon -->
				<?php $icon = get_field('icon', $post->ID); ?>
				<?php if($icon): ?>
		    
		    <div class="logo-picture">
				    <i class="fa fa-3x <?php echo $icon; ?>"></i>
				</div>
				
				<?php endif; ?>
				<!-- /post icon -->

				<!-- post title -->
				<h2>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a><br><small><?php echo get_field('sottotitolo') ?></small>
				</h2>
				<!-- /post title -->
				
				<div class="card-list-excerpt">
					<?php html5wp_excerpt('html5wp_index'); ?>
				</div>

				<?php $skill_term = wp_get_post_terms($post->ID, 'skills', array('orderby' => 'count', 'order' => 'DESC') );
					if ( ! empty( $skill_term ) ):
						if ( ! is_wp_error( $skill_term ) ): ?>
				
							<div class="skills text-center">
				    		<ul class="horizontal-navigation small-horizontal-navigation">
				
								<?php foreach( $skill_term as $term ): ?>
									
									<li>
										<a href="<?php echo get_term_link( $term->slug, 'skills' ); ?>">
											<?php echo esc_html( $term->name ); ?>
										</a>
									</li>

								<?php endforeach; ?>
								</ul>
							</div>
						<?php endif;
					endif;
				?>

				<?php $technology_term = wp_get_post_terms($post->ID, 'technologies', array('orderby' => 'count', 'order' => 'DESC') );
					if ( ! empty( $technology_term ) ):
						if ( ! is_wp_error( $technology_term ) ): ?>
				
							<div class="technologies text-center">
				    		<ul class="horizontal-navigation small-horizontal-navigation">
				
								<?php foreach( $technology_term as $term ): ?>
                                    <?php $tech_icon_name=mana_term_slug_translate($term->slug);?>
									<li>
										<a href="<?php echo get_term_link( $term->slug, 'technologies' ); ?>">
											<img src="<?php echo get_template_directory_uri(); ?>/assets/skills/vector/<?php echo esc_html( $tech_icon_name ); ?>.svg" alt="<?php echo get_term_link( $term->slug, 'technologies' ); ?>" width="15" height="15">
											<span> <?php echo esc_html( $term->name ); ?></span>
										</a>
									</li>

								<?php endforeach; ?>
								</ul>
							</div>
						<?php endif;
					endif;
				?>

				<?php //edit_post_link(); ?>

			</div>
		</div>
	</div>
	<!-- /div -->

<?php endwhile; wp_reset_postdata(); ?>

<?php else: ?>

	<!-- article -->
	<div>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</div>
	<!-- /div -->

<?php endif; ?>
