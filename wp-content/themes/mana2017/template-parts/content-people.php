								<?php global $person; ?>
								<div class="col-xs-6 col-sm-3 text-center card-margin">
								    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
								        <div class="flipper">
								            <div class="front">
								                <div class="card-padding hexagon">
								                    <div class="card-vertical clearfix">
								                        <div class="profile-picture">
								                        		<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($person->ID), 'medium'); ?>
								                            <a href="<?php echo get_permalink( $person->ID ); ?>">
								                            	<img src="<?php echo $image[0]; ?>" alt="" class="img-responsive" width="260" height="210">
								                            </a>
								                        </div>
								                        <h2><?php echo $person->post_title; ?></h2>
								                    </div>
								                </div>
								            </div>
								            <div class="back">
								                <div class="card-padding hexagon">
								                    <div class="card-vertical text-center clearfix">
								                        
								                        <h4 class="name">
								                        	<a href="<?php echo get_permalink( $person->ID ); ?>">
								                        		<strong>
								                        			<?php echo $person->post_title; ?>
								                        		</strong>
								                        	</a>
								                        </h4>
								                        <h4><span class="role"><?php echo get_field('role', $person->ID) ?></span></h4>

								                        <div class="card-list-excerpt hidden-xs">
								                            <?php echo $person->post_content; ?>
								                        </div>
								                        
								                        <?php $skill_term = wp_get_post_terms($person->ID, 'skills', array('orderby' => 'count', 'order' => 'DESC') );
																					if ( ! empty( $skill_term ) ):
																						if ( ! is_wp_error( $skill_term ) ): ?>
																				
																							<div class="skills text-center">
														                		<ul class="horizontal-navigation small-horizontal-navigation">
																				
																								<?php foreach( $skill_term as $term ): ?>
																									
																									<li>
																										<a href="<?php echo get_term_link( $term->slug, 'skills' ); ?>">
																											<?php echo esc_html( $term->name ); ?>
																										</a>
																									</li>

																								<?php endforeach; ?>
																								</ul>
														            			</div>
																						<?php endif;
																					endif;
																				?>

																				<?php $technology_term = wp_get_post_terms($person->ID, 'technologies', array('orderby' => 'count', 'order' => 'DESC') );
																					if ( ! empty( $technology_term ) ):
																						if ( ! is_wp_error( $technology_term ) ): ?>
																				
																							<div class="technologies text-center">
														                		<ul class="horizontal-navigation small-horizontal-navigation">
																				
																								<?php foreach( $technology_term as $term ):?>
																									<?php $tech_icon_name=mana_term_slug_translate($term->slug);?>
																									<li>
																										<a href="<?php echo get_term_link( $term->slug, 'technologies' ); ?>" title="<?php echo esc_html( $term->name ); ?>">
                                                                                                            <?php if (!empty($tech_icon_name)){?>
																											    <img src="<?php echo get_template_directory_uri(); ?>/assets/skills/vector/<?php echo esc_html( $tech_icon_name ); ?>.svg" alt="<?php echo esc_html( $term->name ); ?>" width="15" height="15">
																											<?php } ?>
                                                                                                            <span> <?php echo esc_html( $term->name ); ?></span>
																										</a>
																									</li>

																								<?php endforeach; ?>
																								</ul>
														            			</div>
																						<?php endif;
																					endif;
																				?>

																				<?php $twitter = get_field('twitter', $person->ID); ?>
																				<?php $linkedin = get_field('linkedin', $person->ID); ?>

																				<?php if ($twitter || $linkedin): ?>
								                        <div class="social-footer">
								                            <ul class="horizontal-navigation small-horizontal-navigation">
								                            		<?php if ($linkedin): ?>
								                                <li>
								                                    <a href="<?php echo $linkedin; ?>" title="<?php _e('Follow me on LinkedIn','mana17');?>" target="_blank">
								                                        <i class="fa fa-linkedin"></i>
								                                    </a>
								                                </li>
								                                <?php endif; ?>
								                                <?php if ($twitter): ?>
								                                <li>
								                                    <a href="<?php echo $twitter; ?>" title="<?php _e('Follow me on Twitter','mana17');?>" target="_blank">
								                                        <i class="fa fa-twitter"></i>
								                                    </a>
								                                </li>
								                                <?php endif; ?>
								                            </ul>
								                        </div>
								                      	<?php endif; ?>

								                    </div>
								                </div>
								            </div>
								        </div>
								    </div>
								</div>