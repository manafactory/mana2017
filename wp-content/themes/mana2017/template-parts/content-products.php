								<?php global $product; ?>
								<div class="col-xs-12 col-sm-6 col-md-4 text-center card-margin match-height">
								    <div class="card-content square">
								        <div class="card-vertical">

								            <?php $icon = get_field('icon', $product->ID); ?>
														<?php if($icon): ?>
		                        
		                        <div class="logo-picture">
										            <i class="fa fa-3x <?php echo $icon; ?>"></i>
										        </div>
										      	
										      	<?php endif; ?>
								            
								            <h2><a href="<?php echo get_permalink( $product->ID ); ?>" title="<?php echo $product->post_title; ?>"><?php echo $product->post_title; ?></a><br><small><?php echo get_field('sottotitolo', $product->ID) ?></small></h2>
								            
								            <div class="card-excerpt">
								                <?php echo $product->post_content; ?>
								            </div>

														<?php $skill_term = wp_get_post_terms($product->ID, 'skills', array('orderby' => 'count', 'order' => 'DESC') );
															if ( ! empty( $skill_term ) ):
																if ( ! is_wp_error( $skill_term ) ): ?>
														
																	<div class="skills text-center">
								                		<ul class="horizontal-navigation small-horizontal-navigation">
														
																		<?php foreach( $skill_term as $term ): ?>
																			
																			<li>
																				<a href="<?php echo get_term_link( $term->slug, 'skills' ); ?>">
																					<?php echo esc_html( $term->name ); ?>
																				</a>
																			</li>

																		<?php endforeach; ?>
																		</ul>
								            			</div>
																<?php endif;
															endif;
														?>

														<?php $technology_term = wp_get_post_terms($product->ID, 'technologies', array('orderby' => 'count', 'order' => 'DESC') );
															if ( ! empty( $technology_term ) ):
																if ( ! is_wp_error( $technology_term ) ): ?>
														
																	<div class="technologies text-center">
								                		<ul class="horizontal-navigation small-horizontal-navigation">
														
																		<?php foreach( $technology_term as $term ): ?>
																			
																			<li>
																				<a href="<?php echo get_term_link( $term->slug, 'technologies' ); ?>">
																					<img src="<?php echo get_template_directory_uri(); ?>/assets/skills/vector/<?php echo esc_html( $term->name ); ?>.svg" alt="<?php echo get_term_link( $term->slug, 'technologies' ); ?>" width="15" height="15">
																					<span> <?php echo esc_html( $term->name ); ?></span>
																				</a>
																			</li>

																		<?php endforeach; ?>
																		</ul>
								            			</div>
																<?php endif;
															endif;
														?>

								        </div>
								    </div>
								</div>