<?php get_header(); ?>

	<div class="section fp-auto-height">
        <div class="container">
            <div class="row titolo-sezione text-center">
                <div class="col-xs-12">
                    <div class="titolo-pagina clearfix">
                    <?php $current_term = get_queried_object()->name; ?>
                      <h1><?php echo $current_term; ?></h1>
                    </div>
                </div>
            </div>
				</div>
	</div>

	<div class="section fp-auto-height <?php echo strtolower($project_name); ?>-section">
		    <div class="container">
		        <div class="row">
							<?php get_template_part('template-parts/loop'); ?>
						</div>
				</div>
	</div>

<?php get_footer(); ?>
