<?php get_header(); ?>

		<div class="section <?php echo strtolower($product_name); ?>-section">
			<div class="container">
				<div class="row titolo-sezione">
					<div class="col-xs-12">
						<div class="titolo-pagina text-center clearfix">
							<h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
							<h2>
								<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'mana17' ); ?></a>
							</h2>      
						</div>
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>
