<?php get_header(); ?>

		<div class="section fp-auto-height filter-section">
        <div class="container">

            <div class="row titolo-sezione text-center">
                <div class="col-xs-12">
                    <div class="titolo-pagina taxonomy clearfix">
                    	<?php $current_term = get_queried_object()->name; ?>
                      <h1><span class="hidden"><?php _e( 'Technologies', 'html5blank' ); ?>: </span><?php echo $current_term; ?></h1>
                    </div>
                </div>
            </div>

            <?php
            $terms = get_terms( array(
						    'taxonomy' => 'technologies',
						    'hide_empty' => true,
						    'orderby' => 'count',
						    'order' => 'DESC'
						) );

						if ( !empty( $terms ) && !is_wp_error( $terms ) ): ?>

						<div class="technologies filter-choice frame">
								<ul class="horizontal-navigation slidee">

						<?php foreach ( $terms as $term ): ?>
										
										<li>
												<a
												href="<?php echo esc_url( get_term_link( $term ) ); ?>"
												title="<?php echo esc_attr( sprintf( __('View all post filed under %s', 'my_localization_domain' ), $term->name ) ); ?>"
												class="<?php if ($current_term === $term->name): echo 'active'; endif; ?>">
												<img src="<?php echo get_template_directory_uri(); ?>/assets/skills/vector/<?php echo esc_html( $term->slug ); ?>.svg" alt="<?php echo esc_html( $term->name ); ?>" width="15" height="15">
												<span> <?php echo $term->name; ?></span></a>
						        </li>

						<?php endforeach; wp_reset_postdata(); ?>
								</ul>
            </div>

            <div class="scrollbar">
					    <div class="handle">
					      <div class="mousearea"></div>
					    </div>
					  </div>
						<?php endif; ?>

				</div>
		</div>


		<?php
		$i = 0;
		$args_projects = array(
		  'posts_per_page'   => -1,
		  'post_type'        => 'projects',
		  'tax_query' => array(
					array(
						'taxonomy' => 'technologies',
						'field'    => 'name',
						'terms'    => $current_term
					),
				)
		);
		$projects_tax = get_posts( $args_projects ); ?>
		
		<?php if ($projects_tax): ?>
		<div class="section fp-auto-height projects-section">
				<div class="container">
            <div class="row">
            	<div class="col-xs-12 text-center">
            		<h3 class="interested-title"><?php _e( 'Interested Projects', 'html5blank' ); ?></h3>
            	</div>
            </div>

            <div class="row">

                <?php foreach ($projects_tax as $project): ?>
                <?php $i++; ?>

		            <!-- signle project -->
		            <?php get_template_part('template-parts/content', 'projects'); ?>
		            <!-- end single project -->

		            <?php if ( ($i % 2) == 0 ): ?>
		            	<div class="clearfix visible-xs-block hidden-sm hidden-md hidden-lg"></div>
		            <?php endif; ?>

		            <?php if ( ($i % 3) == 0 ): ?>
		            	<div class="clearfix hidden-xs visible-sm-block hidden-md hidden-lg"></div>
		            <?php endif; ?>

		            <?php if ( ($i % 6) == 0 ): ?>
		            	<div class="clearfix hidden-xs hidden-sm visible-md-block visible-lg-block"></div>
		            <?php endif; ?>

		            <?php endforeach; wp_reset_postdata(); ?>

            </div>
        </div>
    </div>
    <?php endif; ?>


    <?php
		$args_people = array(
		  'posts_per_page'   => -1,
		  'post_type'        => 'people',
		  'tax_query' => array(
					array(
						'taxonomy' => 'technologies',
						'field'    => 'name',
						'terms'    => $current_term
					),
				)
		);
		$people_tax = get_posts( $args_people ); ?>

		<?php if($people_tax): ?>
		<div class="section fp-auto-height people-section">
				<div class="container">
            <div class="row">
            	<div class="col-xs-12 text-center">
            		<h3 class="interested-title"><?php _e( 'People Involved', 'html5blank' ); ?></h3>
            	</div>
            </div>
            

            <div class="row">

                <?php foreach ($people_tax as $person): ?>

		            <!-- single person -->
		            <?php get_template_part('template-parts/content', 'people'); ?>
		            <!-- end single person -->

		            <?php endforeach; wp_reset_postdata(); ?>

            </div>
		    </div>
    </div>
		<?php endif; ?>
    
<?php get_footer(); ?>