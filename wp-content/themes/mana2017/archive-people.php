<?php get_header(); ?>

	<div class="section fp-auto-height people-section">
        <div class="container">
            <div class="row titolo-sezione text-center">
                <div class="col-xs-12">
                    <div class="titolo-pagina clearfix">
                    	<?php $current_term = get_queried_object()->name; ?>
                      <h1><?php echo 'Partners'; ?></h1>
<!--                      <h1>--><?php //echo $current_term; ?><!--</h1>-->
                    </div>
                </div>
            </div>
				</div>

		    <div class="container">
		        <div class="row">
					<?php get_template_part('template-parts/loop-people'); ?>
				</div>
			</div>
	</div>

<?php get_footer(); ?>
