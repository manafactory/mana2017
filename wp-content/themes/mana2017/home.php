<?php get_header(); ?>

	<div class="section text-center" id="home-slide">

    	<a href="javascript:$.fn.fullpage.moveTo(2);" title="Go to next section">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bollo-nero.svg" alt="" width="200" height="200" class="home-badge">
      </a>

    	<a href="javascript:$.fn.fullpage.moveTo(2);" title="Go to next section">
    			<?php $title_hp = get_field('titolo_home_page', 'option'); ?>
          <h1><?php echo $title_hp;?></h1>
			</a>

    	<a href="javascript:$.fn.fullpage.moveTo(2);" title="Go to next section">
    			<?php $subtitle_hp = get_field('sottotitolo_home_page', 'option'); ?>
          <p><?php echo $subtitle_hp;?></p>
      </a>
    	            
      <div class="next-slide">
          <a href="javascript:$.fn.fullpage.moveTo(2);" title="Go to next section">
              <i class="fa fa-2x fa-angle-down"></i>
          </a>
      </div>

  </div>
		
		<!-- section products -->
		<?php
    $args = array(
      'posts_per_page'   => '3',
      'post_type'        => 'products',
      'post_status'      => 'publish',
      'suppress_filters' => false,
			'orderby' => 'menu_order',
			'order' => 'ASC'
    );
    $products = get_posts( $args ); ?>

    <?php $product_obj = get_post_type_object( 'products' ); ?>
    <?php $product_name = $product_obj->labels->name; ?>

    <?php global $product; ?>
		
		<?php if ($products): ?>
    <div class="section <?php echo strtolower($product_name); ?>-section">
		    <div class="container">

		        <div class="row titolo-sezione">
		            <div class="col-xs-12">
		                <div class="titolo-pagina text-center clearfix">
		                    <h1><?php echo $product_name; ?></h1>
		                </div>
		            </div>
		        </div>

		        <div class="row">
		            
		            <?php foreach ($products as $product): ?>
		            
		            <!-- single product -->
		            <?php get_template_part('template-parts/content', 'products'); ?>
		            <!-- end single product -->
		            
		            <?php endforeach; ?>

		        </div>

		        <div class="row hidden">
		            <div class="col-xs-12 text-center">
		                <ul class="horizontal-navigation">
		                    <li>
		                        <h4>
		                            <a href="<?php echo get_post_type_archive_link( 'products' ); ?>" title="<?php _e( 'See all', 'html5blank' ); ?> <?php echo $product_name; ?>" class="see-all">
		                                <?php _e( 'See all', 'html5blank' ); ?>
		                            </a>
		                        </h4>   
		                    </li>
		                </ul>
		            </div>
		        </div>
		    </div>
		</div>
		<?php endif; ?>
		<!-- end section products -->

		
		<!-- section projects -->
		<?php
		$i = 0;
    $args = array(
      'posts_per_page'   => '6',
      'post_type'        => 'projects',
      'post_status'      => 'publish',
      'suppress_filters' => false,
			'orderby' => 'menu_order',
			'order' => 'ASC'
    );
    $projects = get_posts( $args ); ?>

    <?php $project_obj = get_post_type_object( 'projects' ); ?>
    <?php $project_name = $project_obj->labels->name; ?>

    <?php global $project; ?>
		
		<?php if ($projects): ?>
		<div class="section fp-auto-height <?php echo strtolower($project_name); ?>-section">
		    <div class="container">

		        <div class="row titolo-sezione">
		            <div class="col-xs-12">
		                <div class="titolo-pagina text-center clearfix">
		                    <h1><?php echo $project_name; ?></h1>
		                </div>
		            </div>
		        </div>
		        
		        <div class="row">

		            <?php foreach ($projects as $project): ?>
		            <?php $i++; ?>

		            <!-- signle project -->
		            <?php get_template_part('template-parts/content', 'projects'); ?>
		            <!-- end single project -->

		            <?php if ( ($i % 2) == 0 ): ?>
		            	<div class="clearfix visible-xs-block hidden-sm hidden-md hidden-lg"></div>
		            <?php endif; ?>

		            <?php if ( ($i % 3) == 0 ): ?>
		            	<div class="clearfix hidden-xs visible-sm-block hidden-md hidden-lg"></div>
		            <?php endif; ?>

		            <?php if ( ($i % 6) == 0 ): ?>
		            	<div class="clearfix hidden-xs hidden-sm visible-md-block visible-lg-block"></div>
		            <?php endif; ?>

		            <?php endforeach; ?>

		        </div>

		        <div class="row">
		            <div class="col-xs-12 text-center">
		                <ul class="horizontal-navigation">
		                    <li>
		                        <h4>
		                            <a href="<?php echo get_post_type_archive_link( 'projects' ); ?>" title="<?php _e( 'See all', 'html5blank' ); ?> <?php echo $project_name; ?>" class="see-all">
		                                <?php _e( 'See all', 'html5blank' ); ?>
		                            </a>
		                        </h4>   
		                    </li>
		                </ul>
		            </div>
		        </div>
		    </div>
		</div>
		<?php endif; ?>
		<!-- end section projects -->

		
		<!-- section people -->
		<?php
    $args = array(
      'posts_per_page'   => '4',
      'post_type'        => 'people',
      'post_status'      => 'publish',
      'suppress_filters' => false,
			'orderby' => 'menu_order',
			'order' => 'ASC'
    );
    $people = get_posts( $args ); ?>

    <?php $people_obj = get_post_type_object( 'people' ); ?>
    <?php $people_name = $people_obj->labels->name; ?>

    <?php global $person; ?>
		
		<?php if ($people): ?>
		<div class="section <?php echo strtolower($people_name); ?>-section" style="display:none;">
		    <div class="container">

		        <div class="row titolo-sezione">
		            <div class="col-xs-12">
		                <div class="titolo-pagina text-center clearfix">
		                    <h1><?php echo 'Partners'; ?></h1>
<!--		                    <h1>--><?php //echo $people_name; ?><!--</h1>-->
		                </div>
		            </div>
		        </div>

		        <div class="row">

		        		<?php foreach ($people as $person): ?>

		            <!-- single person -->
		            <?php get_template_part('template-parts/content', 'people'); ?>
		            <!-- end single person -->

		            <?php endforeach; ?>

		        </div>

		        <div class="row">
		            <div class="col-xs-12 text-center">
		                <ul class="horizontal-navigation">
		                    <li>
		                        <h4>
		                            <a href="<?php echo get_post_type_archive_link( 'people' ); ?>" title="<?php _e( 'See all', 'html5blank' ); ?> <?php echo $people_name; ?>" class="see-all">
		                                <?php _e( 'See all', 'html5blank' ); ?>
		                            </a>
		                        </h4>   
		                    </li>
		                </ul>
		            </div>
		        </div>
		    </div>
		</div>
		<?php endif; ?>
		<!-- end section people -->


<?php get_footer(); ?>