			<!-- footer -->
			<footer class="footer hidden" role="contentinfo">

				<!-- copyright -->
				<p class="copyright">
					&copy; <?php echo date('Y'); ?> <?php _e( 'Copyright', 'mana17' ); ?> <?php bloginfo('name'); ?>.
				</p>
				<!-- /copyright -->

			</footer>
			<!-- /footer -->
			
			</div>
			<!-- #fullpage page container -->

			<!-- modal header -->
			<div class="remodal-bg">
				<div class="remodal no-padding" data-remodal-id="quick-contact" id="quick-contact">
				  <ul>
                      <?php echo language_menu('modal_menu_lang');?>
						<li>
							<a href="tel:+390681173726" title="<?php _e('Call Manafactory','mana17');?>">
								+39 06 81173726
							</a>
						</li>
						<li>
							<a href="mailto:hello@manafactory.it" title="<?php _e('Email Manafactory','mana17');?>" target="_blank">
                                <?php _e('hello@manafactory.it','mana17');?>
							</a>
						</li>
						<?php $fb_url = get_field('facebook_url', 'option'); ?>
						<?php $tw_url = get_field('twitter_url', 'option'); ?>
						<?php $li_url = get_field('linkedin_url', 'option'); ?>
						<?php $md_url = get_field('medium_url', 'option'); ?>
						<?php if ($fb_url || $tw_url || $li_url || $md_url): ?>
						<li>
							<?php if($fb_url): ?>
							<a href="<?php echo $fb_url; ?>" title="<?php _e('Follow Manafactory on Facebook','mana17');?>" class="pull-left social-link-icon" target="_blank">
								<i class="fa fa-facebook"></i>
							</a>
							<?php endif; ?>
							<?php if($tw_url): ?>
							<a href="<?php echo $tw_url; ?>" title="<?php _e('Follow Manafactory on Twitter','mana17');?>" class="pull-left social-link-icon" target="_blank">
								<i class="fa fa-twitter"></i>
							</a>
							<?php endif; ?>
							<?php if($li_url): ?>
							<a href="<?php echo $li_url; ?>" title="<?php _e('Follow Manafactory on LinkedIn','mana17');?>" class="pull-left social-link-icon" target="_blank">
								<i class="fa fa-linkedin"></i>
							</a>
							<?php endif; ?>
							<?php if($md_url): ?>
							<a href="<?php echo $md_url; ?>" title="<?php _e('Follow Manafactory on Medium','mana17');?>" class="pull-left social-link-icon" target="_blank">
								<i class="fa fa-medium"></i>
							</a>
							<?php endif; ?>
						</li>
						<?php endif; ?>
					</ul>
				</div>

				<div class="remodal" data-remodal-id="manifesto" id="manifesto-modal">
					<div class="remodal-close" data-remodal-action="close"></div>
					<div class="scrollable">

						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bollo-nero.svg" alt="<?php _e('Manfactory SRL Logo','mana17');?>" width="140" height="140" class="manifesto-badge">
						
						<h3>
			        <strong><?php echo get_field('titolo_manifesto', 'option'); ?></strong>
			      </h3>

			      <div class="text-justify">
			        <?php $testo_manifesto = get_field('testo_manifesto', 'option'); ?>
			        <?php echo str_replace(array('<p>', '</p>'), '', $testo_manifesto); ?>
			      </div>
					</div>
				</div>
			</div>
			<!-- / modal header -->

		<?php wp_footer(); ?>

            <script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us12.list-manage.com","uuid":"8b49f8a822fc5501a40694329","lid":"da61800748"}) })</script>	</body>
</html>
